#pragma once

#include "Component.h"

// 각형ㅇ들을 여러개 가지고 있는다.삼
// 이 갸ㅐㄱ체를 컴포는트로 가지고 있으면 네바ㅣ를 탄다.

_BEGIN(Engine)

class CCell;
class _ENGINE_DLL CNavigation final : public CComponent
{
private:
	explicit CNavigation(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CNavigation(const CNavigation& rhs);
	virtual ~CNavigation() = default;
public:
	HRESULT Ready_Navigation(const _tchar* pFileName);
	HRESULT Ready_Neighbor();
	HRESULT Ready_Clone_Navigation(void* pArg);
	_bool Move_OnNavigation(const _vec3* vPosition, const _vec3* vDirectionPerSec);
	void Render_Navigation();
private:
	vector<CCell*>			m_vecCell;
	typedef vector<CCell*>	VECCELL;
private:
	_uint	m_iCurrentIdx = 0;
public:
	static CNavigation* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar* pFileName);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();
};


_END