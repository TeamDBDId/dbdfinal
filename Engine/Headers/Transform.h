#pragma once

// 월드행렬을 보관한다.
// 객체의 기본적이 ㄴ상태를 표현한다.

#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CTransform final : public CComponent
{
public:
	enum STATE { STATE_RIGHT, STATE_UP, STATE_LOOK, STATE_POSITION, STATE_END };
private:
	explicit CTransform(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CTransform(const CTransform& rhs);
	virtual ~CTransform() = default;
public: // Getter
	const _vec3* Get_StateInfo(STATE eState) const;
	_vec3 Get_Scale();
	_matrix Get_Matrix() const {
		return m_matWorld; }
	const _matrix* Get_Matrix_Pointer() const {
		return &m_matWorld;
	}
	_matrix Get_Matrix_Inverse() const;
	const _matrix*& Get_OtherMatrix() { return m_OtherMatrix; }
	const _matrix Get_WorldxParentMatrix() { return m_matWorld* *m_pParentMatrix; }
public: // Setter
	void Set_Matrix(_matrix matWorld) { m_matWorld = matWorld; }
	void Set_StateInfo(STATE eState, const _vec3* pInfo);
	void Set_PositionY(const _float& fY) {
		m_matWorld.m[3][1] = fY; }
	void Set_Parent(_matrix* pParentMatrix);
	void Set_OtherMatrix(const _matrix* pMat) { m_OtherMatrix = pMat; }
public:
	HRESULT Ready_Transform();
	HRESULT SetUp_OnGraphicDev();
	HRESULT SetUp_OnShader(LPD3DXEFFECT pEffect, const char* pConstantName);

	void SetUp_Speed(const _float& fMovePerSec, const _float& fRotationPerSec);
	void Go_Straight(const _float& fTimeDelta);
	void Go_Left(const _float& fTimeDelta);
	void Go_Right(const _float& fTimeDelta);
	void Go_Up(const _float& fTimeDelta);
	void BackWard(const _float& fTimeDelta);
	void SetUp_RotationX(const _float& fRadian);
	void SetUp_RotationY(const _float& fRadian);
	void SetUp_RotationZ(const _float& fRadian);
	void SetUp_Rotation_Axis(const _float& fRadian, const _vec3* pAxis);
	void Rotation_X(const _float& fTimeDelta);
	void Rotation_Y(const _float& fTimeDelta);
	void Rotation_Z(const _float& fTimeDelta);
	void Reverse_RotationY();
	void Rotation_Axis(const _float& fTimeDelta, const _vec3* pAxis);
	void Rotation_Axis_Angle(const _float& fAngle, const _vec3* pAxis);
	void Scaling(const _float& fX, const _float& fY, const _float& fZ);
	void Go_ToTarget(const _vec3* pTargetPos, const _float& fTimeDelta);
	void Go_ToTarget_DIR(const _vec3 * pDirPos, const _float & fTimeDelta);
	void SetUp_RotateXYZ(const _float* fAngleX, const _float* fAngleY, const _float* fAngleZ);
	void Set_PosY(const _float& _fY);

	void Move_V3(const _vec3& _vMove);
	void Move_V3Time(const _vec3& _vMove, const _float& _fTime);

	inline void Set_Acc(const _float& _fAcc) { m_fAcc= _fAcc; }
	inline const _float& Get_fAcc() { return m_fAcc; }
	void Move_Gravity(const _float& _fTime);

	void Go_ToTarget_ChangeDirection(const _vec3* pTargetPos, const _float& fTimeDelta, _bool bNotMove = false);
private:
	_matrix			m_matWorld; // 객체의 상태.
	_matrix*		m_pParentMatrix = nullptr;	
	_float			m_fSpeed_Move;
	_float			m_fSpeed_Rotation;

	_float			m_fAcc = 0.f;
	const _matrix*	m_OtherMatrix = nullptr;

public:
	static CTransform* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();
};

_END