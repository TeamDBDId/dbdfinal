#pragma once

#include "Component.h"

_BEGIN(Engine)

class CTransform;
class _ENGINE_DLL CFrustum final : public CComponent
{
private:
	explicit CFrustum(LPDIRECT3DDEVICE9 pGraphic_Device);	
	virtual ~CFrustum() = default;
public:
	HRESULT Get_LocalPlane(D3DXPLANE* pOutPlane, const _matrix* pInWorldMatrix);
public:
	HRESULT Ready_Frustum();
	HRESULT Transform_ToWorld();		
	_bool Culling_Frustum(const CTransform* pTransform, const _float& fRadius = 0.f);
	_bool Culling_Frustum(const _vec3* pPosition, const _matrix& matWorldInv, const _float& fRadius = 0.f);
	_bool isIn_Frustum(D3DXPLANE* pPlane, const _vec3* pPosition, const _float& fRadius);	
private:
	_vec3				m_vOriginal_Point[8];
	_vec3				m_vPoint[8];
	D3DXPLANE			m_Plane[6];
private:
	
public:
	static CFrustum* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component(void* pArg = nullptr);

protected:
	virtual void Free();

};

_END