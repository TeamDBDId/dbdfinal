#pragma once

#include "Base.h"

_BEGIN(Engine)
class CManagement;
class _ENGINE_DLL CKeyManager : public CBase
{
	_DECLARE_SINGLETON(CKeyManager)
private:
	enum KeyInput { KEY_DOWN, KEY_PRESSING, KEY_UP, KEY_END };
private:
	explicit CKeyManager();
	~CKeyManager() = default;
public:
	void KeyMgr_Update();
	_bool IsKeyInput() { return m_bKeyInput; }
public:
	_bool KeyDown(_uint In);
	_bool KeyUp(_uint In);
	_bool KeyPressing(_uint In);
public:
	_bool MouseDown(_uint In);
	_bool MouseUp(_uint In);
	_bool MousePressing(_uint In);
private:
	virtual void Free() override;
private:
	map<_uint, _bool[KEY_END]>	map_dwKey;
	map<_uint, _bool[KEY_END]>	map_dwMouse;
	_bool	m_bKeyInput = false;
};

_END