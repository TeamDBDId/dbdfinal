#pragma once

#include "Base.h"

_BEGIN(Engine)
class CGameObject;
class CShader;
class CFrustum;
class CLight final : public CBase
{
private:
	explicit CLight(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CLight() = default;
public:
	D3DLIGHT9* Get_LightInfo() {
		return &m_LightInfo;}
	LPDIRECT3DCUBETEXTURE9 GetShadowCubeTexture() {	return m_pTxCbm; }
public:
	void	Set_Type(D3DLIGHTTYPE type) { m_LightInfo.Type = type; }
	void	Set_Range(_float range) { m_LightInfo.Range = range; }

	void	Set_Diffuse(D3DXCOLOR vDiff) { m_LightInfo.Diffuse = vDiff; }
	void	Set_Ambient(D3DXCOLOR amb) { m_LightInfo.Ambient = amb; }
	void	Set_Specular(D3DXCOLOR spc) { m_LightInfo.Specular = spc; }

	void	Set_Position(_vec3 vPos) { m_LightInfo.Position = vPos; }
	void	Set_Direction(_vec3 vDir) { m_LightInfo.Direction = vDir; }
	void	Set_Phi(_float fPhi) { m_LightInfo.Phi = fPhi; } // �ܺο���
	void	Set_Theta(_float fTheta) { m_LightInfo.Theta = fTheta; } // ���ο���
	void	Set_World(_matrix matWld) { m_matWorld = matWld; }
	void	Set_Attenuation0(_float fAttenuation0) { m_LightInfo.Attenuation0 = fAttenuation0; }
	void	Set_Attenuation1(_float fAttenuation1) { m_LightInfo.Attenuation0 = fAttenuation1; }
	void	Set_Attenuation2(_float fAttenuation2) { m_LightInfo.Attenuation0 = fAttenuation2; }
	void	Set_Falloff(_float fFalloff) { m_LightInfo.Falloff = fFalloff; }
	//void	UseDynamicShadow(_bool b) { m_bUseDynamicShadowTex = b; }
	_vec3	GetProjPos();
	void	Set_Shaft(_bool bShaft) { m_bIsShaft = bShaft; }
	_bool	IsShaft() {	return m_bShaft; }
public:
	HRESULT Ready_Light(const D3DLIGHT9& LightInfo);
	void Render_Light(LPD3DXEFFECT pEffect);
	void Render_ShadowMap();
	void Render_DynamicShadowMap();
	void SetRender(_bool bRender) { m_bIsRender = bRender; }
	void AddObjList(CGameObject* pGameObj) { ObjList.push_back(pGameObj); }
	//void AddDynamicShadowList(CGameObject* pGameObj) { DynamicShadowList.push_back(pGameObj); }
	void CalProjPos();
	_float Get_CamDist() { return m_fCameraDist; }
	_vec4 CalShaftVec();
	_bool IsRender() { return m_bIsRender; }
	void SetStemp(_bool stemp) { m_bRenderStemp = stemp; }
	_bool GetStemp() { return m_bRenderStemp; }
	void Set_Shader(CShader* pShader) { m_pShader_Cube = pShader; }
private:
	_bool				m_bIsShaft = false;
	_bool				m_bShaft = false;
	_float				m_fCameraDist = 0.f;
	CFrustum*			m_pFrustumCom = nullptr;
	LPDIRECT3DDEVICE9	m_pGraphic_Device = nullptr;
	D3DLIGHT9			m_LightInfo;
	_bool				m_bIsRender = true;
	_bool				m_bUseShadowTex = false;
	//_bool				m_bUseDynamicShadowTex = false;
private:
	_bool						m_bRenderStemp = false;
	list<CGameObject*>			ObjList;
	//list<CGameObject*>			DynamicShadowList;
	LPDIRECT3DSURFACE9			pSrf[6] = { nullptr, nullptr,nullptr,nullptr,nullptr,nullptr };
	LPDIRECT3DSURFACE9			pOld = nullptr;
	LPDIRECT3DCUBETEXTURE9		m_pTxCbm = nullptr;
	//LPDIRECT3DCUBETEXTURE9		m_pShadowTex = nullptr;
	CShader*					m_pShader_Cube = nullptr;
	_vec3						vProjPos = _vec3(0, 0, 0);
	_matrix						m_matWorld;
	_int						m_iCount = 0;
private:
	LPDIRECT3DVERTEXBUFFER9		m_pVB = nullptr;
	LPDIRECT3DINDEXBUFFER9		m_pIB = nullptr;
private:
	void T_SetupCubeViewMatrix(_matrix* pmtViw, DWORD dwFace);
public:
	static CLight* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9& LightInfo);
protected:
	virtual void Free();
};

_END