#include "..\Headers\Mesh_Static.h"
#include "MeshTexture.h"
CMesh_Static::CMesh_Static(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{
	m_pRenderNum = new _int;
	*m_pRenderNum = 0;
}

CMesh_Static::CMesh_Static(const CMesh_Static & rhs)
	: CComponent(rhs)
	, m_pMesh(rhs.m_pMesh)
	, m_dwNumMaterials(rhs.m_dwNumMaterials)
	, m_pSubSetDesc(rhs.m_pSubSetDesc)
	, m_vMin(rhs.m_vMin)
	, m_vMax(rhs.m_vMax)
	, m_pRenderNum(rhs.m_pRenderNum)
{
	for (size_t i = 0; i < m_dwNumMaterials; i++)
	{
		if(nullptr != m_pSubSetDesc[i].MeshTexture.pDiffuseTexture)
			m_pSubSetDesc[i].MeshTexture.pDiffuseTexture->AddRef();
		if (nullptr != m_pSubSetDesc[i].MeshTexture.pNormalTexture)
			m_pSubSetDesc[i].MeshTexture.pNormalTexture->AddRef();
		if (nullptr != m_pSubSetDesc[i].MeshTexture.pSpecularTexture)
			m_pSubSetDesc[i].MeshTexture.pSpecularTexture->AddRef();
		if (nullptr != m_pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture)
			m_pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture->AddRef();
		if (nullptr != m_pSubSetDesc[i].MeshTexture.pHightTexture)
			m_pSubSetDesc[i].MeshTexture.pHightTexture->AddRef();
		if (nullptr != m_pSubSetDesc[i].MeshTexture.pMetallicTexture)
			m_pSubSetDesc[i].MeshTexture.pMetallicTexture->AddRef();
		if (nullptr != m_pSubSetDesc[i].MeshTexture.pRoughnessTexture)
			m_pSubSetDesc[i].MeshTexture.pRoughnessTexture->AddRef();
	}
	
	m_pMesh->AddRef();
}

_matrix CMesh_Static::Get_LocalTransform() const
{
	_matrix			matScale, matTrans;

	D3DXMatrixScaling(&matScale, m_vMax.x - m_vMin.x, m_vMax.y - m_vMin.y, m_vMax.z - m_vMin.z);

	D3DXMatrixTranslation(&matTrans, (m_vMax.x + m_vMin.x) * 0.5f, (m_vMax.y + m_vMin.y) * 0.5f, (m_vMax.z + m_vMin.z) * 0.5f);
	
	return matScale * matTrans;
}

HRESULT CMesh_Static::Ready_Mesh_Static(const _tchar * pFilePath, const _tchar * pFileName)
{
	LPD3DXMESH		pMesh = nullptr;
	_tchar			szFullPath[MAX_PATH] = L"";

	lstrcpy(szFullPath, pFilePath);
	lstrcat(szFullPath, pFileName);	

	if (FAILED(D3DXLoadMeshFromX(szFullPath, D3DXMESH_MANAGED, m_pGraphic_Device, &m_pAdjacency, &m_pMaterials, nullptr, &m_dwNumMaterials, &pMesh)))
		return E_FAIL;

	m_pSubSetDesc = new SUBSETDESC[m_dwNumMaterials];
	ZeroMemory(m_pSubSetDesc, sizeof(SUBSETDESC) * m_dwNumMaterials);

	CMeshTexture* pMeshTexture = GET_INSTANCE(CMeshTexture);
	pMeshTexture->AddRef();

	for (size_t i = 0; i < m_dwNumMaterials; i++)
	{
		D3DXMATERIAL* pMaterial = (D3DXMATERIAL*)m_pMaterials->GetBufferPointer() + i;

		m_pSubSetDesc[i].Material = *pMaterial;

		ZeroMemory(szFullPath, sizeof(_tchar) * MAX_PATH);

		_tchar			szFileName[MAX_PATH] = L"";

		if (pMaterial->pTextureFilename == nullptr)
			continue;

		MultiByteToWideChar(CP_ACP, 0, pMaterial->pTextureFilename, strlen(pMaterial->pTextureFilename), szFileName, MAX_PATH);


		lstrcpy(szFullPath, pFilePath);

		_tchar		szTmp[MAX_PATH] = L"";
		_tchar		szTmp2[MAX_PATH] = L"";
		lstrcpy(szTmp, szFullPath);
		lstrcat(szTmp, szFileName);
		if (FAILED(pMeshTexture->AddTexture(m_pGraphic_Device, pFilePath, szFileName, &m_pSubSetDesc[i].MeshTexture.pDiffuseTexture)))
			return E_FAIL;

		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"D", L"N")))
		{
			lstrcat(szTmp, szTmp2);
			if (FAILED(pMeshTexture->AddTexture(m_pGraphic_Device, pFilePath, szTmp2, &m_pSubSetDesc[i].MeshTexture.pNormalTexture)))
				pMeshTexture->AddTexture(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"T_FlatNormal_01.tga", &m_pSubSetDesc[i].MeshTexture.pNormalTexture);
		}
		else
			pMeshTexture->AddTexture(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"T_FlatNormal_01.tga", &m_pSubSetDesc[i].MeshTexture.pNormalTexture);
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"D", L"CAO")))
		{
			lstrcat(szTmp, szTmp2);
			if (FAILED(pMeshTexture->AddTexture(m_pGraphic_Device, pFilePath, szTmp2, &m_pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture)))
				pMeshTexture->AddTexture(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"WhiteAO.png", &m_pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture);
		}
		else
			pMeshTexture->AddTexture(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"WhiteAO.png", &m_pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture);
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"D", L"R")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, pFilePath, szTmp2, &m_pSubSetDesc[i].MeshTexture.pRoughnessTexture);
		}
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"D", L"H")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, pFilePath, szTmp2, &m_pSubSetDesc[i].MeshTexture.pHightTexture);
		}
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"D", L"M")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, pFilePath, szTmp2, &m_pSubSetDesc[i].MeshTexture.pMetallicTexture);
		}
	}
	Safe_Release(pMeshTexture);
	

	_ulong NumFaces = pMesh->GetNumFaces();

	ID3DXBuffer* errors = 0;

	if (FAILED(D3DXCleanMesh(D3DXCLEAN_SIMPLIFICATION, pMesh, (DWORD*)m_pAdjacency->GetBufferPointer(),
		&pMesh, (DWORD*)m_pAdjacency->GetBufferPointer(), &errors)))
		return E_FAIL;

	//

	if (FAILED(pMesh->OptimizeInplace(D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_COMPACT | D3DXMESHOPT_VERTEXCACHE, (DWORD*)m_pAdjacency->GetBufferPointer()
		, (DWORD*)m_pAdjacency->GetBufferPointer(), 0, 0)))
		return E_FAIL;

	if (FAILED(pMesh->Optimize(D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_COMPACT | D3DXMESHOPT_STRIPREORDER, (DWORD*)m_pAdjacency->GetBufferPointer()
		, (DWORD*)m_pAdjacency->GetBufferPointer(), 0, 0, &pMesh)))
		return E_FAIL;

	pMesh->GenerateAdjacency(1.f, (DWORD*)m_pAdjacency->GetBufferPointer());

	_ulong dwFVF = 0;
	dwFVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

 	if (FAILED(pMesh->CloneMeshFVF(pMesh->GetOptions(), dwFVF, m_pGraphic_Device, &m_pMesh)))
		return E_FAIL;

	/*_ulong		dwFVF = pMesh->GetFVF();
	if (false == (dwFVF & D3DFVF_NORMAL))
	{
		if (FAILED(pMesh->CloneMeshFVF(pMesh->GetOptions(), dwFVF | D3DFVF_NORMAL, m_pGraphic_Device, &m_pMesh)))
			return E_FAIL;

		if (FAILED(D3DXComputeNormals(m_pMesh, (_ulong*)m_pAdjacency->GetBufferPointer())))
			return E_FAIL;
	}
	else
	{
		if (FAILED(pMesh->CloneMeshFVF(pMesh->GetOptions(), dwFVF, m_pGraphic_Device, &m_pMesh)))
			return E_FAIL;
	}*/

	Safe_Release(pMesh);


	D3DVERTEXELEMENT9			Element[MAX_FVF_DECL_SIZE];
	ZeroMemory(Element, sizeof(D3DVERTEXELEMENT9) * MAX_FVF_DECL_SIZE);

	_ushort	wOffset = 0;

	m_pMesh->GetDeclaration(Element);

	for (size_t i = 0; i < MAX_FVF_DECL_SIZE; ++i)
	{
		if (D3DDECLUSAGE_POSITION == Element[i].Usage)
		{
			wOffset = Element[i].Offset;
			break;
		}		
	}
	

	void*			pVertices = nullptr;

	m_pMesh->LockVertexBuffer(0, &pVertices);
	
	if (FAILED(D3DXComputeBoundingBox(((_vec3*)((_byte*)pVertices) + wOffset), m_pMesh->GetNumVertices(), D3DXGetFVFVertexSize(m_pMesh->GetFVF()), &m_vMin, &m_vMax)))
		return E_FAIL;

	m_pMesh->UnlockVertexBuffer();

	const D3DVERTEXELEMENT9 verDECL[] =
	{
		{ 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
		{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0 },
		{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
		{ 0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 },
		D3DDECL_END()
	};

	if (FAILED(m_pMesh->CloneMesh(m_pMesh->GetOptions(), verDECL, m_pGraphic_Device, &m_pMesh)))
		return E_FAIL;

	if (FAILED(D3DXComputeTangent(m_pMesh, 0, 0, 0, 0, (_ulong*)m_pAdjacency->GetBufferPointer())))
		return E_FAIL;

	return NOERROR;
}

void CMesh_Static::Render_Mesh(_uint iAttributeID)
{
	if (nullptr == m_pMesh)
		return;

	// 셰이더에 이 서브셋이 사용하는 디퓨즈 텍스쳐를 전달한다.
	m_pMesh->DrawSubset(iAttributeID);
}

HRESULT CMesh_Static::Change_TextureFileName(_tchar* pFileName, const _tchar* pSour, const _tchar* pDest)
{
	wstring FileName = pFileName;
	size_t Num = FileName.find_last_of(pSour);
	if (Num == string::npos)
		return E_FAIL;
	FileName.replace(Num, 1, pDest);
	lstrcpy(pFileName, FileName.c_str());
	return NOERROR;
}

CMesh_Static * CMesh_Static::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar * pFilePath, const _tchar * pFileName)
{
	CMesh_Static*	pInstance = new CMesh_Static(pGraphic_Device);

	if (FAILED(pInstance->Ready_Mesh_Static(pFilePath, pFileName)))
	{
		MessageBox(0, L"CMesh_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CMesh_Static::Clone_Component(void* pArg)
{
	return new CMesh_Static(*this);
}

void CMesh_Static::Free()
{
		

	if (false == m_isClone)
	{
		for (size_t i = 0; i < m_dwNumMaterials; i++)
		{
			m_pSubSetDesc[i].MeshTexture.pDiffuseTexture = nullptr;
			m_pSubSetDesc[i].MeshTexture.pNormalTexture = nullptr;
			m_pSubSetDesc[i].MeshTexture.pSpecularTexture = nullptr;
			m_pSubSetDesc[i].MeshTexture.pRoughnessTexture = nullptr;
			m_pSubSetDesc[i].MeshTexture.pNormalTexture = nullptr;
			m_pSubSetDesc[i].MeshTexture.pMetallicTexture= nullptr;
			m_pSubSetDesc[i].MeshTexture.pHightTexture = nullptr;
		}
		Safe_Delete_Array(m_pSubSetDesc);
	}
	if (!m_isClone)
		Safe_Delete(m_pRenderNum);

	Safe_Release(m_pMesh);

	CComponent::Free();
}
