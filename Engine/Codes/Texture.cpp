#include "..\Headers\Texture.h"

CTexture::CTexture(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{

}

CTexture::CTexture(const CTexture & rhs)
	: CComponent(rhs)
	, m_vecTexture(rhs.m_vecTexture)
{
	for (auto& pTexture : m_vecTexture)
		pTexture->AddRef();
}

HRESULT CTexture::Ready_Texture(TYPE eType, const _tchar * pFilePath, const _uint & iNumTexture)
{
	m_vecTexture.reserve(iNumTexture);

	for (size_t i = 0; i < iNumTexture; i++)
	{
		IDirect3DBaseTexture9*		pTexture = nullptr;
		D3DXIMAGE_INFO ImgInfo;
		_tchar						szFilePath[MAX_PATH] = L"";

		wsprintf(szFilePath, pFilePath, i);

		if (TYPE_GENERAL == eType)
		{
			if (FAILED(D3DXCreateTextureFromFileEx(m_pGraphic_Device, szFilePath, 0, 0, 0, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, &ImgInfo, NULL, (LPDIRECT3DTEXTURE9*)&pTexture)))
				return E_FAIL;
			m_vecTextureRect.push_back(_vec2((_float)ImgInfo.Width, (_float)ImgInfo.Height));
		}
		else if (TYPE_CUBE == eType)
		{
			if (FAILED(D3DXCreateCubeTextureFromFile(m_pGraphic_Device, szFilePath, (LPDIRECT3DCUBETEXTURE9*)&pTexture)))
				return E_FAIL;
		}		

		m_vecTexture.push_back(pTexture);
	}

	return NOERROR;
}

HRESULT CTexture::SetUp_OnGraphicDev(const _uint & iIndex)
{
	if (m_vecTexture.size() <= iIndex)
		return E_FAIL;

	if (FAILED(m_pGraphic_Device->SetTexture(0, m_vecTexture[iIndex])))
		return E_FAIL;

	return NOERROR;
}

HRESULT CTexture::SetUp_OnShader(LPD3DXEFFECT pEffect, const char * pConstantName, const _uint & iIndex)
{
	if (m_vecTexture.size() <= iIndex)
		return E_FAIL;

	if (FAILED(pEffect->SetTexture(pConstantName, m_vecTexture[iIndex])))
		return E_FAIL;

	return NOERROR;
}

_vec2 * CTexture::Get_TexRect(const _uint & iIndex)
{
	if (m_vecTextureRect.size() <= iIndex)
		return nullptr;
	return &m_vecTextureRect[iIndex];
}

CTexture * CTexture::Create(LPDIRECT3DDEVICE9 pGraphic_Device, TYPE eType, const _tchar * pFilePath, const _uint & iNumTexture)
{
	CTexture*	pInstance = new CTexture(pGraphic_Device);

	if (FAILED(pInstance->Ready_Texture(eType, pFilePath, iNumTexture)))
	{
		MessageBox(0, L"CTexture Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}
CComponent * CTexture::Clone_Component(void* pArg)
{
	return new CTexture(*this);
}

void CTexture::Free()
{
	for (auto& pTexture : m_vecTexture)
		Safe_Release(pTexture);

	m_vecTexture.clear();
	m_vecTextureRect.clear();

	CComponent::Free();
}
