#include "..\Headers\Frustum.h"
#include "Transform.h"

CFrustum::CFrustum(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{

}

HRESULT CFrustum::Get_LocalPlane(D3DXPLANE * pOutPlane, const _matrix * pInWorldMatrix)
{
	_vec3		vPoint[8];
	_matrix		matWorldInv;

	D3DXMatrixInverse(&matWorldInv, nullptr, pInWorldMatrix);

	for (size_t i = 0; i < 8; ++i)	
		D3DXVec3TransformCoord(&vPoint[i], &m_vPoint[i], &matWorldInv);	

	// +x
	D3DXPlaneFromPoints(&pOutPlane[0], &vPoint[1], &vPoint[5], &vPoint[6]);
	// -x
	D3DXPlaneFromPoints(&pOutPlane[1], &vPoint[4], &vPoint[0], &vPoint[3]);

	// +y
	D3DXPlaneFromPoints(&pOutPlane[2], &vPoint[4], &vPoint[5], &vPoint[1]);
	// -y
	D3DXPlaneFromPoints(&pOutPlane[3], &vPoint[3], &vPoint[2], &vPoint[6]);


	// +z
	D3DXPlaneFromPoints(&pOutPlane[4], &vPoint[7], &vPoint[6], &vPoint[5]);
	// -z
	D3DXPlaneFromPoints(&pOutPlane[5], &vPoint[0], &vPoint[1], &vPoint[2]);

	return NOERROR;
}

HRESULT CFrustum::Ready_Frustum()
{
	m_vOriginal_Point[0] = _vec3(-1.f, 1.f, 0.f);
	m_vOriginal_Point[1] = _vec3(1.f, 1.f, 0.f);
	m_vOriginal_Point[2] = _vec3(1.f, -1.f, 0.f);
	m_vOriginal_Point[3] = _vec3(-1.f, -1.f, 0.f);

	m_vOriginal_Point[4] = _vec3(-1.f, 1.f, 1.f);
	m_vOriginal_Point[5] = _vec3(1.f, 1.f, 1.f);
	m_vOriginal_Point[6] = _vec3(1.f, -1.f, 1.f);
	m_vOriginal_Point[7] = _vec3(-1.f, -1.f, 1.f);

	return NOERROR;
}

HRESULT CFrustum::Transform_ToWorld()
{
	
	_matrix			matView, matProj;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	D3DXMatrixInverse(&matView, nullptr, &matView);
	D3DXMatrixInverse(&matProj, nullptr, &matProj);


	for (size_t i = 0; i < 8; ++i)
	{
		// 뷰스페이스로 
		D3DXVec3TransformCoord(&m_vPoint[i], &m_vOriginal_Point[i], &matProj);

		// 월드스페이스로.
		D3DXVec3TransformCoord(&m_vPoint[i], &m_vPoint[i], &matView);
	}

	return NOERROR;
}

_bool CFrustum::Culling_Frustum(const CTransform * pTransform, const _float& fRadius)
{
	_vec3		vPoint[8];

	memcpy(vPoint, m_vPoint, sizeof(_vec3) * 8);

	_vec3		vPosition = *(pTransform->Get_StateInfo(CTransform::STATE_POSITION));
	_matrix		matWorldInv = pTransform->Get_Matrix_Inverse();

	D3DXVec3TransformCoord(&vPosition, &vPosition, &matWorldInv);

	for (size_t i = 0; i < 8; ++i)	
		D3DXVec3TransformCoord(&vPoint[i], &vPoint[i], &matWorldInv);

	// +x
	D3DXPlaneFromPoints(&m_Plane[0], &vPoint[1], &vPoint[5], &vPoint[6]);
	// -x
	D3DXPlaneFromPoints(&m_Plane[1], &vPoint[4], &vPoint[0], &vPoint[3]);

	// +y
	D3DXPlaneFromPoints(&m_Plane[2], &vPoint[4], &vPoint[5], &vPoint[1]);
	// -y
	D3DXPlaneFromPoints(&m_Plane[3], &vPoint[3], &vPoint[2], &vPoint[6]);


	// +z
	D3DXPlaneFromPoints(&m_Plane[4], &vPoint[7], &vPoint[6], &vPoint[5]);
	// -z
	D3DXPlaneFromPoints(&m_Plane[5], &vPoint[0], &vPoint[1], &vPoint[2]);

	return isIn_Frustum(m_Plane, &vPosition, fRadius);
}

_bool CFrustum::Culling_Frustum(const _vec3 * pPosition, const _matrix & matWorldInv, const _float & fRadius)
{
	_vec3		vPoint[8];

	memcpy(vPoint, m_vPoint, sizeof(_vec3) * 8);

	_vec3		vPosition = *pPosition;	

	D3DXVec3TransformCoord(&vPosition, &vPosition, &matWorldInv);

	for (size_t i = 0; i < 8; ++i)
		D3DXVec3TransformCoord(&vPoint[i], &vPoint[i], &matWorldInv);

	// +x
	D3DXPlaneFromPoints(&m_Plane[0], &vPoint[1], &vPoint[5], &vPoint[6]);
	// -x
	D3DXPlaneFromPoints(&m_Plane[1], &vPoint[4], &vPoint[0], &vPoint[3]);

	// +y
	D3DXPlaneFromPoints(&m_Plane[2], &vPoint[4], &vPoint[5], &vPoint[1]);
	// -y
	D3DXPlaneFromPoints(&m_Plane[3], &vPoint[3], &vPoint[2], &vPoint[6]);


	// +z
	D3DXPlaneFromPoints(&m_Plane[4], &vPoint[7], &vPoint[6], &vPoint[5]);
	// -z
	D3DXPlaneFromPoints(&m_Plane[5], &vPoint[0], &vPoint[1], &vPoint[2]);

	return isIn_Frustum(m_Plane, &vPosition, fRadius);
}

_bool CFrustum::isIn_Frustum(D3DXPLANE* pPlane, const _vec3 * pPosition, const _float& fRadius)
{
	for (size_t i = 0; i < 6; ++i)
	{
		// ax + by + cz + d = ?
		if (fRadius < D3DXPlaneDotCoord(&pPlane[i], pPosition))
			return false;
	}
	return _bool(true);
}

CFrustum * CFrustum::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CFrustum*	pInstance = new CFrustum(pGraphic_Device);

	if (FAILED(pInstance->Ready_Frustum()))
	{
		MessageBox(0, L"CFrustum Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CFrustum::Clone_Component(void * pArg)
{
	AddRef();

	return this;
}



void CFrustum::Free()
{
	CComponent::Free();
}
