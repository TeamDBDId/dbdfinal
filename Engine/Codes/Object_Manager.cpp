#include "..\Headers\Object_Manager.h"
#include "Layer.h"

_IMPLEMENT_SINGLETON(CObject_Manager)
CObject_Manager::CObject_Manager()
{

}

CComponent * CObject_Manager::Get_ComponentPointer(const _uint & iSceneID, const _tchar * pLayerTag, const _tchar * pComponentTag, const _uint & iIndex)
{
	if (nullptr == m_pMapLayers)
		return nullptr;

	if (m_iNumScene <= iSceneID)
		return nullptr;

	CLayer*		pLayer = Find_Layer(iSceneID, pLayerTag);
	if (nullptr == pLayer)
		return nullptr;

	return pLayer->Get_ComponentPointer(pComponentTag, iIndex);
}

HRESULT CObject_Manager::Reserve_Object_Manager(const _uint & iNumScene)
{
	if (nullptr != m_pMapLayers)
		return E_FAIL;

	m_pMapLayers = new MAPLAYERS[iNumScene];

	m_iNumScene = iNumScene;

	return NOERROR;
}

HRESULT CObject_Manager::Add_Prototype_GameObject(const _tchar * pGameObjectTag, CGameObject * pGameObject)
{
	if (nullptr == pGameObject)
		return E_FAIL;

	if (nullptr != Find_Prototype(pGameObjectTag))
		return E_FAIL;

	auto IsExist = m_mapPrototype.insert(MAPPROTOTYPE::value_type(pGameObjectTag, pGameObject));
	if (!IsExist.second)
	{
		IsExist.first->second = pGameObject;
	}

	return NOERROR;
}

HRESULT CObject_Manager::Add_GameObjectToLayer(const _tchar * pProtoTag, const _uint & iSceneID, const _tchar * pLayerTag, CGameObject** ppCloneObject)
{
	if (nullptr == m_pMapLayers)
		return E_FAIL;

	if (m_iNumScene <= iSceneID)
		return E_FAIL;

	CGameObject*	pPrototype = nullptr;

	pPrototype = Find_Prototype(pProtoTag);

	if (nullptr == pPrototype)
		return E_FAIL;

	CGameObject*	pGameObject = pPrototype->Clone_GameObject();
	if (nullptr == pGameObject)
		return E_FAIL;

	if (nullptr != ppCloneObject)
		*ppCloneObject = pGameObject;

	// 객체를 레이어에 추가합니다.
	// 객체를 추가할 레이어가 있냐?!
	CLayer*	pLayer = Find_Layer(iSceneID, pLayerTag);

	if (nullptr == pLayer) // 그 레이어가 없었어.
	{
		pLayer = CLayer::Create();
		if (nullptr == pLayer)
			return E_FAIL;

		if (FAILED(pLayer->Add_Object(pGameObject)))
			return E_FAIL;

		auto IsExist = m_pMapLayers[iSceneID].insert(MAPLAYERS::value_type(pLayerTag, pLayer));
		if (!IsExist.second)
		{
			IsExist.first->second = pLayer;
		}
	}
	else // 레이거가 이미 있었어.
		if (FAILED(pLayer->Add_Object(pGameObject)))
			return E_FAIL;

	return NOERROR;
}

_int CObject_Manager::Update_GameObject_Manager(const _float & fTimeDelta)
{
	if (nullptr == m_pMapLayers)
		return -1;

	for (size_t i = 0; i < m_iNumScene; i++)
	{
		for (auto& Pair : m_pMapLayers[i])
		{
			if (nullptr != Pair.second)
			{
				if (Pair.second->Update_Object(fTimeDelta) & 0x80000000)
					return -1;
			}
		}
	}

	return _int();
}

_int CObject_Manager::Update_Scene_GameObject_Manager(const _float & fTimeDelta, const _uint & iSceneID)
{
	if (nullptr == m_pMapLayers)
		return -1;

	for (auto& Pair : m_pMapLayers[iSceneID])
	{
		if (nullptr != Pair.second)
		{
			if (Pair.second->Update_Object(fTimeDelta) & 0x80000000)
				return -1;
		}
	}

	return _int();
}

_int CObject_Manager::LastUpdate_GameObject_Manager(const _float & fTimeDelta)
{
	if (nullptr == m_pMapLayers)
		return -1;

	for (size_t i = 0; i < m_iNumScene; i++)
	{
		for (auto& Pair : m_pMapLayers[i])
		{
			if (nullptr != Pair.second)
			{
				if (Pair.second->LastUpdate_Object(fTimeDelta) & 0x80000000)
					return -1;
			}
		}
	}

	return _int();
}

_int CObject_Manager::LastUpdate_Scene_GameObject_Manager(const _float & fTimeDelta, const _uint & iSceneID)
{
	if (nullptr == m_pMapLayers)
		return -1;

	for (auto& Pair : m_pMapLayers[iSceneID])
	{
		if (nullptr != Pair.second)
		{
			if (Pair.second->LastUpdate_Object(fTimeDelta) & 0x80000000)
				return -1;
		}
	}
	return _int();
}

HRESULT CObject_Manager::Clear_Layers(const _uint & iSceneID)
{
	if (nullptr == m_pMapLayers)
		return E_FAIL;

	if (m_iNumScene <= iSceneID)
		return E_FAIL;

	for (auto& Pair : m_pMapLayers[iSceneID])
		Safe_Release(Pair.second);

	m_pMapLayers[iSceneID].clear();

	return NOERROR;
}

HRESULT CObject_Manager::Remove_Prototype_GameObject(const _tchar * pGameObjectTag)
{
	if (m_mapPrototype.empty())
		return E_FAIL;

	auto it = m_mapPrototype.find(pGameObjectTag);
	if (m_mapPrototype.end() == it)
		return E_FAIL;

	Safe_Release(it->second);

	return NOERROR;
}

HRESULT CObject_Manager::Remove_GameObjectFromLayer(const _uint & iSceneID, const _tchar * pLayerTag)
{
	if (nullptr == m_pMapLayers)
		return E_FAIL;

	if (m_iNumScene <= iSceneID)
		return E_FAIL;

	auto it = m_pMapLayers[iSceneID].find(pLayerTag);
	if (m_pMapLayers[iSceneID].end() == it)
		return E_FAIL;

	Safe_Release(it->second);

	return NOERROR;
}

list<CGameObject*>* CObject_Manager::Get_ObjList(const _uint & iSceneID, const _tchar * pLayerTag)
{
	if (Find_Layer(iSceneID, pLayerTag) == nullptr)
		return nullptr;
	else
		return &Find_Layer(iSceneID, pLayerTag)->Get_ObjList();
}

list<CGameObject*>& CObject_Manager::Get_ObjectList(const _uint & iSceneID, const _tchar * pLayerTag)
{
	auto iter = find_if(m_pMapLayers[iSceneID].begin(), m_pMapLayers[iSceneID].end()
		, CFinder_Tag(pLayerTag));

	if (iter == m_pMapLayers[iSceneID].end())
	{
		//_MSG_BOX("Can't Find ObjectList Layer");
		return list<CGameObject*>();
	}

	return iter->second->GetGameObjectList();
}

CGameObject * CObject_Manager::Get_GameObject(const _uint & iSceneID, const _tchar * pLayerTag, const _uint & iIndex)
{
	if (nullptr == m_pMapLayers)
		return nullptr;
	if (m_iNumScene <= iSceneID)
		return nullptr;
	CLayer*		pLayer = Find_Layer(iSceneID, pLayerTag);
	if (nullptr == pLayer)
		return nullptr;

	return pLayer->Get_GameObject(iIndex);
}

CGameObject * CObject_Manager::Find_Prototype(const _tchar * pGameObjectTag)
{
	auto	iter = find_if(m_mapPrototype.begin(), m_mapPrototype.end(), CFinder_Tag(pGameObjectTag));

	if (iter == m_mapPrototype.end())
		return nullptr;

	return iter->second;
}

CLayer * CObject_Manager::Find_Layer(const _uint & iSceneID, const _tchar * pLayerTag)
{
	auto	iter = find_if(m_pMapLayers[iSceneID].begin(), m_pMapLayers[iSceneID].end(), CFinder_Tag(pLayerTag));

	if (iter == m_pMapLayers[iSceneID].end())
		return nullptr;

	return iter->second;
}

void CObject_Manager::Free()
{
	for (size_t i = 0; i < m_iNumScene; i++)
	{
		for (auto& Pair : m_pMapLayers[i])
			Safe_Release(Pair.second);

		m_pMapLayers[i].clear();
	}

	Safe_Delete_Array(m_pMapLayers);

	for (auto& Pair : m_mapPrototype)
		Safe_Release(Pair.second);

	m_mapPrototype.clear();
}
