#include "..\Headers\Buffer_TriCol.h"

CBuffer_TriCol::CBuffer_TriCol(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVIBuffer(pGraphic_Device)
{
}

CBuffer_TriCol::CBuffer_TriCol(const CBuffer_TriCol & rhs)
	: CVIBuffer(rhs)
{
}

HRESULT CBuffer_TriCol::Ready_VIBuffer()
{
	// 삼각형의 형태를 띄는 정점(버텍스)배열을 담을ㅇ 수 있는 공간(버퍼)을 할당한다.
	
	m_iStride = sizeof(VTXCOL);
	m_iNumVertices = 3;
	m_dwFVF = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX0;
	m_iNumPolygons = 1;
	
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	// 버텍스버퍼의 공간을 만든다.
	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	// 우리가 할당한 정점버퍼 공간에 접근하여 값을 변경한다.
	VTXCOL*		pVertices = nullptr;
	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition = _vec3(0.0f, 1.f, 0.f);
	pVertices[0].dwColor = D3DXCOLOR(1.f, 0.f, 0.f, 1.f);

	pVertices[1].vPosition = _vec3(1.f, -1.f, 0.f);
	pVertices[1].dwColor = D3DXCOLOR(1.f, 1.f, 0.f, 1.f);

	pVertices[2].vPosition = _vec3(-1.f, -1.f, 0.f);
	pVertices[2].dwColor = D3DXCOLOR(0.f, 1.f, 0.f, 1.f);
	
	m_pVB->Unlock();

	return NOERROR;
}

void CBuffer_TriCol::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, m_iNumPolygons);
}

CBuffer_TriCol * CBuffer_TriCol::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_TriCol*	pInstance = new CBuffer_TriCol(pGraphic_Device);

	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		MessageBox(0, L"CBuffer_TriCol Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}


CComponent * CBuffer_TriCol::Clone_Component(void* pArg)
{
	return new CBuffer_TriCol(*this);
}

void CBuffer_TriCol::Free()
{
	CVIBuffer::Free();
}
