

// 컨스턴트 테이블
matrix		g_matWorld, g_matView, g_matProj;
float2		g_fUV;
vector		g_vColor;

// 텍스쳐정보를 받아오기위한 전역변수.
texture		g_DiffuseTexture;

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

// 로컬영역상에(정점버퍼에 있는 정점의 정보) 있는 정점의 정보를 받아온다.
struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};


// 정점의 변환. (Vertex Shader)
// 1. 정점의 로컬위치를 투영행렬을 곱해놓은 상태까지 변환한다.
// 2. 정점구조체의 멤버를 변경한다.

// VS_MAIN 버텍스 셰이더를 수행하기위한 진입점함수.
// 언제호출? : 정점버퍼를 (인덱스버프를 이용하여)그린다
//리턴타입 VS_MAIN(vector	vPosition : POSITION, vector vNormal : NORMAL, float2	vTexUV : TEXCOORD0)
VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	// 정점 위치 * 월드 * 뷰 * 투영.
	matrix		matWV, matWVP;
	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;

	return Out;
}

// /z  연산을 수행.(원근투영)
// 뷰포트 변환.
// 래스터라이즈를 수행(픽셀의 정보를 생성한다. 픽셀이 만들어진다.)
struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector	vColor : COLOR;
};


// 픽셀의 색을 결정.(Pixel Shader)
PS_OUT PS_MAIN(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	// 픽셀의 색을 결정하는 작업.	
	// tex2D : 텍스쳐의 정보를 담은 샘플러로부터 색을 얻어온다.
	// 1 : 텍스쳐의 정보를 담은 샘플러, 2 : 유브이좌표.
	In.vTexUV = In.vTexUV*	g_fUV;
	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	Out.vColor = vDiffuseColor;

	return Out;
}


// technique : 장치 지원여부에 따른 ㅅㅖ이더 선택을 가능하게 하기위해. 
technique	DefaultDevice
{
	// pass : 기능의 캡슐화(명암 + 카툰 + 그림자)
	//		: 렌더스테이츠를 지정한다. (셰이더의 엔드가 호출될때까지 유효하다.)
	//		: 번역해야할 셰이더 버젼을 지정한다.
	//		: 호출해야할 진입점함수를 결정한다.
	pass Default_Rendering
	{
		ZEnable = true;
		ZWriteEnable = true;
		AlphaTestEnable = false;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	// pass : 기능의 캡슐화(카툰 + 그림자)
	pass Color_Rendering
	{	
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = false;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass UI_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;
		AlphaRef = 0;
		AlphaFunc = Greater;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}


//
//셰이더 비긴
//
//패스 시작(0)
//객체 렌더링
//패스 끝
//
//패스 시작(1)
//객체 렌더링
//패스 끝
//
//
//셰이더 엔드