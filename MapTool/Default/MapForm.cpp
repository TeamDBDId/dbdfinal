// MapForm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MapTool.h"
#include "MapForm.h"
#include "Defines.h"
#include "ToolManager.h"
#include "Base.h"
#include "Management.h"
#include "MapToolView.h"
#include "MainFrm.h"
#include <fstream>
#include <string>
#include "LoadManager.h"
// CMapForm
_USING(Client)

IMPLEMENT_DYNCREATE(CMapForm, CFormView)

CMapForm::CMapForm()
	: CFormView(IDD_MAPFORM)
	, m_fTemp(0)
	, m_IsChangeRot(FALSE)
	, m_IsTempPressing(TRUE)
{
	
}

CMapForm::~CMapForm()
{
}

void CMapForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT2, m_PickingPoint.x);
	DDX_Text(pDX, IDC_EDIT3, m_PickingPoint.y);
	DDX_Text(pDX, IDC_EDIT4, m_PickingPoint.z);
	DDX_Control(pDX, IDC_COMBO1, m_ComboStage);
	DDX_Control(pDX, IDC_EDIT1, m_CMouseState);
	DDX_Text(pDX, IDC_EDIT5, m_vPosition.x);
	DDX_Text(pDX, IDC_EDIT6, m_vPosition.y);
	DDX_Text(pDX, IDC_EDIT7, m_vPosition.z);
	DDX_Text(pDX, IDC_EDIT8, m_vRotation.x);
	DDX_Text(pDX, IDC_EDIT9, m_vRotation.y);
	DDX_Text(pDX, IDC_EDIT10, m_vRotation.z);
	DDX_Text(pDX, IDC_EDIT11, m_vScale.x);
	DDX_Text(pDX, IDC_EDIT12, m_vScale.y);
	DDX_Text(pDX, IDC_EDIT13, m_vScale.z);
	DDX_Text(pDX, IDC_EDIT14, m_OtherOption);
	DDX_Radio(pDX, IDC_RADIO1, m_TypeRadio);
	DDX_Control(pDX, IDC_LIST1, m_pObjectList);
	DDX_Text(pDX, IDC_EDIT15, m_fTemp);
	DDX_Text(pDX, IDC_EDIT16, m_ResetPosition.x);
	DDX_Text(pDX, IDC_EDIT17, m_ResetPosition.z);
	DDX_Check(pDX, IDC_CHECK1, m_IsChangeRot);
	DDX_Check(pDX, IDC_CHECK2, m_IsTempPressing);
	DDX_Check(pDX, IDC_CHECK3, m_IsTempMoveX);
}

BEGIN_MESSAGE_MAP(CMapForm, CFormView)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON4, &CMapForm::OnBnClickedDraw)
	ON_BN_CLICKED(IDC_BUTTON5, &CMapForm::OnBnClickedIdle)
	ON_BN_CLICKED(IDC_BUTTON6, &CMapForm::OnBnClickedChange)
	ON_BN_CLICKED(IDC_BUTTON3, &CMapForm::OnBnClickedResetListBox)
	ON_BN_CLICKED(IDC_RADIO1, &CMapForm::OnBnClickedTypeRadio)
	ON_BN_CLICKED(IDC_RADIO2, &CMapForm::OnBnClickedTypeRadio)
	ON_BN_CLICKED(IDC_RADIO3, &CMapForm::OnBnClickedTypeRadio)
	ON_BN_CLICKED(IDC_BUTTON7, &CMapForm::OnBnClickedDelete)
	ON_EN_CHANGE(IDC_EDIT5, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT6, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT7, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT8, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT9, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT10, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT11, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT12, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT13, &CMapForm::OnEnChangeEdits)
	ON_EN_CHANGE(IDC_EDIT14, &CMapForm::OnEnChangeEdits)
	ON_BN_CLICKED(IDC_BUTTON2, &CMapForm::OnBnClickedSave)
	ON_BN_CLICKED(IDC_BUTTON1, &CMapForm::OnBnClickedReset)
	ON_EN_CHANGE(IDC_EDIT15, &CMapForm::OnBnClickedTypeRadio)
	ON_EN_CHANGE(IDC_EDIT16, &CMapForm::OnBnClickedTypeRadio)
	ON_EN_CHANGE(IDC_EDIT17, &CMapForm::OnBnClickedTypeRadio)
	ON_BN_CLICKED(IDC_CHECK1, &CMapForm::OnBnClickedTypeRadio)
	ON_BN_CLICKED(IDC_CHECK2, &CMapForm::OnBnClickedTypeRadio)
	ON_BN_CLICKED(IDC_CHECK3, &CMapForm::OnBnClickedTypeRadio)
END_MESSAGE_MAP()


// CMapForm 진단입니다.

#ifdef _DEBUG
void CMapForm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMapForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG
_float CMapForm::Round(_float value, int pos)
{
	_float temp;
	temp = value * (_float)pow(10, pos);
	temp = floor(temp + 0.5f);
	temp *= (_float)pow(10, -pos);
	return temp;
}
_vec3 CMapForm::Round(_vec3 value, int pos)
{
	_vec3 temp;
	temp.x = Round(value.x, pos);
	temp.y = Round(value.y, pos);
	temp.z = Round(value.z, pos);
	return temp;
}
void CMapForm::CheckMouseState()
{
	if (m_MouseState == STATE_Idle)
		m_CMouseState.SetWindowTextW(L"STATE_Idle");
	else if (m_MouseState == STATE_Select)
		m_CMouseState.SetWindowTextW(L"STATE_Select");
	else if (m_MouseState == STATE_Draw)
		m_CMouseState.SetWindowTextW(L"STATE_Draw");
	UpdateData(false);
}
void CMapForm::KeyCheck()
{
	if (KEYMGR->KeyDown(DIK_F1))
		OnBnClickedDraw();
	if (KEYMGR->KeyDown(DIK_F2))
		OnBnClickedDelete();
	if (KEYMGR->KeyDown(DIK_F3))
		OnBnClickedChange();
	if (KEYMGR->KeyDown(DIK_F4))
		ResetPosition();

	if (KEYMGR->KeyDown(DIK_GRAVE))
	{
		m_OtherOption = 9;
		OnBnClickedChange();
	}
	if (m_IsTempPressing)
	{
		if (KEYMGR->KeyPressing(DIK_F5))
			PlusPosition();
		if (KEYMGR->KeyPressing(DIK_F6))
			MinusPosition();
	}
	else
	{
		if (KEYMGR->KeyDown(DIK_F5))
			PlusPosition();
		if (KEYMGR->KeyDown(DIK_F6)|| KEYMGR->KeyDown(DIK_F8))
			MinusPosition();
	}

	if (KEYMGR->KeyDown(DIK_F7))
		m_IsTempMoveX = !m_IsTempMoveX;

	if (m_PickingPoint == _vec3(-1.f, -1.f, -1.f))
		return;
	if (KEYMGR->MouseDown(1))
		OnBnClickedIdle();
	if (KEYMGR->MouseDown(0))
		SelectPoint();
	if (KEYMGR->MouseDown(2))
		Set_ResetPoint();
}
void CMapForm::SelectPoint()
{
	if (m_MouseState == STATE_Idle)
	{
		m_SelectObject = GET_INSTANCE(CToolManager)->Get_PickingObject();
		if (m_SelectObject != nullptr)
		{
			m_MouseState = STATE_Select;
			_matrix matWorld = m_SelectObject->Get_Matrix();
			m_vPosition = { matWorld._41,matWorld._42,matWorld._43 };
			m_OtherOption = m_SelectObject->Get_OtherOption();
		}
	}
	else if (m_MouseState == STATE_Draw)
		AddObject();
	else if (m_MouseState == STATE_Select)
	{
		m_vPosition = m_PickingPoint;
		OnBnClickedChange();
	}
}
void CMapForm::AddObject()
{

	_int index = m_pObjectList.GetCurSel();
	if (m_pObjectList.GetCurSel() < 0)
		return;
	GET_INSTANCE_MANAGEMENT;

	m_vPosition = m_PickingPoint;
	CString ProtoName = L"";
	m_pObjectList.GetText(index, ProtoName);

	if (m_TypeRadio == 0)
		pManagement->Add_GameObjectToLayer(L"GameObject_StaticMap", SCENE_STAGE, L"Layer_Map_Static", &m_SelectObject);
	else if (m_TypeRadio == 1)
		pManagement->Add_GameObjectToLayer(L"GameObject_NaviMap", SCENE_STAGE, L"Layer_Map_Navi", &m_SelectObject);
	else
		pManagement->Add_GameObjectToLayer(ProtoName, SCENE_STAGE, L"Layer_GameObject", &m_SelectObject);

	_matrix matWorld = Compute_Matrix();
	m_SelectObject->Set_Base(ProtoName, matWorld, m_OtherOption);
	m_MouseState = STATE_Select;

	//m_SelectObject = nullptr;
	//m_MouseState = STATE_Draw;

	Safe_Release(pManagement);
}
_matrix CMapForm::Compute_Matrix()
{
	_matrix matTransform, matScale, matRotaion, matRotationtemp;

	D3DXMatrixScaling(&matScale, m_vScale.x, m_vScale.y, m_vScale.z);

	//D3DXMatrixRotationX(&matRotationtemp, D3DXToRadian(m_vRotation.x));
	//matRotaion = matRotationtemp;
	D3DXMatrixRotationY(&matRotationtemp, D3DXToRadian(m_vRotation.y));
	matRotaion = matRotationtemp;
	//D3DXMatrixRotationZ(&matRotationtemp, D3DXToRadian(m_vRotation.z));
	//matRotaion *= matRotationtemp;

	D3DXMatrixTranslation(&matTransform, m_vPosition.x, m_vPosition.y, m_vPosition.z);
	return _matrix(matScale * matRotaion * matTransform);
}
void CMapForm::ResetPosition()
{
	if (m_MouseState != STATE_Select)
		return;
	m_vPosition = m_ResetPosition;
	OnBnClickedChange();
	UpdateData(false);
}
void CMapForm::PlusPosition()
{
	if (m_MouseState != STATE_Select)
		return;
	if (m_IsTempMoveX)
		m_vPosition += { m_fTemp, 0.f, 0.f };
	else
		m_vPosition += { 0.f, 0.f, m_fTemp };
	OnBnClickedChange();
	UpdateData(false);
}
void CMapForm::MinusPosition()
{
	if (m_MouseState != STATE_Select)
		return;
	if (m_IsTempMoveX)
		m_vPosition -= { m_fTemp, 0.f, 0.f };
	else
		m_vPosition -= { 0.f, 0.f, m_fTemp };
	OnBnClickedChange();
	UpdateData(false);
}
void CMapForm::Set_ResetPoint()
{
	m_ResetPosition = m_vPosition;
	UpdateData(false);
}
_matrix CMapForm::Compute_Matrix_OnlyPos()
{
	_matrix matWorld, matScale, matRotaion, matRotationtemp;

	matWorld = m_SelectObject->Get_Matrix();

	matWorld._41 = m_vPosition.x;
	matWorld._42 = m_vPosition.y;
	matWorld._43 = m_vPosition.z;
	return matWorld;
}




// CMapForm 메시지 처리기입니다.


void CMapForm::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	SetTimer(0, 1, 0);

	CString Num;
	for (_int i = 0; i < 5; i++)
	{
		Num.Format(_T("%d"), i + 1);
		m_ComboStage.AddString(Num);
	}
	m_ComboStage.SetCurSel(0);
	UpdateData(false);

	CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	m_pMapToolView = dynamic_cast<CMapToolView*>(pMainFrm->m_MainSplt.GetPane(0, 0));

}


void CMapForm::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nIDEvent)
	{
	case 0:
		if (m_pMapToolView == nullptr)
			return;
		if (!m_pMapToolView->MouseCheck())
			m_PickingPoint = Round(GET_INSTANCE(CToolManager)->Get_PickingPoint(), 2);
		else
			m_PickingPoint = { -1.f,-1.f,-1.f };
		CheckMouseState();
		KeyCheck();
		UpdateData(false);
		break;
	}
	
	CFormView::OnTimer(nIDEvent);
}


void CMapForm::OnBnClickedDraw()
{
	if (m_MouseState == STATE_Select)
		return;
	m_MouseState = STATE_Draw;
	m_SelectObject = nullptr;
}


void CMapForm::OnBnClickedIdle()
{
	m_MouseState = STATE_Idle;
	m_SelectObject = nullptr;
}


void CMapForm::OnBnClickedChange()
{
	if (m_SelectObject == nullptr || m_MouseState != STATE_Select)
		return;
	_matrix matWorld;
	if (m_IsChangeRot)
		matWorld = Compute_Matrix();
	else
		matWorld = Compute_Matrix_OnlyPos();
	m_SelectObject->Set_Base(nullptr, matWorld, m_OtherOption);
}


void CMapForm::OnBnClickedResetListBox()
{
	map<const _tchar*, CGameObject*> mapPrototype = GET_INSTANCE(CManagement)->Get_mapPrototype();
	map<const _tchar*, CComponent*>* mapComPrototype = GET_INSTANCE(CManagement)->Get_mapComPrototype();

	m_pObjectList.ResetContent();
	const _tchar* Name;

	if (m_TypeRadio == TYPE_OBJECT)
	{
		Name = L"GameObject_Map";
		for (auto& iter : mapPrototype)
		{
			if (!wcsncmp(iter.first, Name, 14))
				m_pObjectList.AddString(iter.first);
		}
	}
	else
	{
		Name = m_TypeRadio == TYPE_MAP ? L"Map_Static" : L"Map_Navi";
		for (auto& iter : mapComPrototype[2])
		{
			if (!wcsncmp(iter.first, Name, 8))
				m_pObjectList.AddString(iter.first);
		}
	}
	UpdateData(false);
}


void CMapForm::OnBnClickedTypeRadio()
{
	UpdateData(true);
}


void CMapForm::OnBnClickedDelete()
{
	if (m_MouseState != STATE_Select || m_SelectObject == nullptr)
		return;

	m_SelectObject->SetDead();
	m_MouseState = STATE_Idle;
}


void CMapForm::OnEnChangeEdits()
{
	UpdateData(true);
}


void CMapForm::OnBnClickedSave()
{
	wofstream fout;

	_tchar Path[256] = L"";
	swprintf_s(Path, L"../Bin/Data/Map/Stage%d.txt", m_ComboStage.GetCurSel() + 1);
	fout.open(Path);
	if (fout.fail())
		return;

	GET_INSTANCE_MANAGEMENT;

	map<const _tchar*, CLayer*>* mapLayers = pManagement->Get_mapLayers();

	for (auto& iter : mapLayers[2])
	{
		_int ObjectNum = -1;
		if (iter.first == L"Layer_Map_Static")
			ObjectNum = 0;
		else if (iter.first == L"Layer_Map_Navi")
			ObjectNum = 1;
		else if (iter.first == L"Layer_GameObject" || iter.first == L"Layer_Path" || iter.first == L"Layer_InvisibleCol")
			ObjectNum = 2;
		else
			continue;
		list<CGameObject*>* ObjList = pManagement->Get_ObjList(2, iter.first);
		for (auto& Layer : *ObjList)
		{
			wstring Key  =Layer->Get_Key();
			_int OtherOption = Layer->Get_OtherOption();
			_matrix matWorld = Layer->Get_Matrix();
			 
			fout << matWorld._11 << '|' << matWorld._12 << '|' << matWorld._13 << '|' << matWorld._14 << '|'
				<< matWorld._21 << '|' << matWorld._22 << '|' << matWorld._23 << '|' << matWorld._24 << '|'
				<< matWorld._31 << '|' << matWorld._32 << '|' << matWorld._33 << '|' << matWorld._34 << '|'
				<< matWorld._41 << '|' << matWorld._42 << '|' << matWorld._43 << '|' << matWorld._44 << '|';
			fout << Key << '|' << OtherOption << endl;
		}
	}
	Safe_Release(pManagement);
	fout.close();
}


void CMapForm::OnBnClickedReset()
{
	GET_INSTANCE_MANAGEMENT;
	pManagement->Clear_Layers(2);
	GET_INSTANCE(CLoadManager)->LoadMapData(m_ComboStage.GetCurSel());
	m_MouseState = STATE_Idle;
	m_SelectObject = nullptr;
	Safe_Release(pManagement);
}