#include "stdafx.h"
#include "..\Headers\Terrain.h"
#include "Management.h"
#include "Light_Manager.h"

_USING(Client)

CTerrain::CTerrain(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CTerrain::CTerrain(const CTerrain & rhs)
	: CGameObject(rhs)
{

}


HRESULT CTerrain::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CTerrain::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pTransformCom->SetUp_RotationX(D3DXToRadian(90));
	return NOERROR;
}

_int CTerrain::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	
	return _int();
}

_int CTerrain::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	//m_pBufferCom->Culling_ToFrustum(m_pFrustumCom, m_pTransformCom->Get_Matrix());

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CTerrain::Render_GameObject()
{
	if (nullptr == m_pBufferCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	if (FAILED(SetUp_ConstantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(0);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CTerrain::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_iOtherOption = OtherOption;
	if (m_iOtherOption == 0)
		m_iUV[0] = m_iUV[1] = 1;
	else if (m_iOtherOption == 1 || m_iOtherOption == 5)
		m_iUV[0] = m_iUV[1] = 2;
	else if (m_iOtherOption == 2)
		m_iUV[0] = m_iUV[1] = 4;
	else if (m_iOtherOption == 3)
		m_iUV[0] = m_iUV[1] = 8;
	else if (m_iOtherOption == 4)
		m_iUV[0] = m_iUV[1] = 12;

	m_pTransformCom->Scaling(400.f*m_iUV[0], 400.f * m_iUV[1], 1.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(matWorld._41, matWorld._42, matWorld._43));


	if (Key == nullptr)
		return;
	m_Key = Key;
	GET_INSTANCE_MANAGEMENT
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Texture_Terrain");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return;



	Safe_Release(pManagement);
}

_matrix CTerrain::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CTerrain::Get_Key()
{
	return m_Key.c_str();
}

_int CTerrain::Get_OtherOption()
{
	return m_iOtherOption;
}

HRESULT CTerrain::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (nullptr == m_pTransformCom)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;	

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Shader_Default");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CTerrain::SetUp_ConstantTable(LPD3DXEFFECT pEffect)
{
	if (FAILED(m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld")))
		return E_FAIL;

	_matrix		matView, matProj;
	_vec2		vUV = {(_float)m_iUV[0], (_float)m_iUV[1]};

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);
	pEffect->SetFloatArray("g_fUV", vUV, 2);
	if (FAILED(m_pTextureCom->SetUp_OnShader(pEffect, "g_DiffuseTexture", 0)))
		return E_FAIL;

	D3DMATERIAL9	MtrlInfo;
	m_pGraphic_Device->SetMaterial(&MtrlInfo);

	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CTerrain * CTerrain::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CTerrain*	pInstance = new CTerrain(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CTerrain Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CTerrain::Clone_GameObject()
{
	CTerrain*	pInstance = new CTerrain(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CTerrain Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CTerrain::Free()
{
	//Safe_Release(m_pNaviCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pFrustumCom);

	CGameObject::Free();
}
