#pragma once
#include "Base.h"

_BEGIN(Engine)
class CManagement;
_END

_BEGIN(Client)
class CEffectManager : public CBase
{
	_DECLARE_SINGLETON(CEffectManager)

public:
	enum eEffect { E_Fire, E_Mist,E_Generator,E_Fail,E_Exit, E_ExitOpen, E_Leaf,E_Change,E_Jump,E_Death, E_FootPrint,E_Blood,E_BG_GENON, E_BG_HOOKON,E_BloodUI,E_BloodSpread,E_HookDeath};
private:
	explicit CEffectManager();
	~CEffectManager() = default;

public:
	HRESULT Ready_Effect(LPDIRECT3DDEVICE9 _pGDevice);
	HRESULT Ready_PlayerPos(const _vec3* _pPos);

public:
	void Make_Effect(const eEffect& _eEffect, const _vec3 & _vPos = _vec3(), const _uint& _iType = 0);

public:
	void Make_Mist(const _float& _fTime);
	void Make_BG(const eEffect& _eEffect,const _matrix& _matWorld,void* _pCamper = nullptr);
	void Make_BloodSpread(const _vec3 & _vPos, const _vec3 & _vDir);
private:
	void Make_Fire(const _vec3 & _vPos, const _uint& _fSize);

	void Make_Generator(const _vec3 & _vPos);

	void Make_Fail(const _vec3 & _vPos);

	void Make_Exit(const _vec3 & _vPos);
	void Make_ExitOpen(const _vec3 & _vPos);

	void Make_Leaf(const _vec3 & _vPos);

	void Make_Change(const _vec3 & _vPos);

	void Make_Jump(const _vec3 & _vPos);

	void Make_Death(const _vec3 & _vPos);


	void Make_FootPrint(const _vec3 & _vPos);

	void Make_Blood(const _vec3 & _vPos);
	void Make_BloodUI();

	void Make_Hook(const _vec3 & _vPos);
	
private:
	virtual void Free() override;


private:

	CManagement*			m_pManagement = nullptr;

	_float					m_fMist = 0.f;
	const _vec3*			m_pPos = nullptr;
};

_END