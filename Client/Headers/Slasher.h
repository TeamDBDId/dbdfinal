#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"
#include "AnimationKey.h"
#include "Animation_Slasher.h"
#include "Input_Device.h"

_BEGIN(Engine)
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CTexture;
_END

_BEGIN(Client)
class CCustomLight;
class CSlasher final : public CGameObject
{
#define	SLASHER_ANGER_RANGE 2000.f

#define WRATIH_NOSKILL		0
#define WRAITH_HIDESKILL	1
#define WRAITH_UNHIDESKILL	2
#define WRAITH_HIDEINGSKILL	3

#define SPIRIT_NOSKILL		0
#define SPIRIT_SKILLUSE		1

#define ATTACK_IN			101
#define ATTACK_SWING		102
#define ATTACK_MISS			103
#define ATTACK_BOW			104
#define ATTACK_WIPE			105
#define CATTACK_IN			106
#define CATTACK_SWING		107
#define CATTACK_MISS		108
#define CATTACK_BOW			109

#define WRAITH_SPEED 403.f
#define WRAITH_CLOAKED_SPEED 504.f
#define WRAITH_HIDE_TIME 1.5f
#define WRAITH_UNHIDE_TIME 3.f
#define WRAITH_TERROR_RADIUS 3200.f
#define WRAITH_BELL_SOUND_RANGE 2400.f
#define WRAITH_HIDE_SOUND_RANGE 4000.f

#define SPIRIT_SPEED 400.f
#define SPIRIT_PHASE_WALK_SPEED 734.f
#define SPIRIT_IN_TRAVERSE_TIME 1.f
#define SPIRIT_MAX_TRAVERSE_TIME 5.f
#define SPRIIT_POWER_BAR 15.f
#define SPIRIT_TERROR_RADIUS 2400.f
public:
	enum CHARACTER { C_WRAITH, C_SPIRIT,C_END };
private:
	explicit CSlasher(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CSlasher(const CSlasher& rhs);
	virtual ~CSlasher() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_Stemp();
	virtual void Render_Stencil();
	virtual void Render_ShadowMap(_matrix* VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_CubeMap(_matrix* View, _matrix* Proj);
	void Render_SpiritAfterImage();
public:
	virtual _int Do_Coll(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
	virtual _int Do_Coll_Player(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
	void RotateY(const _float& fTimeDelta);
private:
	void Update_Breath(const _float& fDeltaTime);
	void CommunicationWithServer();
	void Compute_Map_Index();
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
	HRESULT Ready_Action();
public:
	const _matrix* Get_CamMatrix() { return m_pCamMatrix; }
	const _matrix* Get_LHandMatrix() { return m_pLHandMatrix; }
	const _matrix* Get_RHandMatrix() { return m_pRHandMatrix; }
	const _matrix* Get_HeadMatrix() { return m_pHeadMatrix; }
	const _matrix* Get_CamperAttachMatrix() { return m_pCamperAttachMatrix; }
	const _matrix* Get_CarryMatrix() { return m_pCarryMatrix; }
	_bool	GetLockKey() { return m_bIsLockKey; }
	AS::STATE Get_State() { return m_eCurState; }
	_float	Get_DissolveTime() { return m_fDissolveTime; }
	_float	Get_Height() { return m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y; }
	_vec3	Get_Pos() { return *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION); }
	_bool	Get_UseSkill() { return m_bSkillUse; }
	_bool	Get_Stealth() { return m_bSkill; }
	_float	Get_AngerGauge() { return m_fAngerGauge; }
	CHARACTER Get_Character() {	return m_iCharacter; }
	void Change_Mesh(_uint iNum);
	_float	Get_SkillTime() { return m_fSkillTime; }
private:
	void Set_StartPosition();
	void SetUp_Sound();
	void Update_Sound();
	void Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundDistance = 1700.f, const _float& SoundValue = 1.f, const _int& OtherOption = 0);
	list<SoundData>			m_SoundList;
	_uint					m_CurChannel = 0;
public:
	HRESULT Set_Index(_uint index);
	void Set_State(AS::STATE eState) { m_eCurState = eState; m_eCurLowerState = eState; State_Check(); LowerState_Check(); }
	void Set_UpperState(AS::STATE eState) { m_eCurState = eState; State_Check(); }
	void Set_LowerState(AS::STATE eState) { m_eCurLowerState = eState; LowerState_Check(); }
	void SetColl(_bool IsColl) { m_isColl = IsColl; }
	void IsLockKey(_bool IsLock) { m_bIsLockKey = IsLock; }
	_bool IsCarried() { return m_bIsCarry; }
	void Set_RendHead(_bool IsRend) { m_bRendHead = IsRend; }
	void Set_Weapon(CGameObject* pWeapon) { m_pWeapon = pWeapon; }
	void Set_WeaponColl(_bool bColl);
	void Set_Carry(_bool bCarry) { m_bIsCarry = bCarry;}
	void Set_CarriedCamper(CGameObject* pCamper);
	void Set_Penetration(_float fTime) { m_fPenetrationTime = fTime; }
	void Set_PColor(_vec4 Color) { m_Color = Color; }
	void Set_MoriCamper(CGameObject* pMori) { m_pMoriCamper = pMori; }
	void Set_AngerGauge(_float fTime) { m_fAngerGauge = fTime; }
	void Init_SkiilCoolTime() { m_fSkillCoolTime = 0.f; }
	CGameObject* Get_CarriedCamper() { return m_pCarriedCamper; }
	CGameObject* GetTargetOfInteraction() { return m_pTargetOfInteraction; }
	_bool IsLook() { return m_isLook; }
private:
	void State_Check();
	void LowerState_Check();
	void Key_Input(const _float& fTimeDelta);
	void Use_Skill_Wraith(const _float& fTimeDelta);
	void Use_Skill_Spirit(const _float& fTimeDelta);
	void Init_Camera();
	void Interact_Object();
	void Cal_LightPos();
	void Check_AngerGauge();
	_bool Check_CampersState();
	void Processing_Others(const _float& fTimeDelta);
	_bool Find_FPVAnim(WSTATE Anim);
	void Check_AfterImage(const _float& fTimeDelta);
	void Check_Spirit_MoriEffect();
	void Check_Wraith_MoriEffect();
	WSTATE Change_Animation_Wriath(AS::STATE Anim);
	SSTATE Change_Animation_Spirit(AS::STATE Anim);
	void Change_TimeOfAnimation(const _float & fTimeDelta);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CGameObject*		m_pWeapon = nullptr;
	CGameObject*		m_pCamperOfInteraction = nullptr;
	CCustomLight*		m_pCustomLight = nullptr;
	CCollider*			m_pHeadColliderCom = nullptr;
private:
	_float				m_fSpeedUpTime = 0.f;
	_bool				m_bSpeedUp = false;
	_int				tst = 0;
	_uint				m_iMoriCount = 0;
	_uint				m_iMoriSound = 0;
	_float				m_fRatio = 0.0f;
	_uint				m_iSoundCheck = 0;
	_float				m_fMoriTime = 0.f;
	_bool				m_bNotRenderSlasher = false;
	_float				m_fSpiritNoise = 0.f;
	_uint				m_iCurAnger = 0;
	_bool				m_bCanGoOut = false;
	_float				m_fPenetrationTime = 0.f;
	_vec4				m_Color = { 1.0f,0.f,0.f,0.f };
	_float				m_fAngerGauge = 0.f;
	_bool				m_bHairSwitch = false;
	_float				m_fHairMove = 0.f;
	_matrix				m_matSpritMatrix = _matrix();
	_int				m_iOldSkill = 0;
	_uint				m_iSkillCount = 0;
	_bool				m_bAfterImaged = false;
	_float				m_fSpiritSkillTime = 0.f;
	_float				m_fSpiritChargingTime = 0.f;
	_float				m_fMoveSpeed = 0.f;
	_float				m_fRotateSpeed = 0.f;
	CHARACTER			m_iCharacter = C_WRAITH;
	_float				m_fTimeDelta = 0.f;
	_float				m_fSkillCoolTime = 0.f;
	_float				m_fDissolveTime = 0.f;
	_float				m_fWiggleTime = 0.f;
	_bool				m_bRendHead = true;
	_bool				m_bSkillUse = false;
	_bool				m_bSkill = false;
	_bool				m_bFlagOfKeyInput = false;
	_float				m_fSkillTime = 0.f;
	_float				m_fSTime = -1.f;
	_bool				m_bIsLockKey = false;
	_bool				m_bLaitInit = false;
	_bool				m_bIsCarry = false;
	_bool				m_bMoveLock = false;
	_bool				m_bIsNoBlending = false;
	_vec3				m_vTargetPos;
	_vec3				m_vTargetCamperPos;
	_float				m_fAnimationIndex = 0.f;
	vector<_uint>		m_vecFPVAnim;
	CInput_Device*		m_pInput_Device = nullptr;
	CGameObject*		m_pCarriedCamper = nullptr;
	CGameObject*		m_pMoriCamper = nullptr;
	CGameObject*		m_pTargetOfInteraction = nullptr;
	list<CGameObject*>  m_CollObjList;
	_matrix				m_matTemp;
	const _matrix*		m_pCarryMatrix = nullptr;
	const _matrix*		m_pCamMatrix = nullptr;
	const _matrix*		m_pRHandMatrix = nullptr;
	const _matrix*		m_pLHandMatrix = nullptr;
	const _matrix*		m_pHeadMatrix = nullptr;
	const _matrix*		m_pCamperAttachMatrix = nullptr;
	//LPDIRECT3DTEXTURE9	m_pSkillTexture = nullptr;
	list<_matrix>		m_pSkillWorld;
	list<vector<_uint>>			m_pFrameNum;
	list<vector<_matrix*>>	m_pSkillMatrix;
	list<_float>		m_pDisapearTime;
	_float				m_BreathTime = 0.f;
	_bool				m_SkillBreath = false;
private:
	AS::STATE	m_eOldState = AS::Idle;
	AS::STATE	m_eCurState = AS::Idle;
	AS::STATE	m_eOldLowerState = AS::Idle;
	AS::STATE	m_eCurLowerState = AS::Idle;
public:
	static CSlasher* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
};

_END