#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_Logo : public CGameObject
{
private:
	explicit CUI_Logo(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_Logo(const CUI_Logo& rhs);
	virtual ~CUI_Logo() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);

private:
	void Set_Sound();
	void Set_BaseTexture();
	void Check_LogoFadeEnd();
	void Check_StateTexture();
private:
	CUI_Texture*	m_pLogoTexture = nullptr;
	CUI_Texture*	m_pStateTexture = nullptr;

	_bool			m_LogoFadeEnd = false;
public:
	static CUI_Logo* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END