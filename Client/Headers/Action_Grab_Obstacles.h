#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCamper;
class CAction_Grab_Obstacles final : public CAction
{
public:
	explicit CAction_Grab_Obstacles();
	virtual ~CAction_Grab_Obstacles() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void Send_ServerData() override;
public:
	void SetCamper(CGameObject* pCamper) { m_pCamper = (CCamper*)pCamper; }
private:
	_bool m_bIsInit = false;
	_float m_fDelay = 0.f;
	_vec3 m_vOriginPos;
	_int m_iState = 0;
	_float m_fIndex = 0.f;
	vector<_vec3>	m_vecPosBK;
	vector<_vec3>	m_vecPosFT;
	vector<_vec3>	m_vecSpiritPosBK;
	CCamper*		m_pCamper = nullptr;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END