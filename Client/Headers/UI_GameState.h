#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CCamper;
class CUI_Texture;
class CUI_GameState : public CGameObject
{
public:
	enum CamperIconState{ UNCONNECTED = 9, HEALTHY = 0, INJURED =1 , DYING = 2, HOOKED = 5, HOOKDEAD = 6, BLOODDEAD = 7, ESCAPE = 8 };
private:
	explicit CUI_GameState(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_GameState(const CUI_GameState& rhs);
	virtual ~CUI_GameState() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	void	Set_Rendering(_bool IsRendering);
	void	Set_Delete();
private:
	HRESULT Set_CamperStateUI();
	HRESULT Set_ExitState();
	HRESULT Set_BasicUI();
	void	Update_Generator();
	void	Check_CamperState();
	void	Update_CamperState();
	void	Update_ChangeState();
private:
	CUI_Texture*	m_BaseUI;
	CUI_Texture*	m_pCamperBlood[4];
	CUI_Texture*	m_pCamperSmoke[4];
	CUI_Texture*	m_pCamperState[4];
	CUI_Texture*	m_pCamperHpBase[4];
	CUI_Texture*	m_pCamperHpBar[4];
	CUI_Texture*	m_pCamperName[4];
	CUI_Texture*	m_pExitState[2];

	_float			m_fChangeState[4];
	_float			m_fChangeExit = 0.f;
	CamperIconState m_CurState[4];
	CamperIconState m_OldState[4];
	_int m_iCamperNum = 4;
	_int m_iLeaveGenerator = 5;
	_bool	m_bUI = false;
public:
	static CUI_GameState* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END