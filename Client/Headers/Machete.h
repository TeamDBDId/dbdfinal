#pragma once
#include "GameObject.h"
_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Static;
class CCollider;
class CPicking;
class CFrustum;
_END


_BEGIN(Client)

class CMachete : public CGameObject
{
private:
	explicit CMachete(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMachete(const CMachete& rhs);
	virtual ~CMachete() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_Stemp();
public:
	HRESULT Set_Index(_uint iIndex);
	void Set_SpiritAttack(_bool bAtt) { m_bSpiritDissolve = bAtt; }
	void Set_RenderAfterImage(_bool bRender) { m_bRenderAfterImage = bRender; }
	void Set_Render(_bool bRender) { m_bRender = bRender; }
	void Set_CollState(_bool bColl) { m_isColl = bColl; }
	void Set_UseSkill(_bool bSkill) { m_bSkillUse = bSkill; }
	void Set_DissolveTime(_float fTime) { m_fDissolveTime = fTime; }
	virtual _int Do_Coll(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
	CGameObject* Get_BeHitObject() { return m_pBeHitObject; }
	void Init_BeHitObject() { m_pBeHitObject = nullptr; }
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	_bool				m_bRender = false;
	const _matrix*		m_pRHandMatrix = nullptr;
	_matrix				m_matParent;
	CTransform*			m_pSlasherTransform = nullptr;
	_bool				m_bSkillUse = false;
	_float				m_fDissolveTime = 0.f;
	_float				m_fHeight = 0.f;
	_float				m_fSkillTime = 0.f;
	_bool				m_bSpiritDissolve = 0.f;
	CGameObject*		m_pBeHitObject = nullptr;
	CCollider*			m_pAfterImageCollider[2] = { nullptr, nullptr };
	_bool				m_bRenderAfterImage = false;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
	void Approach_Slasher();
	_bool SkillTime_Check();
	void Check_SpiritAttack(const _float& fTimeDelta);
	void Make_AfterImage();
public:
	static CMachete* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
