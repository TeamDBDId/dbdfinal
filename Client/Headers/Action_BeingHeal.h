#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCamper;
class CAction_BeingHeal final : public CAction
{
public:
	explicit CAction_BeingHeal();
	virtual ~CAction_BeingHeal() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetCamper(CGameObject* pCamper) { m_pCamper = (CCamper*)pCamper; }
private:
	_int		m_iState = 0;
	_float		m_fDelay = -1.f;
	_float		m_fEndDelay = 0.f;
	_float		m_fTime = 0.f;
	_bool		m_bSkillFail = false;
	CCamper*	m_pCamper = nullptr;
protected:
	virtual void Free();
};

_END