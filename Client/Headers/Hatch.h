#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CFrustum;
_END

_BEGIN(Client)

class CHatch final : public CGameObject
{
public:
	enum STATE { Open, KillerClose, IdleOpen, IdleClose, Close };
private:
	explicit CHatch(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CHatch(const CHatch& rhs);
	virtual ~CHatch() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_Stemp();
	STATE Get_CurState() { return m_CurState; }
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
private:
	void State_Check();
	void Hatch_Check(const _float& fTimeDelta);
	void Position_Check();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	wstring m_Key;
	_int m_iOtherOption = 0;
	_float m_fTime = 0.0f;
	_int m_iNumber = 0;
	_bool m_bPos = false;
	STATE m_OldState = IdleClose;
	STATE m_CurState = IdleClose;
	_bool m_isRendering = false;
	_vec3 vMyPos = _vec3();
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
	void ComunicateWithServer();
public:
	static CHatch* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END