#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCamper;
class CSlasher;
class CAction_Drop final : public CAction
{
public:
	explicit CAction_Drop();
	virtual ~CAction_Drop() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
private:
	_bool m_bIsInit = false;
	_vec3 m_vOriginPos;
	_float	m_fIndex = 0.f;
	_int m_iState = 0;
	_float	m_fDelay = 0.f;
	vector<_vec3>	m_vecPos;
	CCamper*		m_pCamper = nullptr;
	CSlasher*		m_pSlasher = nullptr;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END