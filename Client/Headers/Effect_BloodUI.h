#pragma once

#include "Defines.h"
#include "BaseEffect.h"


_BEGIN(Client)
class CEffect_BloodUI final : public CGameObject
{
private:
	explicit CEffect_BloodUI(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_BloodUI(const CEffect_BloodUI& _rhs);
	virtual ~CEffect_BloodUI() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

private:
	void Make_Drop(const _float& _fTick);
	void Make_DropBig(const _float& _fTick);
	void Make_DropAnim(const _float& _fTick);
	void Make_Anim(const _float& _fTick);

	
public:
	static CEffect_BloodUI* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

private:
	_float			DROPDELAY = 0.1f;
	_float			DROPBIGDELAY = 0.75f;
	_float			DROPANIMDELAY = 0.2f;
	_float			ANIMDELAY = 0.51f;
	
	_float			m_fLifeTime = 0.f;

	_float			m_fDropTime = 0.f;
	_float			m_fDropBigTime = 0.f;
	_float			m_fDropAnimTime = 0.f;
	_float			m_fAnimTime = 0.f;

};

_END