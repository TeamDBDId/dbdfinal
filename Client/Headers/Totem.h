#pragma once
#include "GameObject.h"
_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Static;
class CCollider;
class CFrustum;
_END


_BEGIN(Client)
class CCustomLight;
class CTotem : public CGameObject
{
private:
	explicit CTotem(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CTotem(const CTotem& rhs);
	virtual ~CTotem() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_Stemp();
	virtual void Render_Stencil();
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
private:
	void Update_Sound();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
	CCustomLight*		m_pCustomLight = nullptr;
	CCollider*			m_pLightCollider = nullptr;
private:
	void Init_Light();
private:
	_int	m_iRuin = 0;
	_float	m_Max = 0.f;
	wstring m_Key = L"";
	_bool m_bDissolve = false;
	_float m_fDissolveTime = 0.f;
	_int	m_SoundCheck = 0;
	_bool	m_bCurse = false;
	_bool m_LateInit = false;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
	void ComunicateWithServer();
public:
	static CTotem* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
