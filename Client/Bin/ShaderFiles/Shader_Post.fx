texture			g_DiffuseTexture;

sampler	DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct PS_IN
{
	vector		vPosition : POSITION;
	float2		vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector		vColor : COLOR0;
};

float3 SharpColor(float3 InColor, float2 vUV)
{
	float3 Blur = InColor;
	float2 rcpres = { 0.001f, 0.001f };

	Blur += tex2D(DiffuseSampler, float2(vUV.x, vUV.y + rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x, vUV.y - rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x + rcpres.x, vUV.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x - rcpres.x, vUV.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x - rcpres.x, vUV.y + rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x - rcpres.x, vUV.y - rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x + rcpres.x, vUV.y + rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x + rcpres.x, vUV.y - rcpres.y)) * 0.25;

	Blur /= 3;

	return lerp(Blur, InColor, 3);
}

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector vDiff = tex2D(DiffuseSampler, In.vTexUV);
	//if (vDiff.x < 0.9f || vDiff.y < 0.9f || vDiff.z < 0.9f)
	//{
	//	vDiff.xyz = SharpColor(vDiff.xyz, In.vTexUV);
	//}
	Out.vColor = vDiff;

	return Out;
}

technique	DefaultDevice
{
	pass Render_Blend
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0;

		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}


