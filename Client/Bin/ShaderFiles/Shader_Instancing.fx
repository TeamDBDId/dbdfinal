matrix		g_matWorld, g_matVP;
texture		g_DiffuseTexture;
texture		g_NormalTexture;
texture		g_CubeTexture;
texture		g_AOTexture;
texture		g_MetalicTexture;
texture		g_RoughnessTexture;
texture		g_NoiseTexture;
vector		g_DiffuseColor = vector(0.7f, 0, 0, 1);

bool		g_isFootPrint = true;

sampler NoiseSampler = sampler_state
{
	texture = g_NoiseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler NormalSampler = sampler_state
{
	texture = g_NormalTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler CubeSampler = sampler_state
{
	texture = g_CubeTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler MetalSampler = sampler_state
{
	texture = g_MetalicTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler RoughSampler = sampler_state
{
	texture = g_RoughnessTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler AOSampler = sampler_state
{
	texture = g_AOTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_SkinTexture;
sampler SkinSampler = sampler_state
{
	texture = g_SkinTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_LightEmissive;
sampler LESampler = sampler_state
{
	texture = g_LightEmissive;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

struct VS_IN
{
	float3	vPosition	: POSITION;
	float3	vNormal		: NORMAL;
	float2	vTexUV		: TEXCOORD0;
	float3	vTangent	: TANGENT;
	float3	vRight		: TEXCOORD1;
	float3	vUp			: TEXCOORD2;
	float3	vLook		: TEXCOORD3;
	float3	vPos		: TEXCOORD4;
};

struct VS_OUT_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	vector	vTangent	: TANGENT;
};

VS_OUT_PHONG VS_MAIN_PHONG(VS_IN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matW, matWVP;

	matW = float4x4(float4(In.vRight, 0.f), float4(In.vUp, 0.f), float4(In.vLook, 0.f), float4(In.vPos, 1.f));
	matWVP = mul(matW, g_matVP);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), matW));
	Out.vTangent = normalize(mul(vector(In.vTangent, 0.f), matW));
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;

	return Out;
}

struct PS_IN_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	vector	vTangent	: TANGENT;
};

struct PS_OUT
{
	vector	vDiffuse : COLOR0;
	vector	vNormal : COLOR1;
	vector	vDepth : COLOR2;
	vector	vRMAO : COLOR3;
};

PS_OUT PS_MAIN_PHONG(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, g_isFootPrint, 0.f);
	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz = AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse *= AO;

	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	return Out;
}

PS_OUT PS_MAIN_PHONG_CROSS(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.vDiffuse = tex2D(DiffuseSampler, In.vTexUV);
	Out.vDiffuse.xyz = max(6.10352e-5, Out.vDiffuse.xyz);
	Out.vDiffuse.xyz > 0.04045 ? pow(Out.vDiffuse.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Out.vDiffuse.xyz * (1.0 / 12.92);

	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.f, 0.f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz = AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);
	
	Out.vDiffuse *= 0.5f;
	Out.vDiffuse *= AO;


	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;


	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);


	return Out;
}

technique	DefaultDevice
{
	pass Phong // �ȼ��ܿ��� �� ����.
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}

	pass CrossBuffer
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		CullMode = none;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG_CROSS();
	}
}