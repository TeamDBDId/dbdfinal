
vector			g_vScreenLightPos = vector(0, 0, 0, 0);

texture			g_DiffuseTexture;
texture			g_MaskingTex;
texture			g_FogTex;
texture3D		g_LutTex;
float			g_foguv;
float			g_E = 45.f;
float			g_W = 44.f;

float			g_R = 0.5f;
float			g_G = 0.5f;
float			g_B = 0.5f;

vector			g_ssao;
bool			g_bssao;

sampler	LutSampler = sampler_state
{
	texture = g_LutTex;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

sampler	FogSampler = sampler_state
{
	texture = g_FogTex;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler	MaskingSampler = sampler_state
{
	texture = g_MaskingTex;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler	DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_ShadeTexture;

sampler	ShadeSampler = sampler_state
{
	texture = g_ShadeTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};


texture			g_SpecularTexture;

sampler	SpecularSampler = sampler_state
{
	texture = g_SpecularTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_RefTexture;

sampler	RefSampler = sampler_state
{
	texture = g_RefTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_LightTexture;

sampler	UserMapSampler = sampler_state
{
	texture = g_LightTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_EtcTexture;

sampler	EtcSampler = sampler_state
{
	texture = g_EtcTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_DepthTexture;

sampler	DepthSampler = sampler_state
{
	texture = g_DepthTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_RandomTexture;

sampler	RandomSampler = sampler_state
{
	texture = g_RandomTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct PS_IN
{
	vector		vPosition : POSITION;
	float2		vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector		vColor : COLOR0;

};

//float3 CalcLUT(float3 InColor)
//{
//	//float2 Offset = float2(0.5f / 256.0f, 0.5f / 16.0f);
//	//float Scale = 15.0f / 16.0f;
//
//	//float IntB = floor(InColor.b * 14.9999f) / 16.f;
//	//float FracB = InColor.b * 15.0f - IntB * 16.0f;
//
//	//float U = IntB + InColor.r * Scale / 16.0f;
//	//float V = InColor.g * Scale;
//
//	//float3 RG0 = tex2D(LutSampler, Offset + float2(U, V)).rgb;
//	//float3 RG1 = tex2D(LutSampler, Offset + float2(U + 1.0f / 16.0f, V)).rgb;
//
//	//return lerp(RG0, RG1, FracB);
//
//	float COLOR = 16.0f;
//
//	float MaxColor = COLOR - 1.0f;
//	float3 col = saturate(InColor);
//	float halfColX = 0.5f / 256.0f;
//	float halfColY = 0.5f / MaxColor;
//	float threshold = MaxColor / COLOR;
//
//	float xOffset = halfColX + col.r * threshold / COLOR;
//	float yOffset = halfColY + col.g * threshold;
//	float cell = floor(col.b * MaxColor);
//
//	float2 lutPos = float2(cell / COLOR + xOffset, yOffset);
//	float4 gradedCol1 = tex2D(LutSampler, float2(lutPos.x - 0.00391, lutPos.y - 0.0626f));
//	float4 gradedCol2 = tex2D(LutSampler, float2(lutPos.x - 0.00391, lutPos.y + 0.0626f));
//	float4 gradedCol3 = tex2D(LutSampler, float2(lutPos.x + 0.00391, lutPos.y - 0.0626f));
//	float4 gradedCol4 = tex2D(LutSampler, float2(lutPos.x + 0.00391, lutPos.y + 0.0626f));
//
//	float4 gradedCol = ((gradedCol1 + gradedCol2) + (gradedCol3 + gradedCol4)) * 0.25f;
//
//	return lerp(col, gradedCol, 0.25f);
//}

float3 CalcLUT(float3 InColor)
{
	return tex3D(LutSampler, InColor * 15.0 / 16.0 + 0.50 / 16.0).rgb;
}

float3 ColorMood(float3 InColor)
{
	float fRatio = 0.25;
	float moodR = /*0.25*/ g_R;
	float moodG = /*0.4*/g_G;
	float moodB = /*0.5*/g_B;

	float3 cMood = 1;
	cMood.r = moodR;
	cMood.g = moodG;
	cMood.b = moodB;

	float fLum = (InColor.r + InColor.g + InColor.b) / 3;

	cMood = lerp(0, cMood, saturate(fLum * 2.0));
	cMood = lerp(cMood, 1, saturate(fLum - 0.5) * 2.0);
	float3 final = lerp(InColor, cMood, saturate(fLum * fRatio));
	return final;
}

float3 SharpColor(float3 InColor, float2 vUV)
{
	float3 Blur = InColor;
	float2 rcpres = { 0.0008f, 0.0008f };

	Blur += tex2D(DiffuseSampler, float2(vUV.x, vUV.y + rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x, vUV.y - rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x + rcpres.x, vUV.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x - rcpres.x, vUV.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x - rcpres.x, vUV.y + rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x - rcpres.x, vUV.y - rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x + rcpres.x, vUV.y + rcpres.y)) * 0.25;
	Blur += tex2D(DiffuseSampler, float2(vUV.x + rcpres.x, vUV.y - rcpres.y)) * 0.25;

	Blur /= 3;

	return lerp(Blur, InColor, 3);
}

float3 UnCharToneMapping(float3 vColor)
{
	float A = 0.15f;
	float B = 0.50f;
	float C = 0.10f;
	float D = 0.20f;
	float E = 0.02f;
	float F = 0.30f;

	//float A = 0.1f;
	//float B = 0.01f;
	//float C = 1.06f;
	//float D = 0.13f;
	//float E = 0.57f;
	//float F = 0.45f;

	return ((vColor *(A * vColor + C * B) + D * E) / (vColor * (A * vColor + B) + D * F)) - E / F;
}

float4 RGBtoCMYK(float3 vColor)
{
	float scale = 1;
	if (vColor.r == 0 && vColor.g == 0 && vColor.b == 0)
		return float4(0, 0, 0, 1);

	vColor.r = 1 - vColor.r;
	vColor.g = 1 - vColor.g;
	vColor.b = 1 - vColor.b;

	float mink = min(min(vColor.r, vColor.g), vColor.b);

	return float4(vColor, mink);
}

float3 CMYKtoRGB(float4 vCMYK)
{
	float3 vColor;
	vColor.r = ((1 - vCMYK.r) * (1 - vCMYK.w));
	vColor.g = ((1 - vCMYK.g) * (1 - vCMYK.w));
	vColor.b = ((1 - vCMYK.b) * (1 - vCMYK.w));

	return vColor;
}

float3 GrayScale(float3 InColor)
{
	float3 gray = dot(InColor.xyz, float3(0.222, 0.707, 0.071));
	float3 OutColor = lerp(gray, InColor, 0.5);
	return OutColor;
}
// Occlusion
const float3 avKernel[8] = {
	normalize(float3(1, 1, 1)) * 0.125f,
	normalize(float3(-1,-1,-1)) * 0.250f,
	normalize(float3(-1,-1, 1)) * 0.375f,
	normalize(float3(-1, 1,-1)) * 0.500f,
	normalize(float3(-1, 1 ,1)) * 0.625f,
	normalize(float3(1,-1,-1)) * 0.750f,
	normalize(float3(1,-1, 1)) * 0.875f,
	normalize(float3(1, 1,-1)) * 1.000f
};

//float CalSSAO(float2 uv, float colorx, float4 vSSAOParams)
//{
//	float fRadius = vSSAOParams.y;
//	float fPixelDepth = tex2D(DepthSampler, uv).r;
//	float fDepth = fPixelDepth * 30000.f;
//	float3 vKernelScale = float3(fRadius / fDepth, fRadius / fDepth, fRadius / 30000.f);
//	//float col = colorx;
//	//if (col < 0.1f)
//	//	col = 0.1f;
//
//	float fOcclusion = 0.0f;
//	for (int j = 1; j < 3; j++)
//	{
//		float3 random = tex2D(RandomSampler, uv * (7.0f + (float)j)).xyz;
//		random = random * 2.0f - 1.0f;
//
//		for (int i = 0; i < 8; i++)
//		{
//			float3 vRotatedKernel = reflect(avKernel[i], random) * vKernelScale;
//			float fSampleDepth = tex2D(DepthSampler, vRotatedKernel.xy + uv).r;
//			float fDelta = max(fSampleDepth - fPixelDepth + vRotatedKernel.z, 0);
//			float fRange = abs(fDelta) / (vKernelScale.z * vSSAOParams.z);
//			fOcclusion += lerp(fDelta * vSSAOParams.w, vSSAOParams.x, saturate(fRange));
//		}
//
//		//fOcclusion = lerp(0.1f, 0.6, saturate(col));
//	}
//	fOcclusion = fOcclusion / (2.0f * 8.0f);
//
//	return fOcclusion;
//}

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector			vRef = tex2D(RefSampler, In.vTexUV);
	vector			vDiffuse = /*pow(*/tex2D(DiffuseSampler, In.vTexUV)/*, 2.2f)*/;
	vector			vShade = tex2D(ShadeSampler, In.vTexUV);
	vector			vSpecular = tex2D(SpecularSampler, In.vTexUV);	

	vector vColor = vector(0,0,0,1);
	float vFog = vSpecular.y;
	//if (vFog < 0.0001f)
	//{
	//	Out.vColor = vector(0.4f, 0.4f, 0.4f, 1.f);
	//	return Out;
	//}

	if (g_W < 0.001f)
	{
		Out.vColor = vector(1,1,1,1);
		return Out;
	}

	////float2 vVelosity = vSpecular.zw;
	//float2 vVelosity = abs(In.vTexUV - float2(0.5f, 0.5f)) * 0.01f;

	////In.vTexUV += vVelosity * 0.0001f;
	//for (int i = 1; i <6; ++i, In.vTexUV += vVelosity)
	//{
	//	float4 currentColor = tex2D(ShadeSampler, In.vTexUV) * tex2D(DiffuseSampler, In.vTexUV) /*+ tex2D(RefSampler, In.vTexUV);*/;
	//	vColor += currentColor;
	//}

	//float4 finalColor = vColor / 6.f;
	float4 final = vDiffuse;
	float vDir = vSpecular.x;
	final.xyz = SharpColor(final.xyz, In.vTexUV);

	float4 col;
	//if (vDir > 0.01f)
	//{
	//if (vFog > 0.7f)
	//{
	//	for (int x = -6; x <= 6; ++x)
	//	{
	//		float2 T = In.vTexUV;
	//		T.x += (2.f * x) / 1024.f;
	//		col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 12.f);
	//		final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ vDir * 0.003f);
	//	}
	//	for (int x = -3; x <= 3; ++x)
	//	{
	//		float2 T = In.vTexUV;
	//		T.y += (2.f * x) / 1024.f;
	//		col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 6.f);
	//		final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ vDir * 0.003f);
	//	}

		if (final.x > 0.9f && final.y > 0.9f && final.z > 0.9f)
		{
			for (int x = -12; x <= 12; ++x)
			{
				float2 T = In.vTexUV;
				T.x += (2.f * x) / 512.f;
				col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 24.f);
				final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ 0.5f);
			}
			for (int x = -8; x <= 8; ++x)
			{
				float2 T = In.vTexUV;
				T.y += (2.f * x) / 512.f;
				col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 16.f);
				final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ 0.5f);
			}
		}
	//}
		//if (vRef.w > 0.02f)
		//{
		//	//for (int x = -6; x <= 6; ++x)
		//	//{
		//	//	float2 T = In.vTexUV;
		//	//	T.x += (25.f * x * vRef.w) / 1024.f;
		//	//	col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 12.f);
		//	//	final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ vDir * 0.02f);
		//	//}
		//	final = vector(1, 0, 0, 1);
		//}

	//}
	//else
	//	final = vDiffuse;

	vColor = final * vShade + vRef;

	vector vFogColor = tex2D(FogSampler, float2(g_foguv, 0.5f));
	//vFogColor.xyz = max(6.10352e-5, vFogColor.xyz);
	//vFogColor.xyz > 0.04045 ? pow(vFogColor.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vFogColor.xyz * (1.0 / 12.92);

	vFogColor.a = 1.f;
	vColor = vFog * vColor + (1.f - vFog) *  (/*vector(0.0323f, 0.0272f, 0.0252f, 1.f)*/ vFogColor * 0.1f);

	/////

	/////

	//float2 vVelosity = vSpecular.zw;

	//In.vTexUV += vVelosity;
	//for (int i = 1; i <6; ++i, In.vTexUV += vVelosity)
	//{
	//	float4 currentColor = tex2D(ShadeSampler, In.vTexUV) * tex2D(DiffuseSampler, In.vTexUV) /*+ tex2D(RefSampler, In.vTexUV);*/;
	//	vColor += currentColor;
	//}

	//vColor = vColor / 6.f;

	//col /= 12.f;

	//float factor = max(((0.5f - length(vDir)) / 0.5f), 0.f);
	//float factor = /*((vDir * 1.9f)*//* * (length(vDir) * 1.8f);*/ vDir;
	//Out.vColor = lerp(finalColor, col, /*(1.f - factor) * (1.f - factor)*/factor);

	//}
	//else
	
	//Out.vColor = finalColor;

	//if (g_vScreenLightPos.x > 0.f/* && g_vScreenLightPos.z < 0.f*/ && g_vScreenLightPos.x < 1280.f
	//	&& g_vScreenLightPos.y > 0.f && g_vScreenLightPos.y < 720.f)
	//{
	//vector ShaftColor = LightShaft(In.vTexUV, g_vScreenLightPos.xy);
	//if (ShaftColor.x > 0.2f)
	//{
	//	vector	vShaft = vColor * 1.1f + ShaftColor;

	//	vColor = vShaft;
	//}

	//}
	//else if (g_vScreenLightPos.x > 0.f && g_vScreenLightPos.z > 0.f)
	//{
	//	vColor += LightShaft(In.vTexUV, g_vScreenLightPos.xy);
	//	vColor += LightShaft(In.vTexUV, g_vScreenLightPos.zw);
	//	vColor *= 0.5f;
	//}

	//vColor.xyz = max(6.10352e-5, vColor.xyz);
	//vColor.xyz = min(vColor.xyz * 12.92, pow(max(vColor.xyz, 0.00313067), 1.0 / 2.4) * 1.055 - 0.055);
	//vColor.xyz = max(0, vColor.xyz - 0.004);
	//vColor.xyz = (vColor.xyz * (6.2 * vColor.xyz + 0.5)) / (vColor.xyz * (6.2 * vColor.xyz + 1.7) + 0.06);
	//float Luminance = dot(RGB, float3(0.2125f, 0.7154f, 0.0721f));

	//vColor.xyz = pow(vColor.xyz, 1 / 2.2);


	/////////////
	//vColor.xyz = max(0, vColor.xyz - 0.004);
	//vColor.xyz = (vColor.xyz * (6.2 * vColor.xyz + 0.5)) / (vColor.xyz * (6.2 * vColor.xyz + 1.7) + 0.06);
	/////////////

	//float Luminance = dot(vColor.xyz, float3(0.2125f, 0.7154f, 0.0721f));

	float Exposure = /*32.f;*/ g_E;
	float3 Cur = Exposure * UnCharToneMapping(vColor.xyz);
	float W = /*44.2f;*/g_W;
	float3 WhiteScale = 1.f / UnCharToneMapping(W);

	vColor.xyz = Cur * WhiteScale;

	//vColor.xyz = pow(vColor.xyz, 1 / 2.2);
	vColor.a = 1.f;

	//if (final.x < 0.9f || final.y < 0.9f || final.z < 0.9f)
	//	vColor.b *= 1.286;

	//vColor.xyz = lerp(vColor.xyz, CalcLUT(vColor.xyz), 0.75f);


	if (final.x < 0.9f || final.y < 0.9f || final.z < 0.9f)
	{
		//vColor.xyz = saturate(lerp(vColor.xyz, , 0.13f));

		//{0.222, 0.707, 0.071}
		//if (g_bssao)
		//	vColor.xyz *= /*min(*/CalSSAO(In.vTexUV, (vColor.x + vColor.y + vColor.z) * 0.33f, g_ssao) * 4.5f/*, 1.25f);*/;

		vColor.xyz = ColorMood(vColor.xyz);	
	}
	
	Out.vColor = /*pow(vColor, 1.f/2.2f);*/vColor;

	return Out;
}

PS_OUT PS_MAINSHAPE(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector			vRef = tex2D(RefSampler, In.vTexUV);
	vector			vDiffuse = /*pow(*/tex2D(DiffuseSampler, In.vTexUV)/*, 2.2f)*/;
	vector			vShade = tex2D(ShadeSampler, In.vTexUV);
	vector			vSpecular = tex2D(SpecularSampler, In.vTexUV);

	vector vColor = vector(0, 0, 0, 1);
	float vFog = vSpecular.y;

	float4 final = vDiffuse;

	float4 col;

	float2 vCenter = float2(0.5f, 0.5f);
	float2 vDir = vCenter - In.vTexUV;
	//float fLep = length(vDir);

	
	float fMask = tex2D(MaskingSampler, In.vTexUV).r;

	//final *= fMask;

	//for (int x = -12; x <= 12; ++x)
	//{
	//	float2 T = In.vTexUV;
	//	T.x += (2.f * x) / 1024.f;
	//	col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 12.f);
	//	final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ fMask);
	//}
	//for (int x = -8; x <= 8; ++x)
	//{
	//	float2 T = In.vTexUV;
	//	T.y += (2.f * x) / 1024.f;
	//	col += tex2D(DiffuseSampler, T) * exp((-x*x) / 8.f);
	//	final = lerp(final, col, fMask);
	//}


	float decay = 0.99815;
	float exposure = 1.f;
	float density = 0.966;
	float weight = 0.68767;
	int NUM_SAMPLES = 45;
	float2 tc = In.vTexUV;
	float2 deltaTexCoord = (tc - vCenter);
	deltaTexCoord *= 1.0f / NUM_SAMPLES * density;
	float illuminationDecay = 1.0;
	vector color = tex2D(DiffuseSampler, tc)*0.4f;
	color.w = 1.f;

	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		tc -= deltaTexCoord;
		vector samp = tex2D(DiffuseSampler, tc)*0.4f;
		samp.w = 1.f;
		samp *= illuminationDecay * weight;
		color += samp;
		illuminationDecay *= decay;
	}

	color *= 0.5f;
	color.a = 1.f;
	final = /*color * fMask;*/	lerp(final, color, fMask * 0.2f);


	vColor = final * vShade + vRef;

	vColor = vFog * vColor + (1.f - vFog) *  (vector(0.0323f, 0.0272f, 0.0252f, 1.f));

	vColor.xyz = pow(vColor.xyz, 1 / 2.2);
	vColor.xyz = max(0, vColor.xyz - 0.004);
	vColor.xyz = (vColor.xyz * (6.2 * vColor.xyz + 0.5)) / (vColor.xyz * (6.2 * vColor.xyz + 1.7) + 0.06);

	vColor.a = 1.f;
	Out.vColor = vColor;

	return Out;
}

technique	DefaultDevice
{
	pass Render_Blend
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0;

		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass Render_BlendShape
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0;

		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAINSHAPE();
	}
}


