#include "stdafx.h"
#include "..\Headers\Effect_FootPrint.h"
#include "Management.h"
#include "MeshTexture.h"

#include "Target_Manager.h"

#include "Math_Manager.h"
_USING(Client)

CEffect_FootPrint::CEffect_FootPrint(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_FootPrint::CEffect_FootPrint(const CEffect_FootPrint & _rhs)
	: CBaseEffect(_rhs)

{

}

HRESULT CEffect_FootPrint::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CEffect_FootPrint::Ready_GameObject()
{
	CBaseEffect::Ready_Component(L"Component_Shader_FootPrint");

	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Buffer
	m_pDecalBox = (CBuffer_CubeTex*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_CubeTex");
	if (nullptr == m_pDecalBox)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Buffer", m_pDecalBox)))
		return E_FAIL;

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;


	//m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Com_Mesh_FootPrint");
	//if (Add_Component(L"Com_Mesh", m_pMeshCom))
	//	return E_FAIL;


	Safe_Release(pManagement);

	


	return NOERROR;
}

_int CEffect_FootPrint::Update_GameObject(const _float & _fTick)
{


	if (m_isDead)
		return DEAD_OBJ;



	m_fTime += _fTick;
	if (10.f < m_fTime)
		m_isDead = true;

	return _int(); 
}

_int CEffect_FootPrint::LastUpdate_GameObject(const _float & _fTick)
{
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;


	

	return _int();
}

void CEffect_FootPrint::Render_GameObject()
{
	//if (nullptr == m_pMeshCom ||
	//	nullptr == m_pShaderCom)
	//	return;

	//LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	//if (nullptr == pEffect)
	//	return;

	//pEffect->AddRef();

	//_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	//pEffect->Begin(nullptr, 0);

	//pEffect->BeginPass(0);

	//for (size_t i = 0; i < dwNumMaterials; i++)
	//{
	//	if (FAILED(SetUp_ContantTable(pEffect, i)))
	//		return;

	//	pEffect->CommitChanges();

	//	m_pMeshCom->Render_Mesh(i);
	//}

	//pEffect->EndPass();
	//pEffect->End();

	//Safe_Release(pEffect);

	if (nullptr == m_pDecalBox)
		return;

	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	SetUp_ContantTable(pEffect);


	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(0);

	m_pDecalBox->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CEffect_FootPrint::Set_Param(const _vec3 & _vPos, const void* _pVoid)
{
	/*_vec3 vPos = _vPos;
	vPos.x += Math_Manager::CalRandIntFromTo(-200,200);
	vPos.y += Math_Manager::CalRandIntFromTo(0,100);
	vPos.z += Math_Manager::CalRandIntFromTo(-200,200);*/

	
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vPos);

	//_float fSize = Math_Manager::CalRandIntFromTo(100, 300);
	//m_pTransformCom->Scaling(fSize, fSize, fSize);
	m_pTransformCom->Scaling(600.f, 600.f, 600.f);


	m_fUV.x = Math_Manager::CalRandFloatFromTo(0.f,1.f);
	m_fUV.y = Math_Manager::CalRandFloatFromTo(0.f, 1.f);
}


CEffect_FootPrint * CEffect_FootPrint::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_FootPrint*	pInstance = new CEffect_FootPrint(_pGDevice);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CEffect_FootPrint::Clone_GameObject()
{
	CEffect_FootPrint*	pInstance = new CEffect_FootPrint(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_FootPrint Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT CEffect_FootPrint::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{

	
	_matrix			matWorld = m_pTransformCom->Get_Matrix();
	_pEffect->SetMatrix("g_matWorld", &matWorld);

	_matrix			matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	_pEffect->SetMatrix("g_matView", &matView);

	_matrix			matProj;
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);
	_pEffect->SetMatrix("g_matProj", &matProj);

	CTarget_Manager::GetInstance()->SetUp_OnShader(_pEffect, L"Target_Depth", "g_DepthTexture");
	//const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	//if (nullptr == pSubSet)
	//	return E_FAIL;

	//_pEffect->SetTexture("g_DecalTextures", pSubSet->MeshTexture.pDiffuseTexture);
	_pEffect->SetTexture("g_DecalTextures", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"FootPrint.tga")));
	_pEffect->SetTexture("g_DecalAlphaTextures", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"FootPrintAlpha.tga")));
	_pEffect->SetTexture("g_Gradient", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"FootGradient.png")));
	//_pEffect->SetTexture("g_Color", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));

	_matrix			matWVInv = matWorld*matView;
	D3DXMatrixInverse(&matWVInv, nullptr, &matWVInv);
	_pEffect->SetMatrix("g_matWVInv", &matWVInv);


	_float			fAlpha = 1.f;
	if (m_fTime < 2.f)
		fAlpha = 0.5f*m_fTime;
	else if(8.f<m_fTime)
		fAlpha = (10.f- m_fTime)*0.5f;
	
	_pEffect->SetFloat("g_fAlpha", fAlpha);

	_pEffect->SetFloat("g_fUVX", m_fUV.x);
	_pEffect->SetFloat("g_fUVY", m_fUV.y);

	_pEffect->SetFloat("g_fGradient", sinf(m_fTime*3.f)*0.5f+0.5f);// *0.3f + 0.6f);
	//_float			fUV = m_pTransformCom->Get_Scale().x/ 300.f;
	//_pEffect->SetFloat("g_fUV", fUV);
	return NOERROR;
}

void CEffect_FootPrint::Free()
{
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pDecalBox);
	Safe_Release(m_pRendererCom);

	CGameObject::Free();
}