#include "stdafx.h"
#include "..\Headers\ExitDoorLight.h"
#include "Management.h"
#include "Defines.h"
#include "CustomLight.h"
#include "Light_Manager.h"
#include "MeshTexture.h"

_USING(Client)

CExitDoorLight::CExitDoorLight(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CExitDoorLight::CExitDoorLight(const CExitDoorLight & rhs)
	: CGameObject(rhs)
{
}

HRESULT CExitDoorLight::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CExitDoorLight::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_LateInit = false;
	return NOERROR;
}

_int CExitDoorLight::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	Init_Light();
	//if (KEYMGR->KeyPressing(DIK_J))
	//	m_pTransformCom->Move_V3(_vec3(1.f, 0.f, 0.f));
	//if(KEYMGR->KeyPressing(DIK_L))
	//	m_pTransformCom->Move_V3(_vec3(-1.f, 0.f, 0.f));
	//if (KEYMGR->KeyPressing(DIK_U))
	//	m_pTransformCom->Move_V3(_vec3(0.f, -1.f, 0.f));
	//if (KEYMGR->KeyPressing(DIK_O))
	//	m_pTransformCom->Move_V3(_vec3(0.f, 1.f, 0.f));
	//if (KEYMGR->KeyPressing(DIK_I))
	//	m_pTransformCom->Move_V3(_vec3(0.f, 0.f, 1.f));
	//if (KEYMGR->KeyPressing(DIK_K))
	//	m_pTransformCom->Move_V3(_vec3(0.f, 0.f, -1.f));
	//_vec3 Pos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	//cout << Pos.x << " ";
	//cout << Pos.y << " ";
	//cout << Pos.z << " " << endl;
	Check_Light();

	return _int();
}

_int CExitDoorLight::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > m_Max* 30.f)
		return 0;

	if (!m_pFrustumCom->Culling_Frustum(&m_vLocalPos, m_matLocalInv, m_Max))
		return 0;


	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CExitDoorLight::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
	/*for (int i = 0; i < 3; ++i)
		m_pLightCollider[i]->Render_Collider();*/

	//m_pColliderCom->Render_Collider();
}

void CExitDoorLight::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Mesh_ExitDoorLight");
	Add_Component(L"Com_Mesh", m_pMeshCom);

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Box", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_AABB, m_pMeshCom->Get_LocalTransform(), nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);

	_vec3 vMax = m_pMeshCom->GetMax();
	_vec3 vMin = m_pMeshCom->GetMin();
	_vec3 vResult = (vMax - vMin) * 0.52f;

	m_Max = max(vResult.x, max(vResult.y, vResult.z));

	m_matLocalInv = m_pMeshCom->Get_LocalTransform();

	m_matLocalInv = m_matLocalInv * matWorld;
	m_vLocalPos = *(_vec3*)&m_matLocalInv.m[3][0];
	D3DXMatrixInverse(&m_matLocalInv, nullptr, &m_matLocalInv);

	//
}

_matrix CExitDoorLight::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

void CExitDoorLight::Init_Light()
{
	if (m_LateInit)
		return;

	_matrix matLocal;
	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	_vec3 vLocalPos = { 0.f, 0.f, 12.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[0] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider0", m_pLightCollider[0]);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { 0.f, 0.f, 0.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[1] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider1", m_pLightCollider[1]);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { 0.f, 0.f, -12.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[2] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider2", m_pLightCollider[2]);

	for (int i = 0; i < 3; ++i)
	{
		_vec3 vCenter = m_pLightCollider[i]->Get_Center();

		if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight[i])))
			return;

		m_pCustomLight[i]->Add_Light();
		m_pCustomLight[i]->AddRef();

		m_pCustomLight[i]->Set_Position(vCenter);

		m_pCustomLight[i]->Set_Range(30.f);
		m_pCustomLight[i]->Set_Diffuse(D3DXCOLOR(3.f, 0.1f, 0.1f, 1.f));
		m_pCustomLight[i]->SetRender(false);
		//AddShadowMap
		//m_pCustomLight[i]->SetRender(false);

		CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
		if (nullptr == pLight_Manager)
			return;

		pLight_Manager->AddRef();

		pLight_Manager->Ready_ShadowMap(m_pCustomLight[i]->GetLight());

		Safe_Release(pLight_Manager);
	}
		m_LateInit = true;
			
}

void CExitDoorLight::Check_Light()
{
	if (m_iLightRender == 0)
		return;

	for (_uint i = 0; i < m_iLightRender; ++i)
		m_pCustomLight[i]->SetRender(true);
}

HRESULT CExitDoorLight::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CExitDoorLight::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	D3DXMatrixInverse(&matView, nullptr, &matView);
	pEffect->SetVector("g_vCamPosition", (_vec4*)&matView.m[3][0]);

	if(m_iLightRender == 0)
		pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	else if(m_iLightRender == 1)
		pEffect->SetTexture("g_DiffuseTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_ActivationlightsOn01.tga")));
	else if (m_iLightRender == 2)
		pEffect->SetTexture("g_DiffuseTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_ActivationlightsOn02.tga")));
	else
		pEffect->SetTexture("g_DiffuseTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_ActivationlightsOn03.tga")));

	return NOERROR;
}

CExitDoorLight * CExitDoorLight::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CExitDoorLight*	pInstance = new CExitDoorLight(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CExitDoorLight Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CExitDoorLight::Clone_GameObject()
{
	CExitDoorLight*	pInstance = new CExitDoorLight(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CExitDoorLight Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CExitDoorLight::Free()
{
	for (int i = 0; i < 3; ++i)
	{
		Safe_Release(m_pLightCollider[i]);
		Safe_Release(m_pCustomLight[i]);
	}

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
