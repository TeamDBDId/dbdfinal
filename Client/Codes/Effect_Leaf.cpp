#include "stdafx.h"
#include "Effect_Leaf.h"
#include "Management.h"



#include "Math_Manager.h"
_USING(Client)

CEffect_Leaf::CEffect_Leaf(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_Leaf::CEffect_Leaf(const CEffect_Leaf & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Leaf::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Leaf::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;


	m_vDir = _vec3(Math_Manager::CalRandFloatFromTo(-150.f, 150.f), Math_Manager::CalRandFloatFromTo(-350.f, -250.f), Math_Manager::CalRandFloatFromTo(-150.f, 150.f));
	m_vRot = _vec3(Math_Manager::CalRandFloatFromTo(-2.5f, 2.5f), Math_Manager::CalRandFloatFromTo(-2.5f, 2.5f), Math_Manager::CalRandFloatFromTo(-2.5f, 2.5f));
	
	
	m_iType = Math_Manager::CalRandIntFromTo(0, 3);


	m_pTransformCom->SetUp_Speed(0.f,D3DXToRadian(90.f));
	m_pTransformCom->Scaling(2.f, 2.f, 2.f);


	return NOERROR;
}

_int CEffect_Leaf::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;


	m_fTime += _fTick;
	if (4.5f < m_fTime)
		m_isDead = true;

	return _int();
}

_int CEffect_Leaf::LastUpdate_GameObject(const _float & _fTick)
{
	if (nullptr == m_pRendererCom)
		return -1;


	//_matrix		matView;
	//m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	//D3DXMatrixInverse(&matView, nullptr, &matView);

	//_vec3		vRight, vUp, vLook;


	//vRight = *(_vec3*)&matView.m[0][0] * m_pTransformCom->Get_Scale().x;
	//vUp = *(_vec3*)&matView.m[1][0] * m_pTransformCom->Get_Scale().y;
	//vLook = *(_vec3*)&matView.m[2][0];



	//m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	//m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	//m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);




	m_pTransformCom->Rotation_X(_fTick*m_vRot.x);
	m_pTransformCom->Rotation_Y(_fTick*m_vRot.y);
	m_pTransformCom->Rotation_Z(_fTick*m_vRot.z);

	m_pTransformCom->Move_V3(m_vDir*_fTick);


	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;



	return _int();
}

void CEffect_Leaf::Render_GameObject()
{

	//if (nullptr == m_pBufferCom)
	//	return;

	//// 셰이더를 이용해서 그려. 
	//LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	//if (nullptr == pEffect)
	//	return;

	//pEffect->AddRef();


	//if (FAILED(SetUp_ContantTable(pEffect)))
	//	return;

	//pEffect->Begin(nullptr, 0);
	//pEffect->BeginPass(0);

	//m_pBufferCom->Render_VIBuffer();

	//pEffect->EndPass();
	//pEffect->End();

	//Safe_Release(pEffect);

	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(_E_LEAF);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);



}

void CEffect_Leaf::Set_Param(const _vec3 & _vPos, const void* _pVoid)
{
	_vec3 vPos = _vPos;
	vPos.x += Math_Manager::CalRandFloatFromTo(-500.f,500.f);
	vPos.y += 1000.f;
	vPos.z += Math_Manager::CalRandFloatFromTo(-500.f, 500.f);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}




HRESULT CEffect_Leaf::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Effect");


	GET_INSTANCE_MANAGEMENTR(E_FAIL);


	//m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	//if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
	//	return E_FAIL;


	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Leaf");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Mesh_Leaf");
	if (Add_Component(L"Com_Mesh", m_pMeshCom))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_Leaf::SetUp_ConstantTable(LPD3DXEFFECT _pEffect, const _uint& iAttributeID)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);


	m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	_pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0");

	_pEffect->SetInt("g_iColumn", m_iType/2);
	_pEffect->SetInt("g_iRow", m_iType%2);


	return NOERROR;


	//m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");
	////_pEffect->SetMatrix("g_matWorld", &m_matWorld);

	//m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0");


	//_pEffect->SetInt("g_iColumn", m_iType/2);
	//_pEffect->SetInt("g_iRow", m_iType%2);


	//if(3.5f<m_fTime)
	//	_pEffect->SetFloat("g_fAlpha", (4.f-m_fTime)*2.f);
	//else
	//	_pEffect->SetFloat("g_fAlpha", 1.f);
	//	

	//return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Leaf * CEffect_Leaf::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Leaf*	pInst = new CEffect_Leaf(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Leaf::Clone_GameObject()
{
	CEffect_Leaf*	pInst = new CEffect_Leaf(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_Leaf Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Leaf::Free()
{
	//Safe_Release(m_pBufferCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pTextureCom);


	//지울것들



	CBaseEffect::Free();
}
