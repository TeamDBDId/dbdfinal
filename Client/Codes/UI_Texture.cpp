#include "stdafx.h"
#include "..\Headers\UI_Texture.h"
#include "MeshTexture.h"
#include "Management.h"
#include "FontManager.h"
#include "Math_Manager.h"
_USING(Client);

CUI_Texture::CUI_Texture(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_Texture::CUI_Texture(const CUI_Texture & rhs)
	:CGameObject(rhs)
	,m_vUV(rhs.m_vUV)
	,m_vAngle(rhs.m_vAngle)
	,m_fRotation(rhs.m_fRotation)
	,m_eFadeState(rhs.m_eFadeState)
	,m_fAlpha(rhs.m_fAlpha)
	, m_isRender(rhs.m_isRender)
	, m_IsBillboard(rhs.m_IsBillboard)
{
}

HRESULT CUI_Texture::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_Texture::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_isRender = true;
	m_pTransformCom->SetUp_RotationZ(0.f);
	return NOERROR;
}


_int CUI_Texture::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead ||m_iOneTimeRendring == 2)
		return 1;
	if (m_fDeadTime != 0.f)
	{
		m_fDeadTime -= fTimeDelta;
		if (m_fDeadTime <= 0.f)
			return 1;
	}
	if (m_eFadeState != NONE)
	{
		m_fFadeTime -= fTimeDelta;
		if (m_fFadeTime <= 0.f)
			m_fFadeTime = 0.f;
	}

	Test(fTimeDelta);
	return _int();
}

_int CUI_Texture::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (m_isDead || m_iOneTimeRendring == 2)
		return 1;

	Update_Billboard();
	if(m_isRender)
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
			return -1;
	return _int();
}

void CUI_Texture::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	_int Shade = 0;
	if (m_IsButtonFrame)
		Shade = 1;

	LPD3DXEFFECT pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;
	pEffect->AddRef();
	pEffect->Begin(nullptr, 0);

	if (m_iOneTimeRendring == 1)
		m_iOneTimeRendring = 2;

	if (m_vUV.x != 1.f || m_vUV.y != 1.f)
	{
		if (FAILED(SetUp_ConstantTable_UV(pEffect)))
			return;
		pEffect->BeginPass(8);
	}
	else if (m_vAngle.x != 0.f && m_vAngle.y != 0.f)
	{
		if (FAILED(SetUp_ConstantTable_Angle(pEffect)))
			return;
		pEffect->BeginPass(9);
	}
	else if (m_IsShadeColor)
	{
		if (FAILED(SetUp_ConstantTable_Color(pEffect)))
			return;
		pEffect->BeginPass(7);
	}
	else if (m_IsBillboard)
	{
		if (FAILED(SetUp_ConstantTable_Billboard(pEffect)))
			return;
		pEffect->BeginPass(10);
	}
	else if (m_IsCoolTime)
	{
		if (FAILED(SetUp_ConstantTable_CoolTime(pEffect)))
			return;
		pEffect->BeginPass(15);
	}
	else if (m_IsColorCoolTime)
	{
		if (FAILED(SetUp_ConstantTable_ColorCoolTime(pEffect)))
			return;
		pEffect->BeginPass(16);
	}
	else
	{
		if (FAILED(SetUp_ConstantTable(pEffect)))
			return;
		pEffect->BeginPass(10+Shade);
	}
	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CUI_Texture::Set_TexName(wstring TexName, _int Num)
{
	if (Num == 0)
	{
		m_TexName = TexName;
		m_Texture = GET_INSTANCE(CMeshTexture)->Find_Texture(TexName);
	}
	else if (Num == 1)
	{
		m_TexName2 = TexName;
		m_Texture2 = GET_INSTANCE(CMeshTexture)->Find_Texture(TexName);
	}
	else if (Num == 2)
	{
		m_TexName = TexName;
		m_Texture = GET_INSTANCE(CMeshTexture)->Find_Texture(TexName);
		Set_Scale(_vec2(0.666667f, 0.666667f));
	}
}

void CUI_Texture::Set_Pos(const _vec2& Pos)
{
	 m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(Pos.x - (g_iBackCX >> 1), - Pos.y + (g_iBackCY >> 1), 0.f));
}

void CUI_Texture::Set_Pos(const _vec3& Pos)
{
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Pos);
}

void CUI_Texture::Set_PosPlus(_vec2 Pos)
{
	_vec3 BeforePos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	BeforePos += _vec3(Pos.x, Pos.y, 0.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &BeforePos);
}

_vec2 CUI_Texture::Get_Pos()
{
	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	return _vec2(vPos.x + (g_iBackCX >> 1), -vPos.y +(g_iBackCY >> 1));
}

void CUI_Texture::Set_Scale(_vec2 Scale, _bool IsOriginScale)
{
	if (IsOriginScale)
	{
		if (m_TexName == L"")
			return;
		_vec2 vTexScale = *GET_INSTANCE(CMeshTexture)->Find_TextureSize(m_TexName);
		m_pTransformCom->Scaling(vTexScale.x*Scale.x, vTexScale.y*Scale.y, 1.f);
	}
	else
		m_pTransformCom->Scaling(Scale.x,Scale.y,1.f);
}

_vec2 CUI_Texture::Get_Scale()
{
	_vec3 vScale = m_pTransformCom->Get_Scale();
	return _vec2(vScale.x, vScale.y);
}

void CUI_Texture::Set_Font(wstring Font, _vec2 Size, D3DXCOLOR Color,_uint FontNum)
{
	m_Texture = GET_INSTANCE(CFontManager)->FindFont(Font, Size, Color,FontNum);
	if (m_Texture == nullptr)
		return;
	m_TexName = Font;
	Set_FontScale(Size);
}

void CUI_Texture::Set_FontScale(_vec2 Size)
{
	Set_Scale(_vec2(m_TexName.length()* Size.x, Size.y), false);
}

void CUI_Texture::Set_UV(_vec2 vUV)
{
	if (vUV.x > 1.f || vUV.y > 1.f)
		return;

	m_vUV = vUV;
}

void CUI_Texture::Set_Rotation(_float Radian)
{
	if (m_fRotation + Radian >= 2.f*D3DX_PI)
		m_fRotation = 2.f*D3DX_PI;
	else
		m_fRotation += Radian;
	

	m_pTransformCom->SetUp_RotationZ(-m_fRotation);

}

_bool CUI_Texture::IsTouch()
{
	POINT Pos;
	_vec2 vPos;
	GetCursorPos(&Pos);
	//ScreenToClient(g_hWnd, &Pos);
	vPos = { (_float)Pos.x * (_float)g_iBackCX / (_float)g_iFullCX, (_float)Pos.y * (_float)g_iBackCY / (_float)g_iFullCY };
	_vec3 m_vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_vec2 m_vScale = Get_Scale();
	m_vScale *= 0.5f;
	m_vPosition.x += (g_iBackCX >> 1);
	m_vPosition.y = (g_iBackCY >> 1) - m_vPosition.y;
	return Math_Manager::IsRectIn(vPos,_vec4(m_vPosition.x - m_vScale.x, m_vPosition.x + m_vScale.x, m_vPosition.y - m_vScale.y, m_vPosition.y + m_vScale.y));
}

void CUI_Texture::Update_Billboard()
{
	if (!m_IsBillboard)
		return;

	_matrix		matView;			//���Ʒ� ������
	_vec3		vRight, vUp, vLook, vScale;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	vScale = m_pTransformCom->Get_Scale();
	D3DXMatrixInverse(&matView, nullptr, &matView);

	vRight = *(_vec3*)&matView.m[0][0];
	vUp = *(_vec3*)&matView.m[1][0];
	vLook = *(_vec3*)&matView.m[2][0];

	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
	m_pTransformCom->Scaling(vScale.x, vScale.y, vScale.z);
}

void CUI_Texture::Test(const _float& fDeltaTime)
{
	if (!m_isTest)
		return;
	_vec3 m_vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_vec3 m_vScale = m_pTransformCom->Get_Scale();
	if (KEYMGR->KeyPressing(DIK_L))
		m_vPosition.x += 10.f*fDeltaTime;
	if (KEYMGR->KeyPressing(DIK_J))
		m_vPosition.x -= 10.f*fDeltaTime;
	if (KEYMGR->KeyPressing(DIK_I))
		m_vPosition.y += 10.f*fDeltaTime;
	if (KEYMGR->KeyPressing(DIK_K))
		m_vPosition.y -= 10.f*fDeltaTime;
	
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vPosition);
	//cout << (m_vPosition.x + (g_iBackCX >> 1))/g_iBackCX <<" "<< ((g_iBackCY >> 1) - m_vPosition.y) / g_iBackCY << endl;
}

HRESULT CUI_Texture::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");

	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");

	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Shader_Default");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}



HRESULT CUI_Texture::SetUp_ConstantTable(LPD3DXEFFECT pEffect)
{
	_matrix		matWorld, matView, matProj;
	
	matWorld = m_pTransformCom->Get_Matrix();
	pEffect->SetMatrix("g_matWorld", &matWorld);
	
	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetTexture("g_DiffuseTexture",m_Texture);

	if(m_IsBlend)
		pEffect->SetTexture("g_BlendTexture", m_Texture2);

	if (m_eFadeState == FADEIN)
		pEffect->SetFloat("g_fTime", ((m_fMaxFadeTime - m_fFadeTime) / m_fMaxFadeTime)*m_fAlpha);
	else if (m_eFadeState == FADEOUT)
		pEffect->SetFloat("g_fTime", (m_fFadeTime / m_fMaxFadeTime)*m_fAlpha);
	else
		pEffect->SetFloat("g_fTime",m_fAlpha);

	return NOERROR;
}

HRESULT CUI_Texture::SetUp_ConstantTable_Color(LPD3DXEFFECT pEffect)
{
	_matrix		matWorld, matView, matProj;

	matWorld = m_pTransformCom->Get_Matrix();
	matWorld._41 -= (1.f - m_vUV.x) * matWorld._11 * 0.5f;
	matWorld._42 += (1.f - m_vUV.y) * matWorld._22 * 0.5f;
	matWorld._11 *= m_vUV.x;
	matWorld._22 *= m_vUV.y;

	pEffect->SetMatrix("g_matWorld", &matWorld);

	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetTexture("g_DiffuseTexture", m_Texture);
	pEffect->SetVector("g_vColor", (_vec4*)&m_Color);
	
	if (m_eFadeState == FADEIN)
		pEffect->SetFloat("g_fTime", ((m_fMaxFadeTime - m_fFadeTime) / m_fMaxFadeTime)*m_fAlpha);
	else if (m_eFadeState == FADEOUT)
		pEffect->SetFloat("g_fTime", (m_fFadeTime / m_fMaxFadeTime)*m_fAlpha);
	else
		pEffect->SetFloat("g_fTime", m_fAlpha);
	return NOERROR;
}

HRESULT CUI_Texture::SetUp_ConstantTable_UV(LPD3DXEFFECT pEffect)
{
	_matrix		matWorld, matView, matProj;

	matWorld = m_pTransformCom->Get_Matrix();
	matWorld._41 -= (1.f-m_vUV.x) * matWorld._11 * 0.5f;
	matWorld._42 += (1.f-m_vUV.y) * matWorld._22 * 0.5f;
	matWorld._11 *= m_vUV.x;
	matWorld._22 *= m_vUV.y;

	pEffect->SetMatrix("g_matWorld", &matWorld);

	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetTexture("g_DiffuseTexture", m_Texture);
	pEffect->SetFloatArray("g_fUV", m_vUV, 2);

	if (m_eFadeState == FADEIN)
		pEffect->SetFloat("g_fTime", ((m_fMaxFadeTime - m_fFadeTime) / m_fMaxFadeTime)*m_fAlpha);
	else if (m_eFadeState == FADEOUT)
		pEffect->SetFloat("g_fTime", (m_fFadeTime / m_fMaxFadeTime)*m_fAlpha);
	else
		pEffect->SetFloat("g_fTime", m_fAlpha);

	return NOERROR;
}

HRESULT CUI_Texture::SetUp_ConstantTable_Angle(LPD3DXEFFECT pEffect)
{
	_matrix		matWorld, matView, matProj;

	matWorld = m_pTransformCom->Get_Matrix();
	pEffect->SetMatrix("g_matWorld", &matWorld);

	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetTexture("g_DiffuseTexture", m_Texture);
	pEffect->SetFloatArray("g_fAngle", m_vAngle, 2);
	pEffect->SetFloat("g_fTime", m_fAlpha);
	return NOERROR;
}

HRESULT CUI_Texture::SetUp_ConstantTable_Billboard(LPD3DXEFFECT pEffect)
{
	_matrix	matWorld, matView, matProj;

	matWorld = m_pTransformCom->Get_Matrix();
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matWorld", &matWorld);
	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);
	pEffect->SetTexture("g_DiffuseTexture", m_Texture);

	if (m_eFadeState == FADEIN)
		pEffect->SetFloat("g_fTime", ((m_fMaxFadeTime - m_fFadeTime) / m_fMaxFadeTime)*m_fAlpha);
	else if (m_eFadeState == FADEOUT)
		pEffect->SetFloat("g_fTime", (m_fFadeTime / m_fMaxFadeTime)*m_fAlpha);
	else
		pEffect->SetFloat("g_fTime", m_fAlpha);

	return NOERROR;
}

HRESULT CUI_Texture::SetUp_ConstantTable_CoolTime(LPD3DXEFFECT pEffect)
{
	_matrix		matWorld, matView, matProj;

	matWorld = m_pTransformCom->Get_Matrix();
	pEffect->SetMatrix("g_matWorld", &matWorld);

	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetTexture("g_DiffuseTexture", m_Texture);
	pEffect->SetFloatArray("g_fAngle", m_vAngle, 2);
	pEffect->SetFloat("g_fTime", /*m_fAlpha*/1.f);
	return NOERROR;
}

HRESULT CUI_Texture::SetUp_ConstantTable_ColorCoolTime(LPD3DXEFFECT pEffect)
{
	_matrix		matWorld, matView, matProj;

	matWorld = m_pTransformCom->Get_Matrix();
	matWorld._41 -= (1.f - m_vUV.x) * matWorld._11 * 0.5f;
	matWorld._42 += (1.f - m_vUV.y) * matWorld._22 * 0.5f;
	matWorld._11 *= m_vUV.x;
	matWorld._22 *= m_vUV.y;

	pEffect->SetMatrix("g_matWorld", &matWorld);

	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetTexture("g_DiffuseTexture", m_Texture);
	pEffect->SetVector("g_vColor", (_vec4*)&m_Color);
	pEffect->SetFloatArray("g_fAngle", m_vAngle, 2);
	pEffect->SetFloat("g_fTime", m_fAlpha);
	return NOERROR;
}


CUI_Texture * CUI_Texture::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_Texture*	pInstance = new CUI_Texture(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_Texture Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_Texture::Clone_GameObject()
{
	CUI_Texture*	pInstance = new CUI_Texture(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_Texture Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI_Texture::Free()
{
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}