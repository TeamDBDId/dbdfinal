#include "stdafx.h"
#include "Effect_PlayerChange.h"
#include "Management.h"

#include "Effect_SmokeBall.h"
//#include "Effect_Flash.h"


#include "Math_Manager.h"
_USING(Client)

CEffect_PlayerChange::CEffect_PlayerChange(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CEffect_PlayerChange::CEffect_PlayerChange(const CEffect_PlayerChange & _rhs)
	: CGameObject(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_PlayerChange::Ready_Prototype()
{


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_PlayerChange::Ready_GameObject()
{




	return NOERROR;
}

_int CEffect_PlayerChange::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;

	m_fTick += _fTick;
	if (2.f < m_fTick)
		m_isDead = true;






	return _int();
}

_int CEffect_PlayerChange::LastUpdate_GameObject(const _float & _fTick)
{



	return _int();
}

void CEffect_PlayerChange::Render_GameObject()
{


}

void CEffect_PlayerChange::Set_Param(const _vec3 & _vPos)
{
	m_vPos = _vPos;
	//m_vPos.y += 40.f;
	//m_vPos.y += 100.f;


	Make_Smoke1();
	Make_Smoke2();
}





void CEffect_PlayerChange::Make_Smoke1()
{

	GET_INSTANCE_MANAGEMENT;

	_vec3 vSmokeDir = _vec3(200.f, 0.f, 0.f);
	

	_matrix matRot;
	D3DXMatrixRotationY(&matRot, D3DXToRadian(20.f));

	for (size_t i = 0; i < SMOKE_CNT; ++i)
	{
		CEffect_SmokeBall* pEffect = nullptr;
		if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_SmokeBall", SCENE_STAGE, L"Layer_ESmokeBall", (CGameObject**)&pEffect)))
			_MSG_BOX("Fail Make_SmokeBall");
		pEffect->Set_Param(m_vPos+ vSmokeDir,&vSmokeDir);
		//pEffect->Set_Type(0);

		D3DXVec3TransformNormal(&vSmokeDir, &vSmokeDir, &matRot);
	}

	Safe_Release(pManagement);
}

void CEffect_PlayerChange::Make_Smoke2()
{
	//GET_INSTANCE_MANAGEMENT;

	//_vec3 vSmokeDir = _vec3(20.f, 0.f, 0.f);
	//_vec3 vPos = m_vPos;


	//_matrix matRot;
	//D3DXMatrixRotationY(&matRot, D3DXToRadian(60.f));

	//for (size_t i = 0; i < 8; ++i)
	//{

	//	for (size_t i = 0; i < SMOKE_CNT/2; ++i)
	//	{
	//		CEffect_SmokeBall* pEffect = nullptr;
	//		if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_SmokeBall", SCENE_STAGE, L"Layer_ESmokeBall", (CGameObject**)&pEffect)))
	//			_MSG_BOX("Fail Make_SmokeBall");
	//		pEffect->Set_Param(vPos + vSmokeDir, &vSmokeDir);

	//		D3DXVec3TransformNormal(&vSmokeDir, &vSmokeDir, &matRot);
	//	}



	//	vPos.y += 20.f;
	//}
	//
	//Safe_Release(pManagement);




	GET_INSTANCE_MANAGEMENT;

	_vec3 vSmokeDir = _vec3(0.f, -90.f, 0.f);
	_vec3 vRand = _vec3();


	for (size_t i = 0; i < SMOKE_CNT*4; ++i)
	{

		vRand = _vec3(Math_Manager::CalRandIntFromTo(-45,45), Math_Manager::CalRandIntFromTo(-80,120), Math_Manager::CalRandIntFromTo(-45,45));


		CEffect_SmokeBall* pEffect = nullptr;
		if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_SmokeBall", SCENE_STAGE, L"Layer_ESmokeBall", (CGameObject**)&pEffect)))
			_MSG_BOX("Fail Make_SmokeBall");
		pEffect->Set_Param(m_vPos +vRand, &vSmokeDir);
		//pEffect->Set_Type(1);

	}


	
	Safe_Release(pManagement);


}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_PlayerChange * CEffect_PlayerChange::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_PlayerChange*	pInst = new CEffect_PlayerChange(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_PlayerChange::Clone_GameObject()
{
	CEffect_PlayerChange*	pInst = new CEffect_PlayerChange(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_PlayerChange Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_PlayerChange::Free()
{

	CGameObject::Free();
}