#include "stdafx.h"
#include "Plank.h"
#include "Management.h"
#include "Light_Manager.h"
#include "MeshTexture.h"
_USING(Client)

CPlank::CPlank(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CPlank::CPlank(const CPlank & rhs)
	: CGameObject(rhs)
{

}


HRESULT CPlank::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CPlank::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_CurState = StandIdle;
	m_pMeshCom->Set_AnimationSet(StandIdle);
	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 1.49f;
	m_iSoundCheck = 0;
	m_fSoundTimer = 0.f;
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	return NOERROR;
}

_int CPlank::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
	{
		m_bDissolve = true;
	}
	if (m_bDissolve)
	{
		m_fDissolveTime += fTimeDelta;
	}
	if (m_fDissolveTime >= 2.5f)
		return DEAD_OBJ;
	Update_Sound(fTimeDelta);
	Plank_Check(fTimeDelta);
	ComunicateWithServer();
	State_Check();



	return _int();
}

_int CPlank::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > 10000.f)
		return 0;

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_matrix matWorld = m_pTransformCom->Get_Matrix();
	vPosition.y += 110.f;
	matWorld._41 = vPosition.y;
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);

	if (m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, 110.f))
	{
		if (!m_bDissolve) 
		{
			if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
				return -1;
			if (FAILED(m_pRendererCom->Add_StemGroup(this)))
				return -1;
		}
		else		
		{
			if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
				return -1;
		}
	}

	return _int();
}

void CPlank::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	m_pMeshCom->Play_Animation(0.0001f);
	m_pMeshCom->Setup_AnimationTime(m_CurState, m_fProgressTime);

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	_uint iPass = 0;
	if (!m_bDissolve)
		iPass = 2;
	else
		iPass = 7;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(iPass);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);
			pEffect->SetInt("numBoneInf", 1);

			if (m_bDissolve)
			{
				pEffect->SetTexture("g_DissolveTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"DissolveSpider.tga")));
				pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
				pEffect->SetFloat("g_fTimeAcc", (m_fDissolveTime / 2.5f));
			}

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	//m_pColliderCom->Render_Collider();
	Safe_Release(pEffect);
}

void CPlank::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(1);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetMatrix("g_matVP", VP);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CPlank::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CPlank::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;

	_vec3 vFront = -*m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
	D3DXVec3Normalize(&vFront, &vFront);


	m_vPosArr = new _vec3[2];
	m_vPosArr[DIR::FRONT] = vFront;
	m_vPosArr[DIR::BACK] = -vFront;


	m_pColliderCom->Set_Dist(90.f,1.5f);


	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_PLANK, this);
}

_matrix CPlank::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CPlank::Get_Key()
{
	return m_Key.c_str();
}

_int CPlank::Get_OtherOption()
{ 
	return m_iOtherOption;
}

void CPlank::State_Check()
{
	if (m_CurState == m_OldState)
		return;
	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CPlank::Plank_Check(const _float& fTimeDelta)
{
	if (m_isFalling)
		m_fProgressTime += fTimeDelta;

	if (m_isFalling && m_fProgressTime >= m_fMaxProgressTime)
	{
		m_isFalling = false;
		m_fProgressTime = m_fMaxProgressTime;
	}

	if (m_CurState == StandIdle && m_bIsPullDown) // 조건되면 동작
	{
		m_CurState = FallOnGround;
		m_isFalling = true;
		m_isBool = true;
	}
}

void CPlank::Update_Sound(const _float& fTimeDelta)
{
	if (server_data.Slasher.InterationObject == m_eObjectID)
		m_fSoundTimer += fTimeDelta;
	
	
	if (m_isFalling && 9.8f/46.f <= m_fProgressTime/m_fMaxProgressTime && m_iSoundCheck == 0)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Plank/plank_land_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_iSoundCheck++;
	}
	else if (m_fDissolveTime >= 0.1f && m_iSoundCheck == 1)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_fire_short_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_iSoundCheck++;
	}
	else if(m_fSoundTimer >= 0.8f &&m_iSoundCheck == 2)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Plank/plank_break_hit_03.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_iSoundCheck++;
	}
	else if (m_fDissolveTime >= 0.9f && m_iSoundCheck == 3)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_fire_short_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_iSoundCheck++;
	}
	else if (m_fSoundTimer >= 1.6f &&m_iSoundCheck == 4)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Plank/plank_break_hit_03.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_iSoundCheck++;
	}
	else if (m_fDissolveTime >= 1.8f && m_iSoundCheck == 5)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_fire_short_03.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_iSoundCheck++;
	}
}

void CPlank::ComunicateWithServer()
{
	if (server_data.Game_Data.WoodBoard[m_eObjectID - PLANK] == (_int)1)
		m_bIsPullDown = true;

	if (server_data.Game_Data.WoodBoard[m_eObjectID - PLANK] == (_int)2)
		m_isDead = true;
}

HRESULT CPlank::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Static
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_Plank");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	m_pMeshColl = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Mesh_Plank2_HULL");
	if (FAILED(Add_Component(L"Com_Mesh2", m_pMeshColl)))
		return E_FAIL;

	//m_fRad = 240.f;
	m_fRad = 180.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
	matLocalTransform._43 = 100.f;
	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);
	m_pColliderCom->Ready_HullMesh(SCENE_STAGE,L"Mesh_Plank");
	m_pColliderCom->Ready_RealMesh(m_pMeshColl);
	
	 

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CPlank::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);

	return NOERROR;
}


CPlank * CPlank::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPlank*	pInstance = new CPlank(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CPlank Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CPlank::Clone_GameObject()
{
	CPlank*	pInstance = new CPlank(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CPlank Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CPlank::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_PLANK, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pMeshColl);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
