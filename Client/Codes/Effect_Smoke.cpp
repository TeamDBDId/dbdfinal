#include "stdafx.h"
#include "Effect_Smoke.h"
#include "Management.h"



#include "Math_Manager.h"
_USING(Client)

CEffect_Smoke::CEffect_Smoke(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_Smoke::CEffect_Smoke(const CEffect_Smoke & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Smoke::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Smoke::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;

	
	m_vDir = _vec3(Math_Manager::CalRandFloatFromTo(-100.f,100.f), Math_Manager::CalRandFloatFromTo(-60.f, 60.f), Math_Manager::CalRandFloatFromTo(-100.f, 100.f));
	m_fRad = Math_Manager::CalRandFloatFromTo(0.f, 6.28f);
	

	return NOERROR;
}

_int CEffect_Smoke::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;


	m_fTime += _fTick;
	if (2.4f < m_fTime)
		m_isDead = true;

	m_pTransformCom->Move_V3(m_vDir*_fTick);
	m_vDir.y -= _fTick*60.f;
	m_vDir.x *= 0.98f;
	m_vDir.z *= 0.98f;

	return _int();
}

_int CEffect_Smoke::LastUpdate_GameObject(const _float & _fTick)
{
	if (nullptr == m_pRendererCom)
		return -1;


	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;


	vRight = *(_vec3*)&matView.m[0][0]*100.f;
	vUp = *(_vec3*)&matView.m[1][0]*100.f;
	vLook = *(_vec3*)&matView.m[2][0];



	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp); 
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
	m_pTransformCom->Rotation_Axis_Angle(m_fRad,&vLook);



	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;



	return _int();
}

void CEffect_Smoke::Render_GameObject()
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(_E_GENE1);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

void CEffect_Smoke::Set_Param(const _vec3 & _vPos, const void * _pVoid)
{
	_vec3 vPos = _vec3(_vPos.x + Math_Manager::CalRandFloatFromTo(-40.f, 40.f)
		, _vPos.y + Math_Manager::CalRandFloatFromTo(-40.f, 40.f)
		, _vPos.z + Math_Manager::CalRandFloatFromTo(-40.f, 40.f));

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}




HRESULT CEffect_Smoke::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Effect");


	GET_INSTANCE_MANAGEMENTR(E_FAIL);


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Mist_SubUV");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;


	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_Smoke::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);



	m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");
	//_pEffect->SetMatrix("g_matWorld", &m_matWorld);

	m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0");

	_pEffect->SetInt("g_iColumn", _uint(m_fTime*25.f) / 8);
	_pEffect->SetInt("g_iRow", _uint(m_fTime*25.f) % 8);



	_pEffect->SetFloat("g_fAlpha", 1.8f-m_fTime);

	_pEffect->SetFloat("g_fDot", 0.125f);


	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Smoke * CEffect_Smoke::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Smoke*	pInst = new CEffect_Smoke(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Smoke::Clone_GameObject()
{
	CEffect_Smoke*	pInst = new CEffect_Smoke(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("Smoke Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Smoke::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pTextureCom);


	//지울것들



	CBaseEffect::Free();
}
