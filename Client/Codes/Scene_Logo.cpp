 #include "stdafx.h"
#include "..\Headers\Scene_Logo.h"
#include "Management.h"
#include "Scene_Stage.h"
#include "Scene_Lobby.h"
#include "LoadManager.h"
#include "UI_Texture.h"
#include "MeshTexture.h"
#include "Light_Manager.h"
#include <dshow.h>
#include "SoundManager.h"

#pragma comment(lib, "Strmiids")
#pragma comment(lib, "Quartz")
#pragma comment(lib, "Strmbase")
_USING(Client)

CScene_Logo::CScene_Logo(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CScene(pGraphic_Device)
{

}

HRESULT CScene_Logo::Ready_Scene()
{

	if (FAILED(Play_Video()))
		return E_FAIL;

	/*if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;

	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;*/

	CShader* pShader = CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_ShadowCube.fx");
	if (nullptr == pShader)
		return E_FAIL;

	GET_INSTANCE(CLight_Manager)->Set_ShadowCube(pShader);

	InitializeCriticalSection(&m_CritSec);
	m_hThread = (HANDLE)_beginthreadex(nullptr, 0, LoadThreadFunc, this, 0, nullptr);


	return NOERROR;
}

_int CScene_Logo::Update_Scene(const _float & fTimeDelta)
{
	LoadEnd();
	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Logo::LastUpdate_Scene(const _float & fTimeDelta)
{
	//if (m_iLogoState == 2 && GetKeyState(VK_SPACE) & 0x8000)
	//{
	//	GET_INSTANCE_MANAGEMENTR(-1);

	//	CScene_Stage*	pNewScene = CScene_Stage::Create(m_pGraphic_Device);
	//	if (nullptr == pNewScene)
	//		return - 1;

	//	if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
	//		return -1;

	//	Safe_Release(pNewScene);
	//	Safe_Release(pManagement);

	//	return 0;

	//} 
	if (m_iLogoState == 2 && GetKeyState(VK_SPACE) & 0x8000)
	{
		GET_INSTANCE_MANAGEMENTR(-1);

		GET_INSTANCE(CSoundManager)->ChannelStop(string("Logo_BGM"));

		CScene_Lobby*	pNewScene = CScene_Lobby::Create(m_pGraphic_Device);
		if (nullptr == pNewScene)
			return -1;

		if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
			return -1;

		Safe_Release(pNewScene);
		Safe_Release(pManagement);

		return 0;
	}

	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Logo::Render_Scene()
{
}

HRESULT CScene_Logo::End_Loading()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOGO, L"Layer_UI", (CGameObject**)&m_pSkipTexture);
	m_pSkipTexture->Set_Font(L"ESC�� ���� ��ŵ", _vec2(30.f, 30.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	m_pSkipTexture->Set_Pos(_vec2(g_iBackCX*0.9f, g_iBackCY*0.9f));


	Safe_Release(pManagement);

	m_iLogoState = 1;
	return NOERROR;
}

HRESULT CScene_Logo::Ready_Prototype_GameObject()
{

	return NOERROR;
}

HRESULT CScene_Logo::Ready_Prototype_Component()
{

	return NOERROR;
}

HRESULT CScene_Logo::Play_Video()
{
	if (FAILED(CoInitialize(NULL)))
		return E_FAIL;

	if (FAILED(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&m_pGraph)))
		return E_FAIL;

	HRESULT hr;

	hr = m_pGraph->QueryInterface(IID_IMediaControl, (void**)&m_pControl);
	hr = m_pGraph->QueryInterface(IID_IMediaEvent, (void**)&m_pEvent);
	hr = m_pGraph->RenderFile(L"../Bin/Resources/Video/LoadingScreen.avi", NULL);
	hr = m_pGraph->QueryInterface(IID_IVideoWindow, (void**)&m_pVideoWin);
	
	hr =m_pVideoWin->put_WindowStyle(WS_CHILD /*| WS_CLIPSIBLINGS| WS_CLIPCHILDREN*/);
	
	if (eWINMODE == WINDOW_MODE)
		hr = m_pVideoWin->SetWindowPosition(0, 0, g_iFullCX, g_iFullCY);
	else
	{
		m_pVideoWin->put_FullScreenMode(OATRUE);
		hr = m_pVideoWin->SetWindowPosition(0, 0, g_iBackCX, g_iBackCY);
	}
	hr = m_pVideoWin->put_Owner((OAHWND)g_hWnd);
	hr =m_pVideoWin->put_MessageDrain((OAHWND)g_hWnd);
	hr = m_pVideoWin->put_AutoShow(OATRUE);
	hr =m_pVideoWin->put_Visible(OATRUE);
	//hr =m_pVideoWin->SetWindowForeground(OATRUE);
	if(eWINMODE == FULLMODE)
		m_pVideoWin->put_FullScreenMode(true);

	GET_INSTANCE(CLoadManager)->Set_IsLoding(true);

	m_pControl->Run();
	//long evCode;
	//m_pEvent->WaitForCompletion(INFINITE, &evCode);


	return NOERROR;
}

void CScene_Logo::LoadEnd()
{
	if (m_iLogoState != 1)
		return;
	
	_long ev;
	HRESULT hr;
	hr = m_pEvent->WaitForCompletion(0, &ev);
	if (hr == S_OK || KEYMGR->KeyDown(DIK_ESCAPE))
	{
		GET_INSTANCE(CLoadManager)->Set_IsLoding(false);
		m_pVideoWin->put_Visible(OAFALSE);
		m_pVideoWin->put_Owner((OAHWND)g_hWnd);
		m_pVideoWin->put_MessageDrain((OAHWND)g_hWnd);
		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Logo", SCENE_LOGO, L"Layer_UI");
		Safe_Release(pManagement);
		Safe_Release(m_pVideoWin);
		Safe_Release(m_pControl);
		Safe_Release(m_pEvent);	
		Safe_Release(m_pGraph);
		CoUninitialize();
		m_iLogoState = 2;
		SetFocus(g_hWnd);
		SetForegroundWindow(g_hWnd);
	}
}

size_t CScene_Logo::LoadThreadFunc(LPVOID lpParam)
{
	CScene_Logo* pLogo = reinterpret_cast<CScene_Logo*>(lpParam);

	EnterCriticalSection(&(pLogo->m_CritSec));

	if (FAILED(GET_INSTANCE(CLoadManager)->Add_ReplacedName()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_L_Component()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_L_DynamicMesh()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_L_StaticMesh()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_L_Texture()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_OL_Texture()))
		return E_FAIL;
	
	GET_INSTANCE(CLoadManager)->Get_Renderer()->Set_FogTexture(GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"color_swatch.tga")));
	
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_L_GameObject()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_CollMesh()))
		return E_FAIL;
	GET_INSTANCE(CSoundManager)->Load_LobbySound();

	pLogo->End_Loading();
	LeaveCriticalSection(&(pLogo->m_CritSec));

	return 0;
}

CScene_Logo * CScene_Logo::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Logo*	pInstance = new CScene_Logo(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		MessageBox(0, L"CScene_Logo Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);		
	}
	return pInstance;
}

void CScene_Logo::Free()
{
	GET_INSTANCE_MANAGEMENT;
	pManagement->Clear_Layers(SCENE_LOGO);
	Safe_Release(pManagement);

	CScene::Free();
}
