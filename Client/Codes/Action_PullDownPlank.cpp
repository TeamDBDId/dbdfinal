#include "stdafx.h"
#include "..\Headers\Action_PullDownPlank.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Plank.h"

_USING(Client)

CAction_PullDownPlank::CAction_PullDownPlank()
{
}

HRESULT CAction_PullDownPlank::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(true);
	m_bIsPlaying = true;
	m_fIndex = 0.f;
	if (!m_bIsInit)
		SetVectorPos();

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

	CTransform* pPlankTransform = (CTransform*)m_pPlank->Get_ComponentPointer(L"Com_Transform");
	_vec3 vPlankPos = *pPlankTransform->Get_StateInfo(CTransform::STATE_POSITION);
	
	_vec3 vRight = *pTransform->Get_StateInfo(CTransform::STATE_RIGHT);
	_vec3 vDir = vPlankPos - m_vOriginPos;
	D3DXVec3Normalize(&vRight, &vRight);
	D3DXVec3Normalize(&vDir, &vDir);
	_float fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vRight, &vDir)));

	if (KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		if (fAngle >= 90.f)
			pCamper->Set_State(AC::PullDownObjectLT);
		else
			pCamper->Set_State(AC::PullDownObjectRT);

		m_iState = AC::PullDownObjectRT;
	}
	else
	{
		if (fAngle >= 90.f)
			pCamper->Set_State(AC::StandPullDownObjectLT);
		else
			pCamper->Set_State(AC::StandPullDownObjectRT);
		m_iState = AC::StandPullDownObjectRT;
	}

	m_pPlank->PullDownPlank();
	Send_ServerData();
	return NOERROR;
}

_int CAction_PullDownPlank::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;

	size_t iIndex = (size_t)m_fIndex;

	if (m_iState == AC::PullDownObjectRT)
	{
		if (m_vecPos.size() == iIndex )
			return END_ACTION;

		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMeshCom->IsOverTime(0.3f))
		{
			CCamper* pCamper = (CCamper*)m_pGameObject;
			pCamper->Set_State(AC::Idle);
		}

		_vec3 vLocalPos = m_vecPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		return UPDATE_ACTION;
	}
	else if (m_iState == AC::StandPullDownObjectRT)
	{
		if (m_fIndex - 21.f > 0.f)
			return END_ACTION;

		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMeshCom->IsOverTime(0.3f))
		{
			CCamper* pCamper = (CCamper*)m_pGameObject;
			pCamper->Set_State(AC::Idle);
		}

	}


	return UPDATE_ACTION;
}

void CAction_PullDownPlank::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
	camper_data.InterationObject = 0;
}

void CAction_PullDownPlank::Send_ServerData()
{
	if (m_pPlank != nullptr)
	{
		camper_data.InterationObject = m_pPlank->GetID();
		camper_data.InterationObjAnimation = m_pPlank->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_PullDownPlank::SetVectorPos()
{
	m_vecPos.reserve(17);

	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 3.676f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 12.526f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 23.284f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 32.682f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 40.179f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 47.471f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 56.642f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 61.779f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 68.966f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 76.291f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 84.811f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 94.67f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 104.58f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 113.253f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 119.402f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 121.737f));
	
	m_bIsInit = true;
}

void CAction_PullDownPlank::Free()
{
	CAction::Free();
}
