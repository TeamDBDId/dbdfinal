#include "stdafx.h"
#include "MeatHook.h"
#include "Management.h"
#include "Light_Manager.h"
#include "Slasher.h"
#include "Spider.h"
_USING(Client)

CMeatHook::CMeatHook(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CMeatHook::CMeatHook(const CMeatHook & rhs)
	: CGameObject(rhs)
{

}


HRESULT CMeatHook::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CMeatHook::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_CurState = Idle;
	m_pMeshCom->Set_AnimationSet(Idle);
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	return NOERROR;
}

_int CMeatHook::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	//m_iCurAnimation = m_CurState;

	m_fDeltaTime = fTimeDelta;
	ComunicateWithServer();
	State_Check();
	Penetration_Check();

	if (m_fPenetrationTime > 0.f)
	{
		m_bPenetration = true;
		m_fPenetrationTime -= fTimeDelta;
	}
	else
		m_fPenetrationTime = 0.0f;

	return _int();
}

_int CMeatHook::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (!m_bPenetration)
	{
		if (m_fCameraDistance > 6000.f)
			return 0;
	} 
	if(m_fCameraDistance < 1000.f)
		m_pRendererCom->Add_CubeGroup(this);

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_matrix matWorld = m_pTransformCom->Get_Matrix();
	vPosition.y += 200.f;
	matWorld._41 = vPosition.y;
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);
	if (m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, 200.f))
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;

		if (m_bPenetration)
			m_pRendererCom->Add_StencilGroup(this);

		if (FAILED(m_pRendererCom->Add_StemGroup(this)))
			return -1;
	}
	return _int();
}

void CMeatHook::Render_GameObject()
{
	if (nullptr == m_pSkinTextures)
		return;

	if (!m_bPenetration)
	{
		if (nullptr == m_pMeshCom ||
			nullptr == m_pShaderCom)
			return;

		m_pMeshCom->Play_Animation(m_fDeltaTime);

		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(2);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();
		pEffect->End();
		//m_pColliderCom->Render_Collider();
		Safe_Release(pEffect);
	}
	else
	{
		if (nullptr == m_pMeshCom ||
			nullptr == m_pShaderCom)
			return;

		m_pMeshCom->Play_Animation(m_fDeltaTime);

		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(4);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();

	/*	pEffect->BeginPass(5);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();*/
		pEffect->End();

		/////////////////////

		//m_pColliderCom->Render_Collider();
		Safe_Release(pEffect);
	}
}

void CMeatHook::Render_Stencil()
{
	if (nullptr == m_pSkinTextures)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(5);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;

			pEffect->SetVector("g_DiffuseColor", &_vec4(1,0,0,1));

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CMeatHook::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetMatrix("g_matVP", VP);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CMeatHook::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CMeatHook::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);
	Attach_Spider();

	if (Key == nullptr)
		return;

	m_Key = Key;



	_vec3 vFront = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	D3DXVec3Normalize(&vFront, &vFront);


	m_vPosArr = new _vec3[1];
	m_vPosArr[DIR::FRONT] = vFront;

	m_pColliderCom->Set_Dist(90.f,1.f);

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_HOOK, this);
}

_matrix CMeatHook::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CMeatHook::Get_Key()
{
	return m_Key.c_str();
}

_int CMeatHook::Get_OtherOption()
{
	return m_iOtherOption;
}

void CMeatHook::SetState(STATE eState)
{
	m_CurState = eState;
	m_iCurAnimation = eState;
	CSpider::STATE eSpiderState;

	switch (eState)
	{
	case SpiderStruggle2Sacrifice:
		eSpiderState = CSpider::StruggleToSacrifice;
		break;
	case SpiderStabOut:
		eSpiderState = CSpider::StabOut;
		break;
	case SpiderStabLoop:
		eSpiderState = CSpider::StabLoop;
		break;
	case SpiderStabIN:
		eSpiderState = CSpider::StabIN;
		break;
	case SpiderReaction_OUT:
		eSpiderState = CSpider::Reaction_Out;
		break;
	case SpiderReaction_Loop:
		eSpiderState = CSpider::Reaction_Loop;
		break;
	case SpiderReaction_IN:
		eSpiderState = CSpider::Reaction_In;
		break;
	case Spider_Struggle:
		eSpiderState = CSpider::Stuggle;
		break;
	default:
		eSpiderState = CSpider::Invisible;
		break;
	}
	m_pSpider->SetState(eSpiderState);
}

void CMeatHook::State_Check()
{
	if (m_CurState != BreakEnd)
		m_fBrokenTime = 0.f;

	if (m_CurState == BreakEnd && m_fBrokenTime < 1.5f)
		m_fBrokenTime += m_fDeltaTime;
	else if (m_CurState == BreakEnd && m_fBrokenTime >= 1.5f)
		m_CurState = Broken_Idle;

	if (m_CurState == m_OldState)
		return;

	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CMeatHook::ComunicateWithServer()
{
	m_CurState = (CMeatHook::STATE)server_data.Game_Data.Hook[m_eObjectID - HOOK];
}

void CMeatHook::Attach_Spider()
{
	CManagement* pManagement = CManagement::GetInstance();
	CGameObject * pGameObject;
	pManagement->Add_GameObjectToLayer(L"GameObject_Spider", SCENE_STAGE, L"Layer_Spider", &pGameObject);

	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	if(vPos.y < -700.f)
		vPos.y -= 5.f;
	else if (vPos.y < 100.f)
		vPos.y = -5.f;
	else
		vPos.y -= 5.f;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	if (pGameObject != nullptr)
	{
		CTransform* pTransform = (CTransform*)pGameObject->Get_ComponentPointer(L"Com_Transform");
		pTransform->Set_Matrix(m_pTransformCom->Get_Matrix());
		m_pSpider = (CSpider*)pGameObject;
		m_pSpider->Set_RotationY();
		m_pSpider->SetID(m_eObjectID - HOOK);
	}

}

void CMeatHook::Penetration_Check()
{
	if (5 != exPlayerNumber)
		return;

	if (m_CurState == Broken_Idle)
	{
		m_bPenetration = false;
		return;
	}	

	GET_INSTANCE_MANAGEMENT;

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	if (nullptr == pSlasher)
	{
		Safe_Release(pManagement);
		return;
	}

	CTransform* pSlasherTransform = (CTransform*)pSlasher->Get_ComponentPointer(L"Com_Transform");

	_vec3 vSlasherPos = *pSlasherTransform->Get_StateInfo(CTransform::STATE_POSITION);
	_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_float fLength = D3DXVec3Length(&(vSlasherPos - vMyPos));

	if (fLength < 450.f)
	{
		m_bPenetration = false;
		Safe_Release(pManagement);
		return;
	}

	if (nullptr == pSlasher->Get_CarriedCamper())
		m_bPenetration = false;
	else
		m_bPenetration = true;

	Safe_Release(pManagement);
}

HRESULT CMeatHook::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Static
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_MeatHook");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;


	m_fRad = 240.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
	matLocalTransform._41 = -10.f;
	matLocalTransform._43 = 120.f;


	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		_MSG_BOX("111");
	if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, L"Mesh_MeatHook")))
		_MSG_BOX("111");

	m_pMatCamperAttach = m_pMeshCom->Find_Frame("joint_CamperAttach");
	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CMeatHook::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);
	pEffect->SetInt("numBoneInf", 1);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetVector("g_DiffuseColor", &_vec4(1,0,0,0));

	return NOERROR;
}


CMeatHook * CMeatHook::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CMeatHook*	pInstance = new CMeatHook(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CMeatHook Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CMeatHook::Clone_GameObject()
{
	CMeatHook*	pInstance = new CMeatHook(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CMeatHook Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CMeatHook::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_HOOK, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
