#include "stdafx.h"
#include "ExitDoor.h"
#include "Management.h"
#include "ExitDoorLight.h"
#include "Camper.h"

_USING(Client)

CExitDoor::CExitDoor(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CExitDoor::CExitDoor(const CExitDoor & rhs)
	: CGameObject(rhs)
	,m_SoundList(rhs.m_SoundList)
{

}


HRESULT CExitDoor::Ready_Prototype()
{
	SetUp_Sound();
	return NOERROR;
}

HRESULT CExitDoor::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_OldState = Open;
	m_CurState = Closed;
	m_pMeshCom->Set_AnimationSet(Closed);
	m_iSoundCheck = 0;
	m_iProgressSoundCheck = 0;
	m_fRatio = 0.f;
	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 20.f;
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	return NOERROR;
}

_int CExitDoor::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	if (bCanGoOut && !bOut)
	{
		bOut = true;
		EFFMGR->Make_Effect(CEffectManager::E_Exit, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	}

	if (m_fProgressTime > m_fMaxProgressTime)
	{
		if (m_fAnimationTime < 1.5f)
		{
			m_fAnimationTime += fTimeDelta;
			m_CurState = STATE::Opening;
		}		
		else
		{
			m_CurState = STATE::Open;
			m_isColl = false;
		}
	}

	m_iCurAnimation = m_CurState;
	m_fDeltaTime = fTimeDelta;
	ComunicateWithServer();
	State_Check();
	Check_LightRender();
	return _int();
}

_int CExitDoor::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	if(bCanGoOut && exPlayerNumber == 5)
		m_pRendererCom->Add_StencilGroup(this);

	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > 6000.f && !bCanGoOut)
		return 0;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	if (FAILED(m_pRendererCom->Add_StemGroup(this)))
		return -1;

	return _int();
}

void CExitDoor::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	_float fPeriod = (_float)m_pMeshCom->GetPeriod();
	_float fTimeAcc = (_float)m_pMeshCom->GetTimeAcc();
	_float fRatio = fTimeAcc / fPeriod;
	if(!(m_CurState == SwitchActivation && fRatio >= 50.f/71.f))
		m_pMeshCom->Play_Animation(m_fDeltaTime);

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			if(bCanGoOut && exPlayerNumber == 5 && j == 3)
				pEffect->BeginPass(4);
			else
				pEffect->BeginPass(18);

			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);
			pEffect->SetInt("numBoneInf", 1);
			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);

			pEffect->EndPass();
		}

	}

	pEffect->End();

	Safe_Release(pEffect);
}

void CExitDoor::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetMatrix("g_matVP", VP);
			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();



}

void CExitDoor::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CExitDoor::Render_Stencil()
{
	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)0);

	pEffect->BeginPass(28);

	m_pMeshCom->Update_Skinning((_uint)0, (_uint)3);

	D3DLOCKED_RECT lock_Rect = { 0, };
	if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
		return;

	memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(0)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

	m_pSkinTextures->UnlockRect(0);

	if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)3)))
		return;
	pEffect->SetVector("g_DiffuseColor", &_vec4(0.7f, 0.7f, 0.f, 0.7f));

	pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);
	pEffect->SetInt("numBoneInf", 1);
	pEffect->CommitChanges();

	m_pMeshCom->Render_Mesh((_uint)0, (_uint)3);

	pEffect->EndPass();

	pEffect->End();

	Safe_Release(pEffect);
}

void CExitDoor::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;

	const _matrix* pMat = m_pMeshCom->Find_Frame("joint_CrankTarget");
	m_fRad = 150.f;
	_matrix			matLocalTransform;
	_vec3			vPos;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);

	vPos = *(_vec3*)&(pMat->m[3][0]);

	matLocalTransform._41 = vPos.x;
	matLocalTransform._42 = 0.f;
	matLocalTransform._43 = vPos.z;

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		_MSG_BOX("111");
	if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, L"Mesh_ExitDoor")))
		_MSG_BOX("111");

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_EXITDOOR, this);

	SetUp_ExitDoorLight();

	_vec3 vRight = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
	D3DXVec3Normalize(&vRight, &vRight);

	m_vPosArr = new _vec3[1];
	m_vPosArr[DIR::FRONT] = vRight;
	m_pColliderCom->Set_Dist(60.f, 1.f);

}

_matrix CExitDoor::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CExitDoor::Get_Key()
{
	return m_Key.c_str();
}

_int CExitDoor::Get_OtherOption()
{
	return m_iOtherOption;
}

void CExitDoor::SetUp_Sound()
{
	Add_SoundList(SwitchActivation, 36.06f / 71.f, "ExitDoor/exitdoor_switch_lever_pull_01.ogg");
	Add_SoundList(SwitchActivation, 42.4f / 71.f, "ExitDoor/exitdoor_switch_lever_pull_02.ogg");
	Add_SoundList(SwitchActivation, 45.f / 71.f, "ExitDoor/exitdoor_switch_lever_pull_03.ogg");
	Add_SoundList(SwitchActivation, 47.f / 71.f, "ExitDoor/exitdoor_spark_short_01.ogg");
	Add_SoundList(SwitchActivation, 49.f / 71.f, "ExitDoor/exitdoor_switch_mechanic_01.ogg");
	Add_SoundList(SwitchActivation, 51.f / 71.f, "ExitDoor/exitdoor_spark_short_02.ogg");
	Add_SoundList(SwitchActivation, 54.f / 71.f, "ExitDoor/exitdoor_spark_short_03.ogg");
	Add_SoundList(SwitchActivation, 57.f / 71.f, "ExitDoor/exitdoor_spark_short_04.ogg");

	Add_SoundList(Opening, 0.f / 115.f, "ExitDoor/exitdoor_open_start_bounce_02.ogg");
	Add_SoundList(Opening, 0.2f / 115.f, "ExitDoor/exitdoor_open_start_bounce_03.ogg");
	Add_SoundList(Opening, 55.f / 115.f, "ExitDoor/exitdoor_open_stop_bounce.ogg");
}

void CExitDoor::Update_Sound()
{
	_float TimeAcc = (_float)m_pMeshCom->GetTimeAcc();
	_float Period = (_float)m_pMeshCom->GetPeriod();
	_float Ratio = TimeAcc / Period;
	Ratio = (Ratio - (int)Ratio);
	if (m_fRatio > Ratio)
	{
		m_iSoundCheck = 0;	//�ߺ�üũ
		m_fRatio = Ratio;
	}
	else if (m_fRatio < Ratio)
	{
		m_fRatio = Ratio;
		_int SoundCount = 0;
		for (auto& iter : m_SoundList)
		{
			if (m_CurState == iter.m_iState)
			{
				if (iter.OtherOption != 0)
					continue;
				if (Ratio >= iter.Ratio && SoundCount == m_iSoundCheck)
				{
					GET_INSTANCE(CSoundManager)->PlaySound(string(iter.SoundName), m_vLightPos, iter.SoundDistance,iter.SoundValue);
					m_iSoundCheck++;
					return;
				}
				SoundCount++;
			}
		}
	}
}

void CExitDoor::Update_ProgressTimeSound()
{
	_float fProgressRatio = m_fProgressTime / m_fMaxProgressTime;
	if (fProgressRatio >= 0.28f && m_iProgressSoundCheck == 0)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_mechanic_01.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.4f && m_iProgressSoundCheck == 1)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_mechanic_02.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.45f && m_iProgressSoundCheck == 2)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_mechanic_background_01.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.5f && m_iProgressSoundCheck == 3)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_mechanic_03.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.61f && m_iProgressSoundCheck == 4)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_mechanic_background_02.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.72f && m_iProgressSoundCheck == 5)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_mechanic_04.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.75f && m_iProgressSoundCheck == 6)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_alarm_02.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.83f && m_iProgressSoundCheck == 7)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_mechanic_background_03.ogg"), m_vLightPos, 3400.f,3.f);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.88f && m_iProgressSoundCheck == 9)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_mechanic_background_04.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.91f && m_iProgressSoundCheck == 8)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_alarm_02.ogg"), m_vLightPos,3400.f,3.f);
		m_iProgressSoundCheck++;
	}
	else if (fProgressRatio >= 0.95f && m_iProgressSoundCheck == 10)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("ExitDoor/exitdoor_open_start_bounce_01.ogg"), m_vLightPos);
		m_iProgressSoundCheck++;
	}
}

void CExitDoor::Add_SoundList(const _uint & m_iState, const _float Ratio, const char * SoundName, const _float & SoundDistance, const _float & SoundValue, const _int & OtherOption)
{
	SoundData Data;
	Data.m_iState = m_iState;
	Data.Ratio = Ratio;
	strcpy_s(Data.SoundName, SoundName);
	Data.SoundDistance = SoundDistance;
	Data.SoundValue = SoundValue;
	Data.OtherOption = OtherOption;
	m_SoundList.push_back(Data);
}

void CExitDoor::SetUp_ExitDoorLight()
{
	CExitDoorLight* pExitDoorLight = nullptr;
	GET_INSTANCE_MANAGEMENT;
	pManagement->Add_GameObjectToLayer(L"GameObject_ExitDoorLight", SCENE_STAGE, L"Layer_ExitDoorLight", (CGameObject**)&pExitDoorLight);
	Safe_Release(pManagement);

	m_pDoorLight = pExitDoorLight;

	_matrix matLocalWorld = m_pTransformCom->Get_Matrix();
	_matrix matLocalTrans;
	D3DXMatrixTranslation(&matLocalTrans, 383.f, 188.f, -348.f);

	matLocalWorld = matLocalTrans * matLocalWorld;
	m_vLightPos = {matLocalWorld._41, matLocalWorld._42, matLocalWorld._43};
	pExitDoorLight->Set_Base(L"m_Key", matLocalWorld);
}

void CExitDoor::State_Check()
{
	Update_ProgressTimeSound();
	Update_Sound();
	if (m_CurState == m_OldState)
		return;

	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CExitDoor::Check_LightRender()
{
	_uint index = 0;
	_float ProgressRatio = m_fProgressTime / m_fMaxProgressTime;
	if (ProgressRatio >= 0.75f)
		index = 3;
	else if (ProgressRatio >= 0.5f)
		index = 2;
	else if (m_fProgressTime > 0.25f)
		index = 1;
	else if(m_fProgressTime <= 0.25f)
		index = 0;
	m_pDoorLight->Set_LightRender(index);
}

void CExitDoor::ComunicateWithServer()
{
	m_fProgressTime = server_data.Game_Data.ExitDoor[m_eObjectID - EXITDOOR];
}

HRESULT CExitDoor::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Dynamic
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_ExitDoor");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CExitDoor::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);

	return NOERROR;
}


CExitDoor * CExitDoor::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CExitDoor*	pInstance = new CExitDoor(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CExitDoor Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CExitDoor::Clone_GameObject()
{
	CExitDoor*	pInstance = new CExitDoor(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CExitDoor Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CExitDoor::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_EXITDOOR, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
