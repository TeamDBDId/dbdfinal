#include "stdafx.h"
#include "..\Headers\Camera_Slasher.h"
#include "Management.h"
#include "Slasher.h"
#include "Action.h"

_USING(Client)

CCamera_Slasher::CCamera_Slasher(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CCamera(pGraphic_Device)
{
}

CCamera_Slasher::CCamera_Slasher(const CCamera_Slasher & rhs)
	: CCamera(rhs)
{

}

HRESULT CCamera_Slasher::Ready_Prototype()
{
	if (FAILED(CCamera::Ready_Prototype()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCamera_Slasher::Ready_GameObject()
{
	if (FAILED(CCamera::Ready_GameObject()))
		return E_FAIL;

	if (FAILED(CCamera_Slasher::Ready_Component()))
		return E_FAIL;

	m_pTransform->SetUp_Speed(5.f, D3DXToRadian(360.f));

	D3DVIEWPORT9			ViewPort;
	m_pGraphic_Device->GetViewport(&ViewPort);

	m_ptMouse.x = ViewPort.Width >> 1;
	m_ptMouse.y = ViewPort.Height >> 1;

	ClientToScreen(g_hWnd, &m_ptMouse);

	CManagement* pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return 0;

	pManagement->AddRef();

	m_pSlasher = (CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front();
	m_pMatCamFrameOfSlasher = ((CMesh_Dynamic*)m_pSlasher->Get_ComponentPointer(L"Com_Mesh"))->Find_Frame("joint_Cam_01");
	m_pMatSlasher = ((CTransform*)m_pSlasher->Get_ComponentPointer(L"Com_Transform"))->Get_Matrix_Pointer();
	m_pSlasher->Set_RendHead(false);

	Safe_Release(pManagement);

	return NOERROR;
}

_int CCamera_Slasher::Update_GameObject(const _float & fTimeDelta)
{


	return _int();
}

_int CCamera_Slasher::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pInput_Device)
		return -1;

	//if (false == m_bIsControl)
	//	return 1;

	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return -1;
	pManagement->AddRef();

	CTransform* pSlasherTransform = (CTransform*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Slasher", L"Com_Transform");
	Safe_Release(pManagement);
	if (nullptr == pSlasherTransform)
		return 0;
	pSlasherTransform->AddRef();

	_vec3 vSlasherLook = *(_vec3*)&m_pMatSlasher->m[2][0];
	vSlasherLook.y = 0.f;
	D3DXVec3Normalize(&vSlasherLook, &vSlasherLook);

	_vec3 vLookup = { 0,0,1 };

	_vec3 vCheck = vLookup ^ vSlasherLook;

	_float fAngle = vLookup * vSlasherLook;
	fAngle = acosf(fAngle);

	if (vCheck.y < 0.f)
		fAngle = D3DXToRadian(360.f) - fAngle;

	CAction* pAction = m_pSlasher->Find_Action(L"Action_Hit");
	_bool bIsPlaying = false;
	if (pAction != nullptr)
		bIsPlaying = pAction->m_bIsPlaying;
	if (!m_pSlasher->GetLockKey() || bIsPlaying)
	{
		if (m_bIsLockCam)
		{
			m_fMoveY = 0.f;
			m_bIsLockCam = !m_bIsLockCam;
		}

		if (m_fMoveY += m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_Y) * 0.1f)
		{
			if (m_fMoveY > 55.f)
				m_fMoveY = 55.f;
			else if (m_fMoveY < -35.f)
				m_fMoveY = -35.f;
		}
	}
	else
	{
		_vec3 vCamLok;
		m_bIsLockCam = m_pSlasher->GetLockKey();
		memcpy(&vCamLok, &m_pMatCamFrameOfSlasher->m[1], sizeof(_vec3));

		_vec3 vCurLook = *m_pTransform->Get_StateInfo(CTransform::STATE_LOOK);

		D3DXVec3Normalize(&vCurLook, &vCurLook);
		D3DXVec3Normalize(&vCamLok, &vCamLok);

		_vec3 vLookUp = { 0.f,1.f,0.f };

		_float CurAngle = acosf(vCurLook * vLookUp);
		_float CamAngle = acosf(vCamLok * vLookUp);

		if ((CurAngle - CamAngle) < -0.001f)
		{
			m_fMoveY -= D3DXToRadian(45.f) * fTimeDelta;
		}
		else if ((CurAngle - CamAngle) > 0.001f)
		{
			m_fMoveY += D3DXToRadian(45.f) * fTimeDelta;
		}

		_matrix mRot, mRotY;
		_vec3 Result;

		D3DXMatrixRotationAxis(&mRot, &-*(_vec3*)&m_pMatCamFrameOfSlasher->m[0], D3DXToRadian(m_fMoveY));

		D3DXVec3TransformNormal(&Result, &vCamLok, &mRot);
		D3DXVec3Normalize(&Result, &Result);

		_matrix matCamera;
		//memcpy(&matCamera.m[0], &-*(_vec4*)&m_pMatCamFrameOfSlasher->m[0], sizeof(_vec4));
		////memcpy(&matCamera.m[1], &m_pMatCamFrameOfSlasher->m[2], sizeof(_vec4));
		//memcpy(&matCamera.m[2], &_vec4(Result.x, Result.y, Result.z, 0.f), sizeof(_vec4));
		//
		//_vec4 vCameraUp = Result ^ -*(_vec3*)&m_pMatCamFrameOfSlasher->m[0];
		//memcpy(&matCamera.m[1], &_vec4(vCameraUp.x, vCameraUp.y, vCameraUp.z, 0.f), sizeof(_vec4));

		//memcpy(&matCamera.m[3], &m_pMatCamFrameOfSlasher->m[3], sizeof(_vec4));


		memcpy(&matCamera.m[0], &-*(_vec4*)&m_pMatCamFrameOfSlasher->m[0], sizeof(_vec4));
		memcpy(&matCamera.m[1], &*(_vec4*)&m_pMatCamFrameOfSlasher->m[2], sizeof(_vec4));
		memcpy(&matCamera.m[2], &*(_vec4*)&m_pMatCamFrameOfSlasher->m[1], sizeof(_vec4));
		memcpy(&matCamera.m[3], &m_pMatCamFrameOfSlasher->m[3], sizeof(_vec4));

		_matrix matSlasher;

		matSlasher = matCamera * *m_pMatSlasher;

		m_pTransform->Set_Matrix(matSlasher);

		slasher_data.vCamLook = *(_vec3*)&matSlasher.m[2];
		Safe_Release(pSlasherTransform);

		Invalidate_ViewProjMatrix();

		m_pFrustumCom->Transform_ToWorld();

		ComunicateWithServer();

		return _int();
	}

	_vec3		vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix			matRotY, matRotAxis;
	D3DXMatrixRotationY(&matRotY, fAngle);
	D3DXMatrixRotationAxis(&matRotAxis, m_pTransform->Get_StateInfo(CTransform::STATE_RIGHT), D3DXToRadian(m_fMoveY));

	D3DXVec3TransformNormal(&vRight, &vRight, &matRotY);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRotY);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRotY);

	D3DXVec3TransformNormal(&vRight, &vRight, &matRotAxis);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRotAxis);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRotAxis);

	m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransform->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &vLook);

	_vec3 vPos = *(_vec3*)&m_pMatCamFrameOfSlasher->m[3][0];
	D3DXVec3TransformCoord(&vPos, &vPos, m_pMatSlasher);

	if (KEYMGR->KeyPressing(DIK_NUMPAD0))
	{
		vPos -= vLook * 250.f;
	}

	m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	slasher_data.vCamLook = vLook;

	Safe_Release(pSlasherTransform);

	Invalidate_ViewProjMatrix();

	m_pFrustumCom->Transform_ToWorld();

	ComunicateWithServer();


	return _int();
}

void CCamera_Slasher::Render_GameObject()
{
}

HRESULT CCamera_Slasher::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

void CCamera_Slasher::ComunicateWithServer()
{
	if (5 != exPlayerNumber)
		return;

	if (nullptr == m_pTransform)
		return;

	slasher_data.vCamLook =	*m_pTransform->Get_StateInfo(CTransform::STATE_POSITION);
}

CCamera_Slasher * CCamera_Slasher::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamera_Slasher*	pInstance = new CCamera_Slasher(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCamera_Slasher Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CCamera_Slasher::Clone_GameObject()
{
	CCamera_Slasher*	pInstance = new CCamera_Slasher(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCamera_Slasher Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamera_Slasher::Free()
{
	Safe_Release(m_pFrustumCom);
	CCamera::Free();
}
