#include "stdafx.h"
#include "..\Headers\Action_BeingHeal.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Action_HealCamper.h"

_USING(Client)

CAction_BeingHeal::CAction_BeingHeal()
{
}

HRESULT CAction_BeingHeal::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D) || KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		camper_data.Packet = 3050;
		return NOERROR;
	}

	m_fEndDelay = 0.f;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_BeingHeal);
	if (pCamper->GetCurCondition() == CCamper::INJURED)
		pCamper->Set_State(AC::BeingHeal);

	else if (pCamper->GetCurCondition() == CCamper::DYING)
		pCamper->Set_State(AC::CrawlToStand);
	
	return NOERROR;
}

_int CAction_BeingHeal::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	//m_fEndDelay += fTimeDelta;
	_uint NoMedikit = 0;
	_uint UseMedikit = 0;
	_uint TotalNum = 0;
	camper_data.Packet = 0;

	_bool bMyHeal[4] = {false, false, false, false};

	for (int i = 0; i < 4; ++i)
	{
		if (i == (exPlayerNumber - 1))
			continue;

		if (!server_data.Campers[i].bConnect || !server_data.Campers[i].bLive)
			continue;

		if (server_data.Campers[i].InterationObjAnimation == exPlayerNumber)
		{
			if (server_data.Campers[i].bUseItem)
				++UseMedikit;
			else
				++NoMedikit;

			bMyHeal[i] = true;
		}

		if (server_data.Campers[i].iState == AC::HealCamperFail)
		{
			m_iState = AC::BeingHealFail;
			if (((CCamper*)m_pGameObject)->GetCurCondition() == CCamper::INJURED)
				((CCamper*)m_pGameObject)->Set_State(AC::BeingHealFail);
		}			
	}

	TotalNum = NoMedikit + UseMedikit;

	if (TotalNum == 0)
		m_fTime += fTimeDelta;

	if (m_fTime > 0.10f)
		return END_ACTION;


	_float fHealAcc = _float((_float(NoMedikit) * 1.4f) + (_float(UseMedikit) * 1.9f));

	switch (TotalNum)
	{
	case 2:
		fHealAcc *= 0.8f;
		break;
	case 3:
		fHealAcc *= 0.7f;
		break;
	default:
		break;
	}

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetHeal(pCamper->GetHeal() + (fTimeDelta * fHealAcc));

	//if (m_fEndDelay > 0.2f)
	//{

	if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D) || KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		camper_data.InterationObject = 0;
		camper_data.InterationObjAnimation = 0;
		camper_data.SecondInterationObject = 0;
		camper_data.SecondInterationObjAnimation = 0;
		return END_ACTION;
	}

	//}
	
	//CManagement* pManagement = CManagement::GetInstance();
	//list<CGameObject*>* pCamperList = pManagement->Get_ObjList(SCENE_STAGE, L"Layer_Camper");

	//if (pCamperList == nullptr)
	//	return END_ACTION;


		//HealCamperFail
	if (m_iState == AC::BeingHealFail)
	{
		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMeshCom->IsOverTime(0.3f))
		{
			m_iState = AC::BeingHeal;
			((CCamper*)m_pGameObject)->Set_State(AC::BeingHeal);
		}
	}	

	if (pCamper->GetHeal() >= pCamper->GetMaxHeal())
	{
		if (pCamper->GetCurCondition() == CCamper::INJURED)
		{
			pCamper->SetCurCondition(CCamper::HEALTHY);
			pCamper->Set_State(AC::Idle);
			pCamper->SetHeal(0.f);
		}
		else
		{
			pCamper->SetCurCondition(CCamper::INJURED);
			pCamper->Set_State(AC::Injured_Idle);
			pCamper->SetHeal(0.f);
			pCamper->SetDyingEnergy();
		}
		
		//camper_data.SecondInterationObject = pCamper->GetID();
		//camper_data.SecondInterationObjAnimation = CLEAR_PACKET;
		return END_ACTION;
		
	}

	return UPDATE_ACTION;
}

void CAction_BeingHeal::End_Action()
{
	camper_data.Packet = 3050;

	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (pCamper == nullptr)
		return;

	if(pCamper->GetCurCondition() == CCamper::DYING)
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_DyingHeal);

	pCamper->IsLockKey(false);
	pCamper->Set_State(AC::Idle);
	m_pCamper = nullptr;
	m_fTime = 0.0f;
	m_fDelay = 0.5f;
}

void CAction_BeingHeal::Send_ServerData()
{
}

void CAction_BeingHeal::Free()
{
	CAction::Free();
}
