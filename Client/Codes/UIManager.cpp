#include "stdafx.h"
#include "UIManager.h"
#include "UI_Interaction.h"
_USING(Client)
_IMPLEMENT_SINGLETON(CUIManager)

CUIManager::CUIManager()
{
}

void CUIManager::Add_Button(const wstring & TextureName, const wstring & TextName)
{
	if (m_pUIInteraction == nullptr)
		return;
	m_pUIInteraction->Add_Button(TextureName, TextName);
}

_int CUIManager::Get_SkillCheckState()
{
	_int State = m_iSkillCheckState; 
	if(State != 1 && State != 2)
		m_iSkillCheckState = 0; 
	return State;
}

void CUIManager::Set_ProgressBar(ProgressState State, _int iItem)
{
	if (m_pUIInteraction == nullptr)
		return;
	m_pUIInteraction->Set_ProgressBar(State, iItem);
}

void CUIManager::Set_ProgressPoint(const _float & fProgress, const _float & fMaxProgress)
{
	if (m_pUIInteraction == nullptr)
		return;
	m_pUIInteraction->Set_ProgressPoint(fProgress, fMaxProgress);
}

void CUIManager::Set_KeyEvent(KeyEvent KeyEvent)
{
	if (m_pUIInteraction == nullptr)
		return;
	m_pUIInteraction->Set_KeyEvent(KeyEvent);
}

void CUIManager::Add_Score(const _uint & Type, const wstring & Text, const _int & Point)
{
	if (Point <= 0)
		return;
	m_pUIScore->Add_Score(Type, Text, Point);
}

void CUIManager::Set_CalZZi()
{
	if (m_pUIInteraction == nullptr)
		return;
	m_pUIInteraction->IsSkillCheck();
}

CUIManager::KeyEvent CUIManager::Get_KeyEvent()
{
	if (m_pUIInteraction == nullptr)
		return EV_None;
	return m_pUIInteraction->Get_KeyEvent();
}

void CUIManager::Free()
{
	m_pUIScore = nullptr;
	m_pUIInteraction = nullptr;
}
