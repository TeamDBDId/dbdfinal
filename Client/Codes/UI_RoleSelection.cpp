#include "stdafx.h"
#include "Management.h"
#include "UI_Texture.h"
#include "UI_RoleSelection.h"

_USING(Client);


CUI_RoleSelection::CUI_RoleSelection(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_RoleSelection::CUI_RoleSelection(const CUI_RoleSelection & rhs)
	: CGameObject(rhs)
	, m_iClickButton(rhs.m_iClickButton)
	
{
}


HRESULT CUI_RoleSelection::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_RoleSelection::Ready_GameObject()
{
	_uint				m_CurButton = NONE;
	_uint				m_OldButton = NONE;
	GET_INSTANCE(CSoundManager)->ChannelVolume(string("Lobby_Burning"), 0.5f);
	GET_INSTANCE(CSoundManager)->PlaySound(string("RoleSelection"),string("Lobby/Ambience_Menus_am_ui_roleselection_01.ogg"), _vec3(), 0, 0.5f);
	m_iClickButton = NONE;
	Set_Background();
	Set_Lobby();
	return NOERROR;
}

_int CUI_RoleSelection::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	Mouse_Check();
	Lobby_Check();
	return _int();
}

_int CUI_RoleSelection::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CUI_RoleSelection::Set_Lobby()
{
	GET_INSTANCE_MANAGEMENT;
	
	CUI_Texture* pTexture;
	_vec2 vPos = { 0.38f,0.74f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Lobby_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	m_UITexture.insert({ L"Camper_Panel", pTexture });

	vPos = { 0.38f,0.7f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Icon_Camper.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.9f, 0.9f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Camper_Icon", pTexture });

	vPos = { 0.62f,0.74f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Lobby_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	//pTexture->Set_Alpha(0.3f);
	m_UITexture.insert({ L"Slasher_Panel", pTexture });

	vPos = { 0.62f,0.7f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Icon_Slasher.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Slasher_Icon", pTexture });

	vPos = { 0.38f,0.85f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"생존자로 플레이"), _vec2(20.f, 20.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Camper_Font", pTexture });
	vPos = { 0.62f,0.85f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"살인마로 플레이"), _vec2(20.f, 20.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Slasher_Font", pTexture });

	Safe_Release(pManagement);
}

void CUI_RoleSelection::Set_Background()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;
	_vec2 vPos = { 0.544559f,0.720623f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Lobby1_Background.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(1.f, 1.f));
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"Lobby_Background", pTexture });

	Safe_Release(pManagement);
}

void CUI_RoleSelection::Button_L1_Camper(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"Camper_Icon"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"Camper_Font"));
		pTexture->Set_Alpha(1.f);
		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Lobby_Panel_Frame.tga"));
		pTexture->Set_Pos(_vec2(0.38f* g_iBackCX, 0.74f * g_iBackCY));
		pTexture->Set_Scale(_vec2(210.f, 210.f),false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"Camper_Frame", pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
		pTexture->Set_Font(wstring(L"생존자"), _vec2(17.f, 19.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		pTexture->Set_Pos(_vec2(0.5f* g_iBackCX, 0.54f * g_iBackCY));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"Camper_Name", pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
		pTexture->Set_Font(wstring(L"생존자로 게임을 합니다. 살인마로부터 도망쳐 살아남으세요."), _vec2(12.f, 14.f), D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.f), 0);
		pTexture->Set_Pos(_vec2(0.5f* g_iBackCX, 0.57f * g_iBackCY));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"Camper_Info", pTexture });
		
		Safe_Release(pManagement);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"Camper_Icon"));
		pTexture->Set_Alpha(0.3f);
		pTexture = Find_UITexture(wstring(L"Camper_Font"));
		pTexture->Set_Alpha(0.3f);
		pTexture = Find_UITexture(wstring(L"Camper_Frame"));
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"Camper_Frame"));
		pTexture = Find_UITexture(wstring(L"Camper_Name"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"Camper_Name"));
		pTexture = Find_UITexture(wstring(L"Camper_Info"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"Camper_Info"));
	}
}

void CUI_RoleSelection::Button_L1_Slasher(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"Slasher_Icon"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"Slasher_Font"));
		pTexture->Set_Alpha(1.f);
		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Lobby_Panel_Frame.tga"));
		pTexture->Set_Pos(_vec2(0.62f* g_iBackCX, 0.74f * g_iBackCY));
		pTexture->Set_Scale(_vec2(210.f, 210.f), false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"Slasher_Frame", pTexture });
		
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
		pTexture->Set_Font(wstring(L"살인마"), _vec2(17.f, 19.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		pTexture->Set_Pos(_vec2(0.5f* g_iBackCX, 0.54f * g_iBackCY));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"Slasher_Name", pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby1", (CGameObject**)&pTexture);
		pTexture->Set_Font(wstring(L"살인마로 게임을 합니다. 엔티티가 만족할 수 있도록 생존자를 희생시키세요."), _vec2(12.f, 14.f), D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.f), 0);
		pTexture->Set_Pos(_vec2(0.5f* g_iBackCX, 0.57f * g_iBackCY));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"Slasher_Info", pTexture });

		Safe_Release(pManagement);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"Slasher_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"Slasher_Font"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"Slasher_Frame"));
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"Slasher_Frame"));
		pTexture = Find_UITexture(wstring(L"Slasher_Name"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"Slasher_Name"));
		pTexture = Find_UITexture(wstring(L"Slasher_Info"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"Slasher_Info"));
	}
}


CUI_RoleSelection * CUI_RoleSelection::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_RoleSelection*	pInstance = new CUI_RoleSelection(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_RoleSelection Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_RoleSelection::Clone_GameObject()
{
	CUI_RoleSelection*	pInstance = new CUI_RoleSelection(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_RoleSelection Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


void CUI_RoleSelection::Mouse_Check()
{
	CUI_Texture* pTexture = nullptr;
	pTexture = Find_UITexture(wstring(L"Camper_Panel"));
	if (pTexture->IsTouch())
	{
		if (m_CurButton == CAMPER)
			return;
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"),_vec3(),0, 1.f);
		m_CurButton = CAMPER;
		return;
	}
	pTexture = Find_UITexture(wstring(L"Slasher_Panel"));
	if (pTexture->IsTouch())
	{
		if (m_CurButton == SLASHER)
			return;
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_02.ogg"), _vec3(), 0, 1.f);
		m_CurButton = SLASHER;
		return;
	}
	m_CurButton = NONE;
}

void CUI_RoleSelection::Lobby_Check()
{
	if (m_CurButton != m_OldButton)
	{
		if (m_CurButton == CAMPER)
			Button_L1_Camper(Touch);
		else if (m_CurButton == SLASHER)
			Button_L1_Slasher(Touch);
		if (m_OldButton == CAMPER)
			Button_L1_Camper(UnTouch);
		else if (m_OldButton == SLASHER)
			Button_L1_Slasher(UnTouch);
		m_OldButton = m_CurButton;
	}

	if (KEYMGR->MouseDown(0) && m_CurButton != NONE)
	{
		m_iClickButton = m_CurButton;
		Delete_All();
		GET_INSTANCE(CSoundManager)->ChannelStop(string("RoleSelection"));
	}
}

CUI_Texture * CUI_RoleSelection::Find_UITexture(wstring UIName)
{
	auto& iter = m_UITexture.find(UIName);
	if (iter == m_UITexture.end())
		return nullptr;
	return iter->second;
}

void CUI_RoleSelection::Delete_UI_Texture(wstring UIName)
{
	auto& iter = m_UITexture.find(wstring(UIName));
	m_UITexture.erase(iter);
}

void CUI_RoleSelection::Delete_All()
{
	for (auto& iter : m_UITexture)
	{
		iter.second->Set_DeadTime(0.3f);
		iter.second->Set_FadeOut(0.3f);
	}
	m_UITexture.clear();
}

void CUI_RoleSelection::Free()
{
	CGameObject::Free();
}
