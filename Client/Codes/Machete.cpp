#include "stdafx.h"
#include "..\Headers\Machete.h"
#include "Management.h"
#include "Defines.h"
#include "Slasher.h"
#include "MeshTexture.h"
#include "AfterImage.h"
#include "Camper.h"

_USING(Client)

CMachete::CMachete(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CMachete::CMachete(const CMachete & rhs)
	: CGameObject(rhs)
{
}

HRESULT CMachete::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CMachete::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	D3DXMatrixIdentity(&m_matParent);
	m_pTransformCom->Set_Parent(&m_matParent);
	Approach_Slasher();
	
	_float fX = D3DXToRadian(90.f);
	_float fY = D3DXToRadian(0.f);
	_float fZ = D3DXToRadian(0.f);

	m_pTransformCom->SetUp_RotateXYZ(&fX, &fY, &fZ);
	return NOERROR;

	m_isColl = false;
	m_bRender = true;
}

_int CMachete::Update_GameObject(const _float & fTimeDelta)
{
	_matrix worldMat = m_pTransformCom->Get_WorldxParentMatrix();
	_vec3 vUp = *(_vec3*)&worldMat._21;

	//cout << vUp.x << " ";
	//cout << vUp.y << " ";
	//cout << vUp.z << endl;
	if (m_isDead)
		return 1;

	//if (server_data.Slasher.iState == AW::TW_Attack_Swing_FPV)
		m_bRenderAfterImage = true;

	Make_AfterImage();

	SkillTime_Check();
	Check_SpiritAttack(fTimeDelta);

	return _int();
}

_int CMachete::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	if (server_data.Slasher.iCharacter == 1 && server_data.Slasher.iState == AS::Mori && !m_bRender)
		return 0;

	if (nullptr != m_pRHandMatrix)
		m_matParent = *m_pRHandMatrix * m_pSlasherTransform->Get_Matrix();

	if (server_data.Slasher.iCharacter == 0 && SkillTime_Check())
		return 0;

	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	D3DXVec3TransformCoord(&vPos, &vPos, &m_matParent);
	
	if (server_data.Slasher.iCharacter == CSlasher::C_SPIRIT && exPlayerNumber == 5)
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
			return -1;

		return 0;
	}
	else if (server_data.Slasher.iCharacter == CSlasher::C_SPIRIT && exPlayerNumber != 5)
	{
		if (server_data.Slasher.iSkill != 0)
			return 0;
		else
		{
			if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
				return -1;

			return 0;
		}
	}

	if (server_data.Slasher.iCharacter == CSlasher::C_WRAITH && m_bSkillUse)
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
			return -1;
	}
	else
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;

		if (FAILED(m_pRendererCom->Add_StemGroup(this)))
			return -1;
	}

	return _int();
}

void CMachete::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	_uint iPass = 9;
	if (m_bSkillUse)
		iPass = 7;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (server_data.Slasher.iCharacter == CSlasher::C_SPIRIT && 0 == i)
			iPass = 9;
		else if (server_data.Slasher.iCharacter == CSlasher::C_SPIRIT && 0 != i)
			iPass = 21;

		pEffect->BeginPass(iPass);

		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
		pEffect->EndPass();
	}

	pEffect->End();

	//m_pColliderCom->Render_Collider();

	//m_pAfterImageCollider[0]->Render_Collider();
	//m_pAfterImageCollider[1]->Render_Collider();

	Safe_Release(pEffect);

}

void CMachete::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	pEffect->SetVector("g_vLightPos", &vLightPos);
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();
}

void CMachete::Render_Stemp()
{
	LPD3DXEFFECT	pEffect = m_pRendererCom->GetCubeEffectHandle()->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(13);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(i);
		if (nullptr == pSubSet)
			return;

		m_pMeshCom->Render_Mesh(i);

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

HRESULT CMachete::Set_Index(_uint iIndex)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	if (iIndex == 0)
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Weapon_Machete");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return E_FAIL;

		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, 120.f, 120.f, 120.f);
		matLocalTransform.m[3][2] = 40.f;

		m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, &m_matParent));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			return E_FAIL;
	}
	else
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Weapon_S_Machete");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return E_FAIL;

		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, 150.f, 150.f, 150.f);
		matLocalTransform.m[3][2] = 40.f;

		m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, &m_matParent));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			return E_FAIL;

		m_bSpiritDissolve = true;
	}

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_WEAPON, this);

	if (5 == exPlayerNumber)
	{
		CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
		pSlasher->Set_Weapon(this);
	}
	m_isColl = false;

	//

	_matrix			matLocalTransform;

	D3DXMatrixIdentity(&matLocalTransform);
	D3DXMatrixScaling(&matLocalTransform, 30.f, 30.f, 30.f);
	matLocalTransform.m[3][2] = 40.f;

	m_pAfterImageCollider[0] = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, &m_matParent));
	if (FAILED(Add_Component(L"Com_AfterCollider1", m_pAfterImageCollider[0])))
		return E_FAIL;

	D3DXMatrixIdentity(&matLocalTransform);
	D3DXMatrixScaling(&matLocalTransform, 30.f, 30.f, 30.f);
	matLocalTransform.m[3][2] = 80.f;

	m_pAfterImageCollider[1] = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, &m_matParent));
	if (FAILED(Add_Component(L"Com_AfterCollider2", m_pAfterImageCollider[1])))
		return E_FAIL;


	Safe_Release(pManagement);
	return NOERROR;
}

_int CMachete::Do_Coll(CGameObject * _pObj, const _vec3 & _vPos)
{
	if (5 == exPlayerNumber)
	{
		if (_pObj->GetID() & CAMPER)
		{
			if (((CCamper*)_pObj)->GetCurCondition() == CCamper::INJURED
				|| ((CCamper*)_pObj)->GetCurCondition() == CCamper::HEALTHY
				|| ((CCamper*)_pObj)->GetCurCondition() == CCamper::SPECIAL
				|| ((CCamper*)_pObj)->GetCurCondition() == CCamper::OBSTACLE)
			{
				m_pBeHitObject = _pObj;
				m_isColl = false;
				slasher_data.SecondInterationObject = _pObj->GetID();
				slasher_data.SecondInterationObjAnimation = PLAYERATTACK;
				EFFMGR->Make_Effect(CEffectManager::E_BloodUI);

				GET_INSTANCE_MANAGEMENTR(0);

				if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").size() != 0)
					((CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front())->Set_AngerGauge(0.f);
				Safe_Release(pManagement);
			}
		}
		else
		{
			m_pBeHitObject = _pObj;
			m_isColl = false;
		}
	}

	return _int();
}

HRESULT CMachete::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CMachete::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);

	if (server_data.Slasher.iCharacter == 0 && m_bSkillUse)
	{
		//_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

		//D3DXVec3TransformCoord(&vPos, &vPos, &m_matParent);
		//_matrix matWorld = m_pTransformCom->Get_Matrix();
		//memcpy(&matWorld.m[3], &vPos, sizeof(_vec3));
		//pEffect->SetMatrix("g_matWorld", &m_matParent);

		pEffect->SetFloat("g_fFloorHeight", m_fHeight);
		pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
		pEffect->SetTexture("g_DissolveTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_SmokeTile.tga")));
		pEffect->SetFloat("g_fTimeAcc", m_fDissolveTime);	
	}
	else if (server_data.Slasher.iCharacter == CSlasher::C_SPIRIT && iAttributeID != 0)
	{
		pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_Dissolve.tga")));
		pEffect->SetFloat("g_fTimeAcc", m_fDissolveTime);
	}

	//if(server_data.Slasher.iCharacter == 1 && )
	pEffect->SetBool("g_isFootPrint", false);
	return NOERROR;
}

void CMachete::Approach_Slasher()
{
	GET_INSTANCE_MANAGEMENT;

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	m_pRHandMatrix = pSlasher->Get_RHandMatrix();
	m_pSlasherTransform = (CTransform*)pSlasher->Get_ComponentPointer(L"Com_Transform");
	
	Safe_Release(pManagement);
}

_bool CMachete::SkillTime_Check()
{
	if (server_data.Slasher.iCharacter != CSlasher::C_WRAITH)
		return false;

	_bool bRend = false;

	GET_INSTANCE_MANAGEMENTR(false);

	if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").size() == 0)
	{
		Safe_Release(pManagement);
		return false;
	}

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front();
	Safe_Release(pManagement);
	if (nullptr == pSlasher)
		return false;

	m_bSkillUse = pSlasher->Get_UseSkill();
	m_fDissolveTime = pSlasher->Get_DissolveTime();
	m_fHeight = pSlasher->Get_Height();

	if (pSlasher->Get_Stealth() && !m_bSkillUse)
		bRend = true;

	return bRend;
}

void CMachete::Check_SpiritAttack(const _float& fTimeDelta)
{
	if (exPlayerNumber != 5 && server_data.Slasher.iCharacter == 1)
	{
		if (server_data.Slasher.iState == AS::Mori)
			return;

		if (server_data.Slasher.iState == AS::Attack_Swing
			|| server_data.Slasher.iState == AS::Attack_In
			|| server_data.Slasher.iState == AS::Attack_Bow
			|| server_data.Slasher.iState == AS::Attack_Miss_Out
			|| server_data.Slasher.iState == AS::Attack_Wipe
			|| server_data.Slasher.iState == AS::Damage_Generator)
		{
			m_bSpiritDissolve = false;
			m_bRender = true;
		}	
		else
			m_bSpiritDissolve = true;
	}

	if (!m_bSpiritDissolve)
	{
		if (m_fSkillTime > 0.0f)
		{
			m_fSkillTime -= fTimeDelta;
			m_fDissolveTime = m_fSkillTime * 5.f;
		}
		else
		{
			m_fSkillTime = 0.0f;
			m_fDissolveTime = 0.0f;
		}
	}
	else
	{
		if (m_fSkillTime < 0.2f)
		{
			m_fSkillTime += fTimeDelta * 0.17f;
			m_fDissolveTime = m_fSkillTime * 5.f;
		}
		else
		{
			m_fSkillTime = 0.2f;
			m_fDissolveTime = 1.f;
		}
	}
}

void CMachete::Make_AfterImage()
{
	if (nullptr == m_pAfterImageCollider[0] || nullptr == m_pAfterImageCollider[1])
		return;

	GET_INSTANCE_MANAGEMENT;

	//if (!m_bRenderAfterImage)
	//{
	//	((CAfterImage*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_AfterImage").front())->ClearBuffer();
	//	Safe_Release(pManagement);
	//	return;
	//}

	_vec3 vLCenter[2];
	vLCenter[0] = m_pAfterImageCollider[0]->Get_Center();
	vLCenter[1] = m_pAfterImageCollider[1]->Get_Center();

	if (vLCenter[0].x <= 100.f && vLCenter[0].z <= 100.f)
	{
		Safe_Release(pManagement);
		return;
	}

	((CAfterImage*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_AfterImage").front())->SetInfo(vLCenter[1], vLCenter[0]);

	Safe_Release(pManagement);
}

CMachete * CMachete::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CMachete*	pInstance = new CMachete(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CMachete Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CMachete::Clone_GameObject()
{
	CMachete*	pInstance = new CMachete(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CArt_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CMachete::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_WEAPON, this);

	Safe_Release(m_pAfterImageCollider[0]);
	Safe_Release(m_pAfterImageCollider[1]);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
