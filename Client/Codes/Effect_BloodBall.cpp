#include "stdafx.h"
#include "Effect_BloodBall.h"
#include "Management.h"



#include "Math_Manager.h"
_USING(Client)

CEffect_BloodBall::CEffect_BloodBall(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_BloodBall::CEffect_BloodBall(const CEffect_BloodBall & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_BloodBall::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_BloodBall::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_fSize = 10.f;

	m_fRot = Math_Manager::CalRandFloatFromTo(0.f,6.28f);
	m_pTransformCom->Scaling(m_fSize, m_fSize, 0.f);
	return NOERROR;
}

_int CEffect_BloodBall::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;


	m_fTime += _fTick;
	if (0.7f < m_fTime)
		m_isDead = true;

	m_pTransformCom->Move_V3(m_vDir*_fTick);
	m_pTransformCom->Move_Gravity(_fTick);

	m_fSize = 20.f + m_fTime*100.f;

	return _int();
}

_int CEffect_BloodBall::LastUpdate_GameObject(const _float & _fTick)
{
	if (nullptr == m_pRendererCom)
		return -1;



	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;


	vRight = *(_vec3*)&matView.m[0][0] * m_fSize;
	vUp = *(_vec3*)&matView.m[1][0] * m_fSize;
	vLook = *(_vec3*)&matView.m[2][0] * m_fSize;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);

	m_pTransformCom->Rotation_Axis_Angle(m_fRot, &vLook);





	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;


	return _int();
}

void CEffect_BloodBall::Render_GameObject()
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(2);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

void CEffect_BloodBall::Set_Param(const _vec3 & _vPos, const void* _pVoid)
{
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vPos);
	

	m_vDir = *(_vec3*)_pVoid*150.f;
	//vDir *= 3.14f;

	//m_pTransformCom->SetUp_RotateXYZ(&vDir.z, &vDir.y, &vDir.x);

	
	//_vec3 vCompare = vDir;
	//vCompare.y = 0.f;

	//D3DXVec3Normalize(&vDir, &vDir);
	//D3DXVec3Normalize(&vCompare, &vCompare);

	//m_fRot = D3DXVec3Dot(&vDir,&vCompare);

	m_iNum = Math_Manager::CalRandIntFromTo(5,7);

}






HRESULT CEffect_BloodBall::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Blood");


	GET_INSTANCE_MANAGEMENTR(E_FAIL);


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;



	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_BloodBall");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;
	//m_pTextureCom[1] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_BloodAnim2");
	//if (FAILED(Add_Component(L"Com_Texture1", m_pTextureCom[1])))
	//	return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_BloodBall::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);

	m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");


	m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0");

	_pEffect->SetInt("g_iColumn", m_iNum);
	_pEffect->SetInt("g_iRow", _uint(m_fTime*11.f));

	_float fAlpha = 1.f;
	if (0.4f < m_fTime)
		fAlpha = (0.5f - m_fTime)*10.f;

	_pEffect->SetFloat("g_fAlpha", fAlpha);

	_pEffect->SetFloat("g_fDot", 0.125f);
	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_BloodBall * CEffect_BloodBall::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_BloodBall*	pInst = new CEffect_BloodBall(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_BloodBall::Clone_GameObject()
{
	CEffect_BloodBall*	pInst = new CEffect_BloodBall(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_BloodBall Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_BloodBall::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pTextureCom);
	//지울것들



	CBaseEffect::Free();
}
