#include "stdafx.h"
#include "..\Headers\MainApp.h"
#include "Scene_Logo.h"
#include "Management.h"
#include "Camera_Debug.h"
#include "Input_Device.h"
#include "LoadManager.h"
#include "EffectManager.h"
#include "FontManager.h"
#include "Light_Manager.h"
#include "MeshTexture.h"

// 객체가 지형위에 탈수 있게만드는작업. 
// 유아이
// 말로 : 절두체, 큐브의 충돌. 

_USING(Client)
 
CMainApp::CMainApp()
	: m_pManagement(CManagement::GetInstance())
{
	m_pManagement->AddRef();
}

HRESULT CMainApp::Ready_MainApp()
{
	srand((unsigned)time(NULL));

	int abcd = sizeof(Server_Data);

	eWINMODE = WINDOWMODE;
	bStartGameServer = false;
	bUseServer = false;
	ActiveOption = false;
	exPlayerNumber = 0;
	exCharacterNum = 0;
	exCountPlayerNum = 0;

	ZeroMemory(&camper_data, sizeof(CamperData));

	if (FAILED(Ready_Default_Setting(eWINMODE, g_iFullCX, g_iFullCY)))
		return E_FAIL;

	//if (FAILED(D3DXCreateVolumeTextureFromFileEx(m_pGraphic_Device, )))
	//	return E_FAIL;

	CLight_Manager::GetInstance()->Ready_LightManager(m_pGraphic_Device);

	if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;

	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;

	if (FAILED(Ready_Start_Scene(SCENE_LOGO)))
		return E_FAIL;

	return NOERROR;
}

_int CMainApp::Update_MainApp(const _float & fTimeDelta)
{
	if (nullptr == m_pManagement)
		return -1;
	
	if (KEYMGR->KeyDown(DIK_P))
	{
		GET_INSTANCE(CLoadManager)->Set_MouseVisible(!m_IsMouseVisible);
		POINT Mouse;
		GetCursorPos(&Mouse);
		ScreenToClient(g_hWnd, &Mouse);
		//cout << Mouse.x << " " << Mouse.y << endl;
	}
	if (GET_INSTANCE(CLoadManager)->Get_MouseVisible() != m_IsMouseVisible)
	{
		m_IsMouseVisible = !m_IsMouseVisible;
		ShowCursor(m_IsMouseVisible);
	}

	if (!m_IsMouseVisible)
	{
		POINT Mouse = { _long(g_iBackCX*0.5f) ,_long(g_iBackCY*0.5f) };
		ClientToScreen(g_hWnd, &Mouse);
		SetCursorPos(Mouse.x, Mouse.y);
	}

	fWorldTimer = fTimeDelta;
	m_fTimeAcc += fTimeDelta;

	if (nullptr != m_pRenderer)
	{
		m_pRenderer->Set_TimeDelta(fTimeDelta);
	}

	CInput_Device::GetInstance()->SetUp_InputState();

	KEYMGR->KeyMgr_Update();
	GET_INSTANCE(CSoundManager)->Update_Sound();
	return m_pManagement->Update_Management(fTimeDelta);
}

void CMainApp::Render_MainApp()
{
	if (nullptr == m_pGraphic_Device)
		return;

	if (nullptr == m_pManagement)
		return;
	if (m_pRenderer == nullptr)
	{
		m_pRenderer = GET_INSTANCE(CLoadManager)->Get_Renderer();
		if (m_pRenderer != nullptr)
			m_pRenderer->AddRef();
	}
	if (!GET_INSTANCE(CLoadManager)->Get_IsLoading())
	m_pGraphic_Device->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, D3DXCOLOR(1.f,1.f,1.f,1.f), 1.f, 0);

	m_pGraphic_Device->BeginScene();

	// 객체들을 렌더링한다.
	if (nullptr != m_pRenderer)
		m_pRenderer->Render_RenderGroup();


	// 씬의 렌더를 호출한다.
	m_pManagement->Render_Management();

	m_pGraphic_Device->EndScene();

	if (!GET_INSTANCE(CLoadManager)->Get_IsLoading())
		m_pGraphic_Device->Present(nullptr, nullptr, 0, nullptr);
	//++m_dwRenderCnt;

	//if (m_fTimeAcc >= 1.f)
	//{
		//_uint remainRem = m_pGraphic_Device->GetAvailableTextureMem();
		////cout << remainRem << endl;
		//wsprintf(m_szFPS, L"FPS:%d", m_dwRenderCnt);
		//m_dwRenderCnt = 0;
		//m_fTimeAcc = 0.f;
	//}

	SetWindowText(g_hWnd, m_szFPS);
}

HRESULT CMainApp::Ready_Default_Setting(_uint eMode, const _uint& iSizeX, const _uint& iSizeY)
{
	if (FAILED(CGraphic_Device::GetInstance()->Ready_Graphic_Device(CGraphic_Device::WINMODE(eMode), g_hWnd, iSizeX, iSizeY, &m_pGraphic_Device)))
		return E_FAIL;

	if (FAILED(CInput_Device::GetInstance()->Ready_Input_Device(g_hInst, g_hWnd)))
		return E_FAIL;

	if (FAILED(m_pManagement->Ready_Management(SCENE_END)))
		return E_FAIL;	

	if (FAILED(CEffectManager::GetInstance()->Ready_Effect(m_pGraphic_Device)))
	{
		_MSG_BOX("Fail Effect_Manager Ready");
		return E_FAIL;
	}
	GET_INSTANCE(CLoadManager)->Set_GraphicDevice(m_pGraphic_Device);
	GET_INSTANCE(CFontManager)->Set_GraphicDevice(m_pGraphic_Device);
	return NOERROR;
}

HRESULT CMainApp::Ready_Prototype_GameObject()
{
	//// For.GameObject_Camera_Debug

	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"GameObject_Camera_Debug", CCamera_Debug::Create(m_pGraphic_Device))))
		return E_FAIL;

	return NOERROR;
}

HRESULT CMainApp::Ready_Prototype_Component()
{
	if (nullptr == m_pManagement)
		return E_FAIL;
	
	// For.Component_Transform
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Transform", CTransform::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Buffer_TriCol
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Buffer_TriCol", CBuffer_TriCol::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Buffer_RcTex
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Buffer_RcTex", CBuffer_RcTex::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Shader_Default
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Default", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Default.fx"))))
		return E_FAIL;

	return NOERROR;
}

HRESULT CMainApp::Ready_Start_Scene(SCENEID eSceneID)
{
	if (nullptr == m_pManagement)
		return E_FAIL;

	CScene*			pScene = nullptr;

	switch (eSceneID)
	{
	case SCENE_LOGO:
		pScene = CScene_Logo::Create(m_pGraphic_Device);
		break;
	case SCENE_STAGE:
		break;
	case SCENE_LOBBY:
		break;
	}

	if (nullptr == pScene)
		return E_FAIL;

	if (FAILED(m_pManagement->SetUp_ScenePointer(pScene)))
		return E_FAIL;

	Safe_Release(pScene);

	return NOERROR;
}

CMainApp * CMainApp::Create()
{
	CMainApp*	pInstance = new CMainApp();

	if (FAILED(pInstance->Ready_MainApp()))
	{
		MessageBox(0, L"CMainApp Created Failed", L"System Error", MB_OK);

		if (nullptr != pInstance)
		{
			delete pInstance;
			pInstance = nullptr;
		}
	}

	return pInstance;
}

void CMainApp::Free()
{
	bLobbyServer = false;
	bGameServer = false;

	Safe_Release(m_pRenderer);
	Safe_Release(m_pManagement);
	Safe_Release(m_pGraphic_Device);

	GET_INSTANCE(CUIManager)->DestroyInstance();
	KEYMGR->DestroyInstance();
	CEffectManager::GetInstance()->DestroyInstance();
	CLoadManager::GetInstance()->DestroyInstance();
	GET_INSTANCE(CFontManager)->DestroyInstance();

	CManagement::Release_Engine();	
	GET_INSTANCE(CSoundManager)->DestroyInstance();
	CServerManager::GetInstance()->DestroyInstance();
}

