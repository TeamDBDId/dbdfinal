﻿#include "stdafx.h"
#include "Management.h"
#include "UI_Texture.h"
#include "UI_OverlayMenu.h"
#include <string>
#include "Math_Manager.h"

_USING(Client);


CUI_OverlayMenu::CUI_OverlayMenu(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_OverlayMenu::CUI_OverlayMenu(const CUI_OverlayMenu & rhs)
	: CGameObject(rhs)
	, m_iMaxPlayer(rhs.m_iMaxPlayer)
	, m_vecCamperPerk(rhs.m_vecCamperPerk)
	, m_vecSlasherPerk(rhs.m_vecSlasherPerk)
	, m_vecEquippedPerk(rhs.m_vecEquippedPerk)
	, m_CurCamper(rhs.m_CurCamper)
	, m_CurSlasher(rhs.m_CurSlasher)
	, m_IsClone(true)
{
}


HRESULT CUI_OverlayMenu::Ready_Prototype()
{
	m_CurCamper = new _int;
	*m_CurCamper = 0;
	m_CurSlasher = new _int;
	*m_CurSlasher = 0;
	m_vecEquippedPerk = new vector<_int>;
	m_vecEquippedPerk->reserve(8);
	m_IsClone = false;
	for (_int i = 0; i<8; i++)
		m_vecEquippedPerk->push_back(-1);
	SetUp_PerkInfo();
	return NOERROR;
}

HRESULT CUI_OverlayMenu::Ready_GameObject()
{
	_uint				m_CurButton = NONE;
	_uint				m_OldButton = NONE;
	Set_Background();
	Set_SideButton();
	Set_OtherButton();
	Set_StartIcon();
	m_WaitingTime = 0;
	return NOERROR;
}

_int CUI_OverlayMenu::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	Server_Check();
	Update_Job();
	Ready_Check();
	Mouse_Check();
	Lobby_Check();
	return _int();
}

_int CUI_OverlayMenu::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CUI_OverlayMenu::Set_Background()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;
	_vec2 vPos = { 0.0523451f,0.459679f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"PanelScreen.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ L"PanelScreen_0", pTexture });

	vPos = { 0.0739831f,0.493054f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Lobby2_Background_1.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"PanelScreen_1", pTexture });
	
	vPos = { 0.5, 0.970944f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Panel_Bottom.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"PanelScreen_2", pTexture });

	vPos = { 0.108739f, 0.276681f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"VerticalLine.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.75f, 0.78f));
	m_UITexture.insert({ L"VerticalLine", pTexture });

	vPos = { 0.07372f, 0.093f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"CurTap.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.78f));
	pTexture->IsRendering(false);
	m_UITexture.insert({ L"CurTap", pTexture });
	
	Safe_Release(pManagement);
}

void CUI_OverlayMenu::Set_CurPerk()
{
	vector<PERKINFO> vecPerk;
	vector<_int> vecEquippedPerk = *m_vecEquippedPerk;
	_int SlasherIndex = 0;
	D3DXCOLOR		BaseColor;
	CUI_Texture* pTexture = nullptr;
	if (m_JobState)
	{
		vecPerk = m_vecSlasherPerk;
		SlasherIndex = 4;
		BaseColor = { 1.f, 0.2f,0.2f,1.f };
	}
	else
	{
		vecPerk = m_vecCamperPerk;
		BaseColor = { 0.0588235f, 0.3137254f, 0.0862745f,1.f };
	}
	for (int i = 0; i < 4; i++)
	{
		if (vecEquippedPerk[i +SlasherIndex] == -1)
			continue;
		for (_uint j = 0; j< vecPerk.size(); j++)
		{
			if (vecEquippedPerk[i+SlasherIndex] == vecPerk[j].TagNum)
			{
				GET_INSTANCE_MANAGEMENT;
				pTexture = Find_UITexture(wstring(L"MainPerk_") + to_wstring(i));
				_vec2 vPos = pTexture->Get_Pos();
				pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
				pTexture->Set_TexName(L"Perk_Base2.tga");
				pTexture->Set_Scale(_vec2(0.35f, 0.35f));
				pTexture->Set_Color(BaseColor);
				pTexture->IsColor(true);
				pTexture->Set_Pos(vPos);
				m_UITexture.insert({ wstring(L"MainPerk_Base2_") + to_wstring(i),pTexture });
				
				pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
				pTexture->Set_TexName(vecPerk[j].TagTexture);
				pTexture->Set_Scale(_vec2(0.4f, 0.4f));
				pTexture->Set_Pos(vPos);
				m_UITexture.insert({ L"MainPerk_Perk_" + to_wstring(i),pTexture });

				pTexture = Find_UITexture(wstring(L"InvenPerk_Perk_") + to_wstring(j));
				vPos = pTexture->Get_Pos();
				pTexture = Find_UITexture(wstring(L"InvenPerk_Used_") + to_wstring(i));
				if (pTexture == nullptr)
				{
					pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
					pTexture->Set_TexName(L"Perk_Used.tga");
					pTexture->Set_Scale(_vec2(0.5f, 0.5f));
				}
				pTexture->Set_Pos(vPos);
				m_UITexture.insert({ wstring(L"InvenPerk_Used_") + to_wstring(i), pTexture });

				Safe_Release(pManagement);
			}
		}
	}
}

void CUI_OverlayMenu::Update_Job()
{
	if (m_OldJobState != m_JobState)
	{
		if (m_JobState == false)
			Set_Camper();
		else
			Set_Slasher();

		m_OldJobState = m_JobState;
	}
}

_bool CUI_OverlayMenu::Set_Camper()
{
	_bool ChangeCamper = false;
	_int  CamperCheck = 0;
	_bool Number[4] = { false,false,false,false };
	GET_INSTANCE(CSoundManager)->ChannelVolume(string("Lobby_Burning"), 1.f);
	if (!lserver_data.GameStart)
	{
		for (int i = 0; i < 5; ++i)
		{
			if (lserver_data.playerdat[i].bConnect
				&&lserver_data.Number[i] < 5
				&& lserver_data.Number[i] >= 1)
			{
				CamperCheck++;
				Number[lserver_data.Number[i] - 1] = true;
			}
		}
		if (CamperCheck < 4)
		{
			for (int i = 0; i < 4; ++i)
			{
				if (!Number[i])
				{
					exPlayerNumber = i + 1;
					lobby_data.Packet = i + 2;
					return true;

					break;
				}
			}
		}
		else
			return false;
	}
	else
		return false;
	return false;
}

_bool CUI_OverlayMenu::Set_Slasher()
{
	_bool bSlasher = false;
	_bool ChangeSlasher = false;
	GET_INSTANCE(CSoundManager)->ChannelVolume(string("Lobby_Burning"), 0.3f);

	if (!lserver_data.GameStart)
	{
		for (int i = 0; i < 5; ++i)
		{
			if (5 == lserver_data.playerdat[i].Number)
			{
				bSlasher = true;
			}
				
		}

		if (!bSlasher)
		{
			lobby_data.Packet = 1;
			exPlayerNumber = 5;
			ChangeSlasher = true;
		}
	}

	return ChangeSlasher;
}

void CUI_OverlayMenu::Button_L2_Change_Job(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CJ_Panel"));
		pTexture->Set_Alpha(1.f);
		_vec2 vPos = pTexture->Get_Pos();
		_vec2 vScale = pTexture->Get_Scale();
		pTexture = Find_UITexture(wstring(L"CJ_Icon"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"CJ_Change_Icon"));
		pTexture->Set_Alpha(1.f);

		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Lobby_Panel_Frame.tga"));
		pTexture->Set_Pos(vPos);
		pTexture->Set_Scale(vScale*0.9f, false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"CJ_Frame", pTexture });

		wstring Tex = L"";
		if (m_JobState)
			Tex = L"생존자로 변경";
		else
			Tex = L"살인마로 변경";

		vPos.x += Tex.length()*15.f + g_iBackCX* 0.01f;

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(L"Black.tga");
		pTexture->Set_Scale(_vec2((_float)Tex.length() * g_iBackCX* 0.015f, 0.04f*g_iBackCY), false);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CJ_TexBase", pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_Font(Tex, _vec2(15.f, 17.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 0);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CJ_Info", pTexture });
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"), _vec3(), 0, 1.f);
		Safe_Release(pManagement);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CJ_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CJ_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CJ_Change_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CJ_Frame"));
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"CJ_Frame"));
		pTexture = Find_UITexture(wstring(L"CJ_TexBase"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CJ_TexBase"));
		pTexture = Find_UITexture(wstring(L"CJ_Info"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CJ_Info"));
	}
	else if (State == Click)
	{
		Delete_Perk();
		m_JobState = !m_JobState;
		if (m_JobState)
		{
			m_iClickButton = Change_To_Slasher;
			lobby_data.character = *m_CurSlasher;
		}
		else
		{
			m_iClickButton = Change_To_Camper;
			lobby_data.character = *m_CurCamper;
		}
		Button_L2_Change_Job(UnTouch);
		m_CurButton = NONE;
		m_OldButton = NONE;
		
		if (m_CurUI == CHANGE_PERK)
		{
			SetUp_Inventory();
			Set_CurPerk();
			Compute_CurPerk();
		}
		else if (m_CurUI == CHANGE_CHARICTER)
		{
			Delete_CharictorPage();
			SetUp_Change_Charicter();
			Set_Edge();
		}
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/ui_select_press_01.ogg"), _vec3(), 0, 1.f);
	}

}

void CUI_OverlayMenu::Button_L2_Charictor(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(L"Char_Base2_" + to_wstring(m_CurButtonIndex));
		_vec2 vPos = pTexture->Get_Pos();
		_vec2 vScale = pTexture->Get_Scale();

		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Lobby_Panel_Frame.tga"));
		pTexture->Set_Pos(vPos);
		pTexture->Set_Scale(vScale, false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"Char_Selected", pTexture });
		Safe_Release(pManagement);

		wstring MainText;
		wstring SubText;

		if (m_JobState)
		{
			if (m_CurButtonIndex == 0)
			{
				MainText = L"레이스";
				SubText = L"일정 시간을 들여 거의 보이지 않는 은신상태가 되거나, 은신을 해제할 수 있다. 은신상태일떄 이동속도가 빨라지고 사물과 상호작용을 할 수 있으나 공격 할 수 없다.";
			}
			else
			{
				MainText = L"스피릿";
				SubText = L"린은 생존자를 감시하다가 그녀의 능력을 이용해 순간적으로 접근하거나, 예상할 수 없는 위치로 순간이동해 생존자를 방해할 수 있다.";
			}
		}
		else
		{
			if (m_CurButtonIndex == 0)
			{
				MainText = L"드와이트 페어필드";
				SubText = L"불안정한 지도자";
			}
			else if (m_CurButtonIndex == 1)
			{
				MainText = L"윌리엄 빌 오버백";
				SubText = L"노련한 군인";
			}
			else if (m_CurButtonIndex == 2)
			{
				MainText = L"니아 칼슨";
				SubText = L"도시의 미술가";
			}
			else
			{
				MainText = L"메그 토마스";
				SubText = L"활동적인 운동선수";
			}
		}
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"), _vec3(), 0, 1.f);
		Add_Explain(MainText, SubText, _vec2(vPos.x + 0.15f * g_iBackCX, vPos.y + 0.08f * g_iBackCY));
	}
	else if(State == UnTouch)
	{ 
		Delete_Explain();
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"Char_Selected"));
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"Char_Selected"));
	}
	else if (State == Click)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/ui_unequip_01.ogg"), _vec3(), 0, 1.f);
		if (m_JobState)
			*m_CurSlasher = m_CurButtonIndex;
		else
			*m_CurCamper = m_CurButtonIndex;

		lobby_data.character = m_CurButtonIndex;
		Set_Edge();
	}
}

void CUI_OverlayMenu::Button_L2_Change_Charicter(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CC_Panel"));
		pTexture->Set_Alpha(1.f);
		_vec2 vPos = pTexture->Get_Pos();
		_vec2 vScale = pTexture->Get_Scale();
		pTexture = Find_UITexture(wstring(L"CC_Icon"));
		pTexture->Set_Alpha(1.f);

		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Lobby_Panel_Frame.tga"));
		pTexture->Set_Pos(vPos);
		pTexture->Set_Scale(vScale*0.9f, false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"CC_Frame", pTexture });

		wstring Tex = L"";
		if (m_JobState)
			Tex = L"살인마 선택";
		else
			Tex = L"생존자 선택";

		vPos.x += Tex.length()*15.f + g_iBackCX* 0.01f;

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(L"Black.tga");
		pTexture->Set_Scale(_vec2((_float)Tex.length() * g_iBackCX* 0.015f, 0.04f*g_iBackCY), false);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CC_TexBase", pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_Font(Tex, _vec2(15.f, 17.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 0);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CC_Info", pTexture });
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"), _vec3(), 0, 1.f);
		Safe_Release(pManagement);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CC_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CC_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CC_Frame"));
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"CC_Frame"));
		pTexture = Find_UITexture(wstring(L"CC_TexBase"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CC_TexBase"));
		pTexture = Find_UITexture(wstring(L"CC_Info"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CC_Info"));
	}
	else if (State == Click)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CurTap"));

		if (m_CurUI == CHANGE_CHARICTER)
		{
			pTexture->IsRendering(false);
			Delete_CharictorPage();
			m_CurUI = NONE;
		}
		else if (m_CurUI == CHANGE_PERK)
		{
			Delete_Inventory();
			Delete_Perk();
			pTexture->Set_Pos(_vec2(0.07372f*g_iBackCX, 0.098f*g_iBackCY));
			SetUp_Change_Charicter();
			Set_Edge();
			m_CurUI = CHANGE_CHARICTER;
		}
		else if (m_CurUI == NONE)
		{
			pTexture->Set_Pos(_vec2(0.07372f*g_iBackCX, 0.098f*g_iBackCY));
			pTexture->IsRendering(true);
			
			SetUp_Change_Charicter();
			Set_Edge();
			m_CurUI = CHANGE_CHARICTER;
		}
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/ui_select_press_01.ogg"), _vec3(), 0, 1.f);
	}
}

void CUI_OverlayMenu::Button_L2_Change_Perk(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CP_Panel"));
		pTexture->Set_Alpha(1.f);
		_vec2 vPos = pTexture->Get_Pos();
		_vec2 vScale = pTexture->Get_Scale();
		pTexture = Find_UITexture(wstring(L"CP_Icon"));
		pTexture->Set_Alpha(1.f);

		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Lobby_Panel_Frame.tga"));
		pTexture->Set_Pos(vPos);
		pTexture->Set_Scale(vScale*0.9f, false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"CP_Frame", pTexture });

		wstring Tex = L"기술 선택";
		vPos.x += Tex.length()*15.f + g_iBackCX* 0.015f;

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(L"Black.tga");
		pTexture->Set_Scale(_vec2((_float)Tex.length() * g_iBackCX* 0.015f, 0.04f*g_iBackCY), false);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CP_TexBase", pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_Font(Tex, _vec2(15.f, 17.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 0);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CP_Info", pTexture });
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"), _vec3(), 0, 1.f);
		Safe_Release(pManagement);
	}
	else if (State == UnTouch)
	{
		Delete_Explain();
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CP_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CP_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CP_Frame"));
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"CP_Frame"));
		pTexture = Find_UITexture(wstring(L"CP_TexBase"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CP_TexBase"));
		pTexture = Find_UITexture(wstring(L"CP_Info"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CP_Info"));
	}
	else if (State == Click)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CurTap"));

		if (m_CurUI == CHANGE_CHARICTER)
		{
			Delete_CharictorPage();
			pTexture->Set_Pos(_vec2(0.07372f*g_iBackCX, 0.245f*g_iBackCY));
			SetUp_Perk();
			SetUp_Inventory();
			Set_CurPerk();
			Compute_CurPerk();
			m_CurUI = CHANGE_PERK;
		}
		else if (m_CurUI == CHANGE_PERK)
		{
			Delete_Inventory();
			Delete_Perk();

			pTexture->IsRendering(false);
			m_CurUI = NONE;
		}
		else if (m_CurUI == NONE)
		{
			pTexture->Set_Pos(_vec2(0.07372f*g_iBackCX, 0.245f*g_iBackCY));
			pTexture->IsRendering(true);
			SetUp_Perk();
			SetUp_Inventory();
			Set_CurPerk();
			Compute_CurPerk();
			m_CurUI = CHANGE_PERK;
		}
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/ui_select_press_01.ogg"), _vec3(), 0, 1.f);
	}
}

void CUI_OverlayMenu::Button_L2_Main_Perk(ButtonState State)
{
	if (State == Touch)
	{
		_int SlasherIndex = 0;
		vector<PERKINFO> CurPerk = m_vecCamperPerk;
		vector<_int> EqippedPerk = *m_vecEquippedPerk;
		if (m_JobState)
		{
			CurPerk = m_vecSlasherPerk;
			SlasherIndex = 4;
		}
		if (EqippedPerk[m_CurButtonIndex + SlasherIndex] == -1)
			return;

		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(L"MainPerk_" + to_wstring(m_CurButtonIndex));
		_vec2 vPos = pTexture->Get_Pos();
		_vec2 vScale = pTexture->Get_Scale();

		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Perk_Selected.tga"));
		pTexture->Set_Pos(vPos);
		pTexture->Set_Scale(vScale*1.1f, false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"MainPerk_Selected", pTexture });

		Safe_Release(pManagement);
		
		for (auto& iter : CurPerk)
		{
			if (iter.TagNum == EqippedPerk[m_CurButtonIndex + SlasherIndex])
			{
				Add_Explain(iter.MainText, iter.SubText, _vec2(vPos.x + 0.15f * g_iBackCX, vPos.y + 0.08f * g_iBackCY));
				GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"), _vec3(), 0, 1.f);
				break;
			}
		}
	}
	else if (State == UnTouch)
	{
		Delete_Explain();
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"MainPerk_Selected"));
		if (pTexture == nullptr)
			return;
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"MainPerk_Selected"));
	}
	else if (State == Click)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"Perk_Used"));
		wstring Tag = L"MainPerk_";
		pTexture->Set_Pos(Find_UITexture(Tag + to_wstring(m_CurButtonIndex))->Get_Pos());
		m_CurPerk = m_CurButtonIndex;
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/ui_select_press_01.ogg"), _vec3(), 0, 1.f);
	}
}

void CUI_OverlayMenu::Button_L2_Inven_Perk(ButtonState State)
{
	if (State == Touch)
	{
		vector<PERKINFO> CurPerk = m_vecCamperPerk;
		if (m_JobState)
			CurPerk = m_vecSlasherPerk;
		if (CurPerk.size() <= m_CurButtonIndex)
			return;

		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(L"InvenPerk_" + to_wstring(m_CurButtonIndex));
		_vec2 vPos = pTexture->Get_Pos();
		_vec2 vScale = pTexture->Get_Scale();

		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Perk_Selected.tga"));
		pTexture->Set_Pos(vPos);
		pTexture->Set_Scale(vScale*1.1f, false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"InvenPerk_Selected" + to_wstring(m_CurButtonIndex), pTexture });
		Safe_Release(pManagement);
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"), _vec3(), 0, 1.f);
		Add_Explain(CurPerk[m_CurButtonIndex].MainText, CurPerk[m_CurButtonIndex].SubText, _vec2(vPos.x + 0.15f * g_iBackCX, vPos.y + 0.08f * g_iBackCY));
	}
	else if (State == UnTouch)
	{
		Delete_Explain();
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"InvenPerk_Selected") + to_wstring(m_OldButtonIndex));
		if (pTexture == nullptr)
			return;
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"InvenPerk_Selected") + to_wstring(m_OldButtonIndex));
	}
	else if (State == Click)
	{
		vector<_int> vecEquippedPerk = *m_vecEquippedPerk;
		D3DXCOLOR BaseColor = { 0.0588235f, 0.3137254f, 0.0862745f,1.f };
		vector<PERKINFO> vecPerk = m_vecCamperPerk;

		_int SlasherIndex = 0;
		if (m_JobState)
		{
			vecPerk = m_vecSlasherPerk;
			SlasherIndex = 4;
			BaseColor = { 1.f, 0.2f, 0.2f, 1.f };
		}
		if (vecPerk.size() <= m_CurButtonIndex)
			return;
		for (_int i = 0; i < 4; i++)
		{
			if (vecEquippedPerk[i+SlasherIndex] == vecPerk[m_CurButtonIndex].TagNum)
			{
				Delete_UI_Texture(wstring(L"MainPerk_Perk_") + to_wstring(i), true);
				Delete_UI_Texture(wstring(L"MainPerk_Base2_") + to_wstring(i), true);
				Delete_UI_Texture(wstring(L"InvenPerk_Used_") + to_wstring(i), true);
				vecEquippedPerk[i+SlasherIndex] = -1;
				*m_vecEquippedPerk = vecEquippedPerk;
				Compute_CurPerk();
				GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/ui_unequip_01.ogg"), _vec3(), 0, 1.f);
				return;
			}
		}
		if (vecEquippedPerk[m_CurPerk + SlasherIndex] == -1)
		{
			CUI_Texture* pTexture = nullptr;
			_vec2 vPos = Find_UITexture(wstring(L"MainPerk_") + to_wstring(m_CurPerk))->Get_Pos();
			GET_INSTANCE_MANAGEMENT;
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(L"Perk_Base2.tga");
			pTexture->Set_Scale(_vec2(0.35f, 0.35f));
			pTexture->Set_Color(BaseColor);
			pTexture->IsColor(true);
			pTexture->Set_Pos(vPos);
			m_UITexture.insert({ wstring(L"MainPerk_Base2_") + to_wstring(m_CurPerk), pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(vecPerk[m_CurButtonIndex].TagTexture);
			pTexture->Set_Scale(_vec2(0.4f, 0.4f));
			pTexture->Set_Pos(vPos);
			m_UITexture.insert({ wstring(L"MainPerk_Perk_") + to_wstring(m_CurPerk), pTexture });

			vPos = Find_UITexture(wstring(L"InvenPerk_") + to_wstring(m_CurButtonIndex))->Get_Pos();
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(L"Perk_Used.tga");
			pTexture->Set_Scale(_vec2(0.5f, 0.5f));
			pTexture->Set_Pos(vPos);
			m_UITexture.insert({ wstring(L"InvenPerk_Used_") + to_wstring(m_CurPerk), pTexture });
			GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/ui_equip_01.ogg"), _vec3(), 0, 1.f);
			Safe_Release(pManagement);

			vecEquippedPerk[m_CurPerk+ SlasherIndex] = vecPerk[m_CurButtonIndex].TagNum;
		}
		else
		{
			CUI_Texture* pTexture = nullptr;
			pTexture = Find_UITexture(wstring(L"MainPerk_Perk_") + to_wstring(m_CurPerk));
			pTexture->Set_TexName(vecPerk[m_CurButtonIndex].TagTexture);
			
			pTexture = Find_UITexture(wstring(L"InvenPerk_Used_") + to_wstring(m_CurPerk));
			pTexture->Set_Pos(Find_UITexture(wstring(L"InvenPerk_") + to_wstring(m_CurButtonIndex))->Get_Pos());
			m_UITexture.insert({ wstring(L"InvenPerk_Used_") + to_wstring(m_CurPerk), pTexture });
			GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/ui_equip_01.ogg"), _vec3(), 0, 1.f);
			vecEquippedPerk[m_CurPerk + SlasherIndex] = vecPerk[m_CurButtonIndex].TagNum;
		}
		*m_vecEquippedPerk = vecEquippedPerk;
		Compute_CurPerk();
	}
}

void CUI_OverlayMenu::Button_L2_Go_Back(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"GB_Panel"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"GB_Icon"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"GB_Text"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"GB_Arrow"));
		pTexture->Set_Alpha(1.f);
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"), _vec3(), 0, 1.f);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"GB_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"GB_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"GB_Text"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"GB_Arrow"));
		pTexture->Set_Alpha(0.5f);
	}
	else if (State == Click)
	{
		lobby_data.Packet = CLICKBACK;
		m_iClickButton = Go_Back;
		Delete_All();
	}
}

void CUI_OverlayMenu::Button_L2_Ready(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"R_Panel"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"R_Icon"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"R_Text"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"R_Arrow"));
		pTexture->Set_Alpha(1.f);
		GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/theentity_short_01.ogg"), _vec3(), 0, 1.f);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"R_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"R_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"R_Text"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"R_Arrow"));
		pTexture->Set_Alpha(0.5f);
	}
	else if (State == Click)
	{
		m_IsReady = !m_IsReady;
		_int MyNumber = lserver_data.Number[client_data.client_imei];

		if (MyNumber > 0 && MyNumber <= 5)
			lobby_data.Ready = m_IsReady;
	}
}

void CUI_OverlayMenu::Add_Explain(wstring MainText, wstring SubText, _vec2 vPosition)
{
	Delete_Explain();
	GET_INSTANCE_MANAGEMENT;
	CUI_Texture* pTexture = nullptr;
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture",SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(L"Explain_Base.tga");
	pTexture->Set_Scale(_vec2(0.4f,0.4f));
	pTexture->Set_Pos(vPosition);
	m_UITexture.insert({ L"Explain_Base", pTexture });

	_vec2 vStartPos = { 0.115f * g_iBackCX, 0.08f * g_iBackCY };
	_vec2 vPos = vPosition - vStartPos;
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_Font(MainText,_vec2(17.f,22.f),D3DXCOLOR(1.f,1.f,1.f,1.f), 1);
	vPos = _vec2(vPos.x + pTexture->Get_Scale().x*0.5f, vPos.y);
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Explain_Main", pTexture });

	_int Cycle = SubText.length() / 22;
	
	for (_int i = 0; i < Cycle+1; i++)
	{
		vStartPos = { 0.115f * g_iBackCX, 0.03f* g_iBackCY - 17.f*i };
		vPos = vPosition - vStartPos;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_Font(SubText.substr(i*22, 22), _vec2(13.f, 17.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		vPos = _vec2(vPos.x + pTexture->Get_Scale().x*0.5f, vPos.y);
		pTexture->Set_Pos(vPos);
		pTexture->Set_Color(D3DXCOLOR(0.7f, 0.7f, 0.7f, 1.f));
		pTexture->IsColor(true);
		m_UITexture.insert({ wstring(L"Explain_Sub_")+to_wstring(i), pTexture });
	}
	Safe_Release(pManagement);
}

CUI_OverlayMenu * CUI_OverlayMenu::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_OverlayMenu*	pInstance = new CUI_OverlayMenu(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_OverlayMenu Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_OverlayMenu::Clone_GameObject()
{
	CUI_OverlayMenu*	pInstance = new CUI_OverlayMenu(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_OverlayMenu Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


void CUI_OverlayMenu::Set_Edge()
{
	CUI_Texture* pTexture = nullptr;
	pTexture = Find_UITexture(wstring(L"Char_Edge_") + to_wstring(0));
	if (pTexture == nullptr)
	{
		GET_INSTANCE_MANAGEMENT;
		for (_int i = 0; i < 4; i++)
		{
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"Charictor_Selected.tga"));
			pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
			pTexture->Set_Rotation(D3DXToRadian(i * 90));
			m_UITexture.insert({ wstring(L"Char_Edge_") + to_wstring(i),pTexture });
		}
		Safe_Release(pManagement);
	}
	_vec2 vPos = Find_UITexture(wstring(L"Char_Base_") + to_wstring(lobby_data.character))->Get_Pos();

	pTexture = Find_UITexture(wstring(L"Char_Edge_") + to_wstring(0));
	pTexture->Set_Pos(_vec2(vPos.x - 0.044f*g_iBackCX,vPos.y - 0.125f*g_iBackCY));
	pTexture = Find_UITexture(wstring(L"Char_Edge_") + to_wstring(1));
	pTexture->Set_Pos(_vec2(vPos.x + 0.0435f*g_iBackCX, vPos.y - 0.125f*g_iBackCY));
	pTexture = Find_UITexture(wstring(L"Char_Edge_") + to_wstring(2));
	pTexture->Set_Pos(_vec2(vPos.x + 0.0435f*g_iBackCX, vPos.y + 0.125f*g_iBackCY));
	pTexture = Find_UITexture(wstring(L"Char_Edge_") + to_wstring(3));
	pTexture->Set_Pos(_vec2(vPos.x - 0.044f*g_iBackCX, vPos.y + 0.125f*g_iBackCY));
}

void CUI_OverlayMenu::SetUp_Inventory()
{
	Delete_Perk();
	vector<PERKINFO> vecPerk;
	D3DXCOLOR		BaseColor;
	if (m_JobState)
	{
		vecPerk = m_vecSlasherPerk;
		BaseColor = { 1.f, 0.2f,0.2f,1.f };
	}
	else
	{
		vecPerk = m_vecCamperPerk;
		BaseColor = { 0.0588235f, 0.3137254f, 0.0862745f,1.f };
	}
	GET_INSTANCE_MANAGEMENT
	CUI_Texture* pTexture = nullptr;
	wstring Tag = L"InvenPerk_";
	wstring TagBase = L"InvenPerk_Base2_";
	wstring TagPerk = L"InvenPerk_Perk_";
	for (_uint i = 0; i < vecPerk.size(); i++)
	{
		pTexture = Find_UITexture(Tag + to_wstring(i));
		_vec2 vPos = pTexture->Get_Pos();
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(L"Perk_Base2.tga");
		pTexture->Set_Scale(_vec2(0.35f, 0.35f));
		pTexture->Set_Color(BaseColor);
		pTexture->IsColor(true);
		pTexture->Set_Pos(vPos);
		m_UITexture.insert({ TagBase + to_wstring(i),pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(vecPerk[i].TagTexture);
		pTexture->Set_Scale(_vec2(0.4f, 0.4f));
		pTexture->Set_Pos(vPos);
		m_UITexture.insert({ TagPerk + to_wstring(i),pTexture });
	}
	Safe_Release(pManagement);
}

void CUI_OverlayMenu::SetUp_PerkInfo()
{
	m_vecCamperPerk.push_back({ DEADHARD, wstring(L"Perk_DeadHard.png"), wstring(L"죽기살기"), wstring(L"40초마다 한 번씩, 부상당한 상태일 때, 달리는 중에 능력 발동 키를 누르면 앞으로 돌진한다. 이 동안엔 무적 상태가 된다. 쿨타임 40초") });
	m_vecCamperPerk.push_back({ PREMONITION, wstring(L"Perk_Premonition.png"), wstring(L"불길한 예감"), wstring(L"30초마다 한 번씩, 전방 45도, 36미터 살인자 존재시 감지한다.") });
	m_vecCamperPerk.push_back({ DARKSENSE, wstring(L"Perk_Darksense.png"), wstring(L"어두운 감각"), wstring(L"발전기 수리를 완료할 때마다, 살인마의 오라가 5초간 보여진다. 마지막 발전기가 수리되었을 때, 살인마의 오라가 10초간 보여진다.") });
	m_vecCamperPerk.push_back({ CRUCIALBLOW, wstring(L"Perk_Crucialblow.png"), wstring(L"결정적인 일격"), wstring(L"게임 중 단 한번 위글을 5번 이상 했을 시 스킬체크가 뜨면서 성공 시 살인자를 기절시킬 수 있다.") });
	m_vecCamperPerk.push_back({ SELFHEAL, wstring(L"Perk_SelfHeal.png"), wstring(L"자가 치료"), wstring(L"구급 상자 없이 50%의 속도로 자신을 치료할 수 있는 능력을 얻는다.") });
	m_vecCamperPerk.push_back({ PALOMA, wstring(L"Perk_Paloma.png"),wstring(L"정면충돌"),wstring(L"3초 동안 캐비닛에 있었을 경우, 정면충돌이 발동한다. 정면충돌 발동시, 캐비닛에서 나올때 돌진하여 살인마가 사정거리 내에 서있었을 경우 3초 동안 기절시킨다. 쿨타임 40초") });
	m_vecCamperPerk.push_back({ BORROWEDTIME, wstring(L"Perk_Borrowedtime.png"), wstring(L"빌려온 시간"), wstring(L"살인마의 공포 범위 안에서 생존자를 구출한 후 15초간, 그 어떠한 공격이든 구조자를 빈사 상태로 만드는 대신 20초의 출혈 타이머를 발생시키게 된다. 구조자가 출혈 타이머 도중 공격을 받거나 출혈 타이머가 끝나면 빈사 상태가 된다.") });
	m_vecCamperPerk.push_back({ ADRENALINE, wstring(L"Perk_Adrenaline.png"),wstring(L"아드레날린"), wstring(L"출구에 전원이 공급되었을 때 즉시 한 단계 회복되며, 150%의 속도로 5초간 질주한다.") });
	m_vecCamperPerk.push_back({ BREAKING, wstring(L"Perk_Breaking.png"),wstring(L"부수기"), wstring(L"탈출이나 구출을 통해 갈고리에서 빠져나가면 갈고리가 부러진다. 동시에, 살인마의 오라를 6초 동안 볼 수 있다.") });
	m_vecCamperPerk.push_back({ SYMPATHY, wstring(L"Perk_Sympathy.png"),wstring(L"공감"), wstring(L"128미터 내에 있는 부상 혹은 빈사 상태의 동료 생존자의 오라를 보여준다.")});
	m_vecCamperPerk.push_back({ BOND, wstring(L"Perk_Bond.png"),wstring(L"유대감"), wstring(L"36미터 내에 있는 동료 생존자의 오라를 보여준다.") });
	m_vecCamperPerk.push_back({ HOMOGENEITY, wstring(L"Perk_Homogeneity.png"), wstring(L"동질감"), wstring(L"갈고리에 걸린 동안, 모든 생존자의 오라가 다른 모든 생존자들에게 보인다. 16m의 범위 내에 살인마가 있다면 살인마의 오라가 다른 모든 생존자들에게 보인다.") });
	m_vecCamperPerk.push_back({ CALMSOUL, wstring(L"Perk_Calmsoul.png"),wstring(L"차분한 영혼"),wstring(L"주변의 까마귀가 날아갈 확률이 100% 감소한다.") });

	m_vecSlasherPerk.push_back({ SWALLOWEDHOPE, wstring(L"Perk_Swallowedhope.png"),wstring(L"주술 : 삼켜지는 희망"),wstring(L"출구에 전원이 공급될 때, 갈고리에 한 번 걸렸던 생존자를 살해 할 수 있다, 주술의 효과는 토템이 활성화되어 있는 동안에만 지속된다.") });
	m_vecSlasherPerk.push_back({ RUIN, wstring(L"Perk_Ruin.png"),wstring(L"주술 : 파멸"), wstring(L"스킬 체크 성공 시, 발전기 수리 진행도가 5 % 감소한다, 주술의 효과는 주술 토템이 활성화되어 있는 동안에만 지속된다.") });
	m_vecSlasherPerk.push_back({ RESENTMENT, wstring(L"Perk_Resentment.png"),wstring(L"원한"), wstring(L" 한 개의 발전기가 수리완료될 때 마다 당신은 3초동안 모든 생존자의 위치를 볼 수 있다.") });
	m_vecSlasherPerk.push_back({ BBQANDCHILLY, wstring(L"Perk_Bbqandchilly.png"), wstring(L"바비큐 & 칠리"), wstring(L"갈고리에 걸었을 때 40미터 외에 있는 생존자들의 오라가 4초동안 보여진다.") });
	m_vecSlasherPerk.push_back({ DISSONANCE, wstring(L"Perk_Dissonance.png"), wstring(L"불협화음"),wstring(L"언제든 생존자 2명 이상이 같은 발전기를 동시에 작업하고 있으면 발전기 오라가 12초 동안 노랗게 표시된다.") });
	m_vecSlasherPerk.push_back({ MURMUR, wstring(L"Perk_Murmur.png"), wstring(L"격렬한 중얼거림"),wstring(L"발전기가 수리될 때마다, 수리된 발전기 16미터 이내에 있는 생존자들의 오라를 5초간 보여준다. 마지막 발전기가 수리되면, 모든 생존자의 오라를 10초간 보여준다.") });
	m_vecSlasherPerk.push_back({ DEERSTALKER, wstring(L"Perk_Deerstalker.png"), wstring(L"사슴 사냥꾼"),wstring(L"36미터 이내에 있는 빈사 상태 생존자의 오라를 보여준다.") });
	m_vecSlasherPerk.push_back({ INCANTATION, wstring(L"Perk_Incantation.png"), wstring(L"주술: 피할 수 없는 죽음"),wstring(L"출구에 전원이 공급될 때, 이 주술이 발동되있는 동안 공격이 생존자를 빈사 상태로 만든다. 해당 주술 토템이 남아있는 한 주술의 효과는 사라지지 않는다.") });
	m_vecSlasherPerk.push_back({ NURSECALLING, wstring(L"Perk_Nursecalling.png"), wstring(L"간호사의 소명"),wstring(L"오라를 감지하는 잠재력을 끌어낸다. 28미터 내에 존재하는 치료하거나 치료받는 생존자의 오라를 볼 수 있다.") });
}

void CUI_OverlayMenu::SetUp_Change_Charicter()
{
	GET_INSTANCE_MANAGEMENT;
	CUI_Texture* pTexture = nullptr;
	_vec2 vPos = { 0.4f*g_iBackCX, 0.538608f*g_iBackCY };

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Button_Background.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Char_Button_Background",pTexture });
	
	vPos = { 0.175656f * g_iBackCX, 0.107541f *g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	if (m_JobState)
		pTexture->Set_Font(wstring(L"살인마"), _vec2(28.f, 34.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 2);
	else
		pTexture->Set_Font(wstring(L"생존자"), _vec2(28.f, 34.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 2);
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Char_Font",pTexture });
	//0.278707,0.379066  0.416426 0.379066  0.416426 0.715912  

	vPos = { 0.278707f * g_iBackCX, 0.379066f * g_iBackCY };
	wstring Texture;
	if (m_JobState)
	{
		vPos.y += (0.168423f*g_iBackCY);
		for (_int i = 0; i < 2; i++)
		{
			if (i == 0)
				Texture = L"Killer_Wraith.png";
			else
				Texture = L"Killer_Spirit.png";

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"Charictor_Base.tga"));
			pTexture->Set_Scale(_vec2(0.55f, 0.666667f));
			pTexture->Set_Pos(vPos);
			pTexture->Set_Color(D3DXCOLOR(0.3764f, 0.31372f, 0.22745f, 1.f));
			pTexture->IsColor(true);
			m_UITexture.insert({ wstring(L"Char_Base_") + to_wstring(i),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"Charictor_Base2.tga"));
			pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
			pTexture->Set_Pos(vPos);
			m_UITexture.insert({ wstring(L"Char_Base2_") + to_wstring(i),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(Texture);
			pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
			pTexture->Set_Pos(vPos);
			m_UITexture.insert({ wstring(L"Char_") + to_wstring(i),pTexture });

			vPos.x += (0.14f*g_iBackCX);
		}
	}
	else
	{
		vPos = { 0.278707f * g_iBackCX, 0.379066f * g_iBackCY };
		for (_int i = 0; i < 4; i++)
		{
			if (i == 0)
				Texture = L"Camper_Dwait.png";
			else if(i == 1)
				Texture = L"Camper_Bill.png";
			else if(i == 2)
				Texture = L"Camper_Nia.png";
			else if(i == 3)
				Texture = L"Camper_Meg.png";

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"Charictor_Base.tga"));
			pTexture->Set_Scale(_vec2(0.55f, 0.666667f));
			pTexture->Set_Pos(vPos);
			pTexture->Set_Color(D3DXCOLOR(0.3764f, 0.31372f, 0.22745f, 1.f));
			pTexture->IsColor(true);
			m_UITexture.insert({ wstring(L"Char_Base_") + to_wstring(i),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"Charictor_Base2.tga"));
			pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
			pTexture->Set_Pos(vPos);
			m_UITexture.insert({ wstring(L"Char_Base2_") + to_wstring(i),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_TexName(Texture);
			pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
			pTexture->Set_Pos(vPos);
			m_UITexture.insert({ wstring(L"Char_")+to_wstring(i),pTexture });

			if (i == 1)
				vPos += {(-0.14f * g_iBackCX), (0.3368f*g_iBackCY)};
			else
				vPos.x += (0.14f * g_iBackCX);
		}
	}
	Safe_Release(pManagement);
}

void CUI_OverlayMenu::SetUp_Perk()
{
	GET_INSTANCE_MANAGEMENT;
	CUI_Texture* pTexture = nullptr;
	_vec2 vPos = { 0.4f*g_iBackCX, 0.538608f*g_iBackCY };

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Button_Background.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Perk_Button_Background",pTexture });

	vPos = { 0.336185f * g_iBackCX, 0.392855f * g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Perk_Background.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Perk_Background",pTexture });
	
	vPos = { 0.347003f*g_iBackCX, 0.410542f*g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"HorizontalLine.tga"));
	pTexture->Set_Scale(_vec2(0.8f, 0.666667f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"HorizontalLine",pTexture });

	vPos = { 0.175656f * g_iBackCX, 0.107541f *g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"기술"), _vec2(28.f, 34.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 2);
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Perk_Font",pTexture });

	vPos = { 0.145467f * g_iBackCX, 0.439013f *g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"인벤토리"), _vec2(18.f, 22.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Perk_Font1",pTexture });

	vPos = { 0.21f*g_iBackCX, 0.321552f*g_iBackCY };
	wstring Tag = L"MainPerk_";
	for (_int i = 0; i < 4; i++)
	{
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Perk_Base.tga"));
		pTexture->Set_Scale(_vec2(0.4f, 0.4f));
		pTexture->Set_Pos(vPos);
		m_UITexture.insert({ Tag + to_wstring(i),pTexture });
		vPos.x += 0.1f * g_iBackCX;
	}
	vPos = { 0.2f*g_iBackCX, 0.489451f*g_iBackCY };
	Tag = L"InvenPerk_";
	for (_int i = 0; i < 14; i++)
	{
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Perk_Base.tga"));
		pTexture->Set_Scale(_vec2(0.4f, 0.4f));
		pTexture->Set_Pos(vPos);

		m_UITexture.insert({ Tag + to_wstring(i),pTexture });
		if (i == 4)
			vPos = { vPos.x - (0.09f * g_iBackCX * 3.5f), vPos.y + (0.1f * g_iBackCY) };
		else if (i == 8)
			vPos = { vPos.x - (0.09f * g_iBackCX * 3.5f), vPos.y + (0.1f * g_iBackCY) };
		else
			vPos.x += 0.09f * g_iBackCX;
	}
	vPos = Find_UITexture(wstring(L"MainPerk_0"))->Get_Pos();
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Perk_Used.tga"));
	pTexture->Set_Scale(_vec2(0.5f, 0.5f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ wstring(L"Perk_Used"),pTexture });
	m_CurPerk = 0;

	Safe_Release(pManagement);
}

void CUI_OverlayMenu::Ready_Check()
{

	for (_uint i = 1; i <= 5; i++)
	{
		_tchar TextureTag[256] = L"";
		swprintf_s(TextureTag, L"S_%d", i);
		if (m_iMaxPlayer >= i)
		{
			CUI_Texture* pUITexture = nullptr;
			pUITexture = Find_UITexture((wstring)TextureTag);
			pUITexture->IsRendering(true);
			if (i <= m_iCurReadyPlayer)
				pUITexture->IsColor(true);
			else
				pUITexture->IsColor(false);
		}
		else
			Find_UITexture((wstring)TextureTag)->IsRendering(false);
	}
}

void CUI_OverlayMenu::Mouse_Check()
{
	CUI_Texture* pTexture = nullptr;
	pTexture = Find_UITexture(wstring(L"CC_Panel"));
	if (pTexture->IsTouch())
	{
		if (m_CurButton == CHANGE_CHARICTER)
			return;
		
		m_CurButton = CHANGE_CHARICTER;
		return;
	}
	pTexture = Find_UITexture(wstring(L"CP_Panel"));
	if (pTexture->IsTouch())
	{
		if (m_CurButton == CHANGE_PERK)
			return;
		m_CurButton = CHANGE_PERK;
		return;
	}
	pTexture = Find_UITexture(wstring(L"CJ_Panel"));
	if (pTexture->IsTouch())
	{
		if (m_CurButton == CHANGE_JOB)
			return;
		m_CurButton = CHANGE_JOB;
		return;
	}
	pTexture = Find_UITexture(wstring(L"GB_Panel"));
	if (pTexture->IsTouch())
	{
		if (m_CurButton == BACK)
			return;
		m_CurButton = BACK;
		return;
	}
	pTexture = Find_UITexture(wstring(L"R_Panel"));
	if (pTexture->IsTouch())
	{
		if (m_CurButton == READY)
			return;
		m_CurButton = READY;
		return;
	}
	if (m_CurUI == CHANGE_PERK)
	{
		wstring Tag = L"MainPerk_";
		for (_int i = 0; i < 4; i++)
		{
			pTexture = Find_UITexture(Tag + to_wstring(i));
			if (pTexture->IsTouch())
			{
				if (m_CurButton == MAINPERK && m_CurButtonIndex == i)
					return;
				m_CurButton = MAINPERK;
				m_CurButtonIndex = i;
				return;
			}
		}
		Tag = L"InvenPerk_";
		for (_int i = 0; i < 14; i++)
		{
			pTexture = Find_UITexture(Tag + to_wstring(i));
			if (pTexture->IsTouch())
			{
				if (m_CurButton == INVENPERK && m_CurButtonIndex == i)
					return;
				m_CurButton = INVENPERK;
				m_CurButtonIndex = i;
				return;
			}
		}
	}
	else if (m_CurUI == CHANGE_CHARICTER)
	{
		wstring Tag = L"Char_Base2_";
		for (_int i = 0; i < 4; i++)
		{
			pTexture = Find_UITexture(Tag + to_wstring(i));
			if (pTexture == nullptr)
				continue;
			if (pTexture->IsTouch())
			{
				if (m_CurButton == CHARICTOR && m_CurButtonIndex == i)
					return;
				m_CurButton = CHARICTOR;
				m_CurButtonIndex = i;
				return;
			}
		}
	}

	m_CurButton = NONE;
}

void CUI_OverlayMenu::Lobby_Check()
{
	if (lserver_data.AllReady)
	{
		Ready_Ready();
		return;
	}

	CUI_Texture* pTexture = Find_UITexture(wstring(L"CJ_Icon"));
	if (m_JobState)
		pTexture->Set_TexName(wstring(L"Camper_Small.tga"));
	else
		pTexture->Set_TexName(wstring(L"Slasher_Small.tga"));

	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));

	if (m_CurButton != m_OldButton || m_OldButtonIndex != m_CurButtonIndex)
	{
		if (m_OldButton == CHANGE_JOB)
			Button_L2_Change_Job(UnTouch);
		else if (m_OldButton == CHANGE_CHARICTER)
			Button_L2_Change_Charicter(UnTouch);
		else if (m_OldButton == CHANGE_PERK)
			Button_L2_Change_Perk(UnTouch);
		else if (m_OldButton == BACK)
			Button_L2_Go_Back(UnTouch);
		else if (m_OldButton == READY)
			Button_L2_Ready(UnTouch);
		else if (m_OldButton == MAINPERK)
			Button_L2_Main_Perk(UnTouch);
		else if (m_OldButton == INVENPERK)
			Button_L2_Inven_Perk(UnTouch);
		else if (m_OldButton == CHARICTOR)
			Button_L2_Charictor(UnTouch);


		if (m_CurButton == CHANGE_JOB)
			Button_L2_Change_Job(Touch);
		else if (m_CurButton == CHANGE_CHARICTER)
			Button_L2_Change_Charicter(Touch);
		else if (m_CurButton == CHANGE_PERK)
			Button_L2_Change_Perk(Touch);
		else if (m_CurButton == BACK)
			Button_L2_Go_Back(Touch);
		else if (m_CurButton == READY)
			Button_L2_Ready(Touch);
		else if (m_CurButton == MAINPERK)
			Button_L2_Main_Perk(Touch);
		else if (m_CurButton == INVENPERK)
			Button_L2_Inven_Perk(Touch);
		else if (m_CurButton == CHARICTOR)
			Button_L2_Charictor(Touch);

		m_OldButtonIndex = m_CurButtonIndex;
		m_OldButton = m_CurButton;
	}

	if (KEYMGR->MouseDown(0))
	{
		if (m_CurButton == CHANGE_JOB)
			Button_L2_Change_Job(Click);
		else if (m_CurButton == CHANGE_CHARICTER)
			Button_L2_Change_Charicter(Click);
		else if (m_CurButton == CHANGE_PERK)
			Button_L2_Change_Perk(Click);
		else if (m_CurButton == BACK)
			Button_L2_Go_Back(Click);
		else if (m_CurButton == READY)
			Button_L2_Ready(Click);
		else if (m_CurButton == MAINPERK)
			Button_L2_Main_Perk(Click);
		else if (m_CurButton == INVENPERK)
			Button_L2_Inven_Perk(Click);
		else if (m_CurButton == CHARICTOR)
			Button_L2_Charictor(Click);
	}
}

void CUI_OverlayMenu::Server_Check()
{
	_int PlayerNum = 0;
	_int ReadyNum = 0;
	for (int i = 0; i < 5; i++)
	{
		if (lserver_data.playerdat[i].bConnect)
			PlayerNum++;
		if (lserver_data.playerdat[i].Ready)
			ReadyNum++;
	}
	m_iMaxPlayer = PlayerNum;
	m_iCurReadyPlayer = ReadyNum;
}

void CUI_OverlayMenu::Set_SideButton()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;

	_vec2 vPos = { 0.063f, 0.103f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	pTexture->Set_Alpha(0.4f);
	m_UITexture.insert({ L"CC_Panel_Shadow", pTexture });

	vPos += { 0.005f, -0.01f};
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	m_UITexture.insert({ L"CC_Panel", pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Charicter_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_UITexture.insert({ L"CC_Icon", pTexture });

	vPos = { 0.063f, 0.25f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	pTexture->Set_Alpha(0.4f);
	m_UITexture.insert({ L"CP_Panel_Shadow", pTexture });

	vPos += { 0.005f, -0.01f};
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	m_UITexture.insert({ L"CP_Panel", pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Perk.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_UITexture.insert({ L"CP_Icon", pTexture });


	vPos = { 0.063f, 0.45f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	pTexture->Set_Alpha(0.4f);
	m_UITexture.insert({ L"CJ_Panel_Shadow", pTexture });

	vPos += { 0.005f, -0.01f};
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	m_UITexture.insert({ L"CJ_Panel", pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Camper_Small.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_UITexture.insert({ L"CJ_Icon", pTexture });

	vPos += { -0.020253f, -0.044f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Change_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.5f, 0.5f));
	m_UITexture.insert({ L"CJ_Change_Icon", pTexture });

	Safe_Release(pManagement);
}

void CUI_OverlayMenu::Set_StartIcon()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture = nullptr;

	_vec2 vPos = { 0.885137f, 0.833541f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_1.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_1", pTexture });

	vPos = { 0.897278f, 0.829982f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_2.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_2", pTexture });

	vPos = { 0.910985f, 0.825878f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_3.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_3", pTexture });

	vPos = { 0.922624f, 0.833617f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_4.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_4", pTexture });

	vPos = { 0.904958f, 0.82927f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_5.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_5", pTexture });
	Safe_Release(pManagement);
}

void CUI_OverlayMenu::Set_OtherButton()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture = nullptr;
	_vec2 vPos = { 0.0931096f, 0.92f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"GoBack_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.56f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"GB_Panel", pTexture });
	 
	vPos = { 0.0892697f, 0.919537f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"GoBack_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"GB_Icon", pTexture });

	vPos = { 0.115088f, 0.919537f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"뒤로가기 [ESC]"),_vec2(12.f,16.f),D3DXCOLOR(1.f,1.f,1.f,1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"GB_Text", pTexture });

	vPos = { 0.0574244f, 0.918603f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"GoBack_Arrow.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"GB_Arrow", pTexture });

	vPos = { 0.873996f, 0.91f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.68f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"R_Panel", pTexture });

	vPos = { 0.870136f, 0.910521f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"R_Icon", pTexture });

	vPos = { 0.897147f, 0.913747f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"준비"), _vec2(22.f, 25.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"R_Text", pTexture });

	vPos = { 0.937298f, 0.912069f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Arrow.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	m_UITexture.insert({ L"R_Arrow", pTexture });
	pTexture->Set_Alpha(0.5f);
	
	Safe_Release(pManagement);
}

CUI_Texture * CUI_OverlayMenu::Find_UITexture(wstring UIName)
{
	auto& iter = m_UITexture.find(UIName);
	if (iter == m_UITexture.end())
		return nullptr;
	return iter->second;
}

void CUI_OverlayMenu::Delete_CharictorPage()
{
	Delete_UI_Texture(L"Char_Button_Background",true);
	Delete_UI_Texture(L"Char_Font", true);

	for (_int i = 0; i < 4; i++)
	{
		Delete_UI_Texture(wstring(L"Char_Edge_") +to_wstring(i),true);
		Delete_UI_Texture(wstring(L"Char_Base_")+ to_wstring(i), true);
		Delete_UI_Texture(wstring(L"Char_Base2_") + to_wstring(i), true);
		Delete_UI_Texture(wstring(L"Char_") + to_wstring(i), true);
	}
	
}

void CUI_OverlayMenu::Delete_Inventory()
{
	Delete_UI_Texture(L"Perk_Button_Background",true);
	Delete_UI_Texture(L"Perk_Background",true);
	Delete_UI_Texture(L"HorizontalLine",true);
	Delete_UI_Texture(L"Perk_Used",true);
	Delete_UI_Texture(L"Perk_Font", true);
	Delete_UI_Texture(L"Perk_Font1", true);
	
	wstring MainTag = L"MainPerk_";
	wstring InvenTag = L"InvenPerk_";

	for (_uint i = 0; i < 14; i++)
	{
		if (Find_UITexture(InvenTag + to_wstring(i)) == nullptr)
			break;
		Delete_UI_Texture(InvenTag + to_wstring(i),true);
		if (i < 4)
			Delete_UI_Texture(MainTag + to_wstring(i),true);
	}
}


void CUI_OverlayMenu::Delete_Perk()
{
	wstring BaseTag = L"InvenPerk_Base2_";
	wstring PerkTag = L"InvenPerk_Perk_";
	wstring InvenUsedTag = L"InvenPerk_Used_";
	wstring MainPerkTag = L"MainPerk_Base2_";
	wstring MainBaseTag = L"MainPerk_Perk_";
	for (_uint i = 0; i < 14; i++)
	{
		if (Find_UITexture(PerkTag + to_wstring(i)) == nullptr)
			break;
		Delete_UI_Texture(PerkTag + to_wstring(i),true);
		Delete_UI_Texture(BaseTag + to_wstring(i),true);
		if (i < 4)
		{
			Delete_UI_Texture(InvenUsedTag + to_wstring(i),true);
			Delete_UI_Texture(MainPerkTag + to_wstring(i),true);
			Delete_UI_Texture(MainBaseTag + to_wstring(i),true);
		}
	}
}

void CUI_OverlayMenu::Delete_UI_Texture(wstring UIName, _bool IsDead)
{
	auto& iter = m_UITexture.find(wstring(UIName));
	if (iter != m_UITexture.end())
	{
		if (IsDead)
			iter->second->SetDead();
		m_UITexture.erase(iter);
	}
}


void CUI_OverlayMenu::Delete_All()
{
	for (auto& iter : m_UITexture)
	{
		iter.second->Set_DeadTime(0.3f);
		iter.second->Set_FadeOut(0.3f);
	}
	m_UITexture.clear();
}

void CUI_OverlayMenu::Ready_Ready()
{
	if (lserver_data.WaittingTime != m_WaitingTime)
	{
		CUI_Texture* pTexture = nullptr;
		wstring Text = L"00:0"+to_wstring(lserver_data.WaittingTime);
		if (lserver_data.WaittingTime == 5)
		{
			_int SlasherIndex = 0;
			vector<_int>vecEquippedPerk = *m_vecEquippedPerk;
			if (m_JobState)
				SlasherIndex = 4;
			_int Perk = 0;
			for (_int i = 0; i < 4; i++)
			{
				if (vecEquippedPerk[i + SlasherIndex] != -1)
					Perk |= vecEquippedPerk[i + SlasherIndex];
			}
			if (m_JobState)
				slasher_data.Perk = Perk;
			else
				camper_data.Perk = Perk;
			GET_INSTANCE_MANAGEMENT;
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_Font(L"게임 시작까지:", _vec2(11.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f),1);
			pTexture->Set_Pos(_vec2(0.838f*g_iBackCX, 0.835f*g_iBackCY));
			m_UITexture.insert({ wstring(L"StartUI"), pTexture });
			
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
			pTexture->Set_Pos(_vec2(0.845f*g_iBackCX, 0.86f*g_iBackCY));
			pTexture->IsColor(true);
			m_UITexture.insert({ wstring(L"StartUI_Time"),pTexture });
			pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));

			GET_INSTANCE(CSoundManager)->ChannelStop(string("Lobby_Sound1"));
			GET_INSTANCE(CSoundManager)->ChannelStop(string("Lobby_Sound2"));
			GET_INSTANCE(CSoundManager)->ChannelStop(string("Lobby_Sound3"));
			GET_INSTANCE(CSoundManager)->ChannelStop(string("Lobby_Burning"));
			GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/Lobby_Start.ogg"), _vec3(0.f, 0.f, 0.f), 0, 1.f);
			Safe_Release(pManagement);
		}
		else
			GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/Heart_Bounce.ogg"), _vec3(0.f, 0.f, 0.f), 0, 1.f);
		pTexture = Find_UITexture(wstring(L"StartUI_Time"));
		if(pTexture != nullptr)
			pTexture->Set_Font(Text, _vec2(15.f, 21.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f),1);
		
		m_WaitingTime = lserver_data.WaittingTime;
	}
}

void CUI_OverlayMenu::Compute_CurPerk()
{
	vector<_int> vecEquippedPerk = *m_vecEquippedPerk;
	_int SlasherIndex = 0;
	if (m_JobState)
		SlasherIndex = 4;
	CUI_Texture* pTexture = nullptr;
	for (_int i = 0; i < 4; i++)
	{
		if (vecEquippedPerk[i+SlasherIndex] == -1)
		{
			pTexture = Find_UITexture(wstring(L"Perk_Used"));
			pTexture->Set_Pos(Find_UITexture(wstring(L"MainPerk_")+to_wstring(i))->Get_Pos());
			m_CurPerk = i;
			return;
		}
	}
}

void CUI_OverlayMenu::Delete_Explain()
{
	Delete_UI_Texture(L"Explain_Base",true);
	Delete_UI_Texture(L"Explain_Main", true);
	for(_int i = 0; i<6; i++)
		Delete_UI_Texture(wstring(L"Explain_Sub_")+to_wstring(i), true);
}

void CUI_OverlayMenu::Free()
{
	if (!m_IsClone)
	{
		Safe_Delete(m_CurCamper);
		Safe_Delete(m_CurSlasher);
		Safe_Delete(m_vecEquippedPerk);
	}
	CGameObject::Free();
}
