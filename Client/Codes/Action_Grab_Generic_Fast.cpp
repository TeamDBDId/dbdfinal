#include "stdafx.h"
#include "..\Headers\Action_Grab_Generic_Fast.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Slasher.h"

_USING(Client)

CAction_Grab_Generic_Fast::CAction_Grab_Generic_Fast()
{
}

HRESULT CAction_Grab_Generic_Fast::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	m_bIsPlaying = true;
	m_fIndex = 0.f;

	if (m_pGameObject->GetID() & SLASHER)
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->IsLockKey(true);
		m_pCamper->SetColl(false);

		pSlasher->Set_State(AS::Grab_Generic_Fast);
		m_iState = AS::Grab_Generic_Fast;
	}
	else
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->SetCurCondition(CCamper::DYING);
		pCamper->IsLockKey(true);

		if (server_data.Slasher.iCharacter == 0)
			pCamper->Set_State(AC::TT_Grab_Generic_Fast);
		else
			pCamper->Set_State(AC::WI_Grab_Generic_Fast);

		m_iState = AC::TT_Grab_Generic_Fast;
		pCamper->SetColl(false);
		pCamper->SetCarry(true);
	}

	Send_ServerData();
	return NOERROR;
}

_int CAction_Grab_Generic_Fast::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	m_fIndex += fTimeDelta * 30.f;
	size_t iIndex = (size_t)m_fIndex;

	if (m_iState == AC::TT_Grab_Generic_Fast)
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		if (pMeshCom->IsOverTime(0.3f) || iIndex >= 71)
		{
			server_data.Slasher.iCharacter == 0 ? pCamper->Set_State(AC::TT_Carry_Idle) : pCamper->Set_State(AC::WI_Carry_Idle);
			return END_ACTION;
		}

	}
	else
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			CSlasher* pSlasher = (CSlasher*)m_pGameObject;
			pSlasher->Set_State(AS::Carry_Idle);
			return END_ACTION;
		}
	}

	return UPDATE_ACTION;
}

void CAction_Grab_Generic_Fast::End_Action()
{
	m_bIsPlaying = false;
	m_fDelay = 1.f;
	if (m_pGameObject->GetID() & SLASHER)
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_Carry(true);
		pSlasher->Set_CarriedCamper(m_pCamper);
		pSlasher->IsLockKey(false);
	}
	else
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->SetHeal(0.f);
		pCamper->SetEnergy(pCamper->GetDyingEnergy());
		pCamper->SetMaxEnergyAndMaxHeal();
		pCamper->IsLockKey(false);
		pCamper->SetCarry(true);
		GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_SetWiggle); // 들었을때
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_Wiggle); // 들었을때
		GET_INSTANCE(CUIManager)->Set_ProgressPoint(pCamper->Get_ProgressTime(), pCamper->Get_MaxProgressTime());
	//	server_data.Slasher.iCharacter == 0 ?pCamper->Set_State(AC::TT_Carry_Idle): pCamper->Set_State(AC::WI_Carry_Idle);
	}

	m_pCamper = nullptr;
}

void CAction_Grab_Generic_Fast::Send_ServerData()
{
	if (m_pGameObject->GetID() & SLASHER)
	{
		slasher_data.SecondInterationObject = m_pCamper->GetID();
		slasher_data.SecondInterationObjAnimation = GRAB_GENERIC_FAST;
	}
}

void CAction_Grab_Generic_Fast::SetVectorPos()
{
	m_vecCamperPos.reserve(81);

	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.036f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.051f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.062f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.07f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.08f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.094f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.114f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.144f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.32f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.443f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.563f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.71f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.872f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.049f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.243f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.44f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.691f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.164f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -5.233f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -11.108f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -18.943f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -27.892f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -37.109f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -45.75f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -52.969f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -57.921f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));

	m_vecCamperSpiritPos.reserve(81);

	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.014f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.034f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.064f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.241f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.365f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.484f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.63f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.801f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -0.977f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -1.163f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -1.347f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -1.593f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -2.084f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -5.564f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -12.541f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -22.128f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -33.436f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -45.58f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -57.672f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -68.825f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -78.151f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -84.764f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -89.464f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -93.606f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -96.911f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.083f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.865f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.889f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.885f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.883f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.882f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.879f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.877f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.875f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.872f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.869f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.866f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.863f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.86f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.856f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.853f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.849f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.846f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.843f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.839f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.836f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.832f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.829f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.826f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.823f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.82f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.817f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.814f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.811f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.809f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.807f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.805f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.803f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.802f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.801f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.8f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.799f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.799f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.799f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.799f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.799f));
	m_vecCamperSpiritPos.push_back(_vec3(0.f, 0.f, -99.799f));
	m_vecCamperSpiritPos.push_back(_vec3(-0.01f, 0.f, -99.799f));

	m_bIsInit = true;
}

void CAction_Grab_Generic_Fast::Free()
{
	CAction::Free();
}
