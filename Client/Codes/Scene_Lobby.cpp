#include "stdafx.h"
#include "..\Headers\Scene_Lobby.h"
#include "Management.h"
#include "Scene_Stage.h"
#include "Camper.h"
#include "Slasher.h"
#include "UI_RoleSelection.h"
#include "UI_OverlayMenu.h"
#include "LoadManager.h"
#include "Camera_Lobby.h"
#include "Light_Manager.h"
#include "UI_Loading.h"
#include "FontManager.h"
#include "UI_Texture.h"
#include <string>
_USING(Client)

CScene_Lobby::CScene_Lobby(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CScene(pGraphic_Device)
{

}

HRESULT CScene_Lobby::Ready_Scene()
{
	ZeroMemory(&lobby_data, sizeof(LobbyData));
	SetUp_Sound();
	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;
	
	GET_INSTANCE(CLoadManager)->LoadMapData(1, SCENE_LOBBY);

	Clear_LayerObjList();
	
	slasher_data.Perk |= RUIN;
	if (FAILED(Ready_Layer_Camera(L"Layer_Camera")))
		return E_FAIL;
	if (FAILED(Ready_LightInfo()))
		return E_FAIL;
	m_IsLoading = false;
	bLobbyServer = true;
	Lobbysock = CServerManager::GetInstance()->Ready_RobbyServer();

	hThread = (HANDLE)_beginthreadex(nullptr, 0, Lobby_Thread, this, 0, nullptr);

	return NOERROR;
}

_int CScene_Lobby::Update_Scene(const _float & fTimeDelta)
{
	if (!m_IsLoading)
	{
		LobbyState_Check();
		if (KEYMGR->KeyDown(DIK_NUMPAD1))
		{
			lobby_data.Packet = 2;
		}
		if (KEYMGR->KeyDown(DIK_NUMPAD2))
			lobby_data.Packet = 3;
		if (KEYMGR->KeyDown(DIK_NUMPAD3))
			lobby_data.Packet = 4;
		if (KEYMGR->KeyDown(DIK_NUMPAD4))
			lobby_data.Packet = 5;

		if (KEYMGR->KeyDown(DIK_PGDN))
		{
			if (lobby_data.character > 0)
				lobby_data.character--;
		}
		if (KEYMGR->KeyDown(DIK_PGUP))
		{
			if (lobby_data.character < 3)
				lobby_data.character++;
		}
		//Check_Connection_OtherPlayers();
		Check_Connedtion_Players();
		return CScene::Update_Scene(fTimeDelta);
	}
	else
		return CScene::Update_One_Scene(fTimeDelta, SCENE_LOBBY);
}

_int CScene_Lobby::LastUpdate_Scene(const _float & fTimeDelta)
{
	
	if (lserver_data.GameStart && lserver_data.WaittingTime == 0 && !m_IsLoading)
	{
		GET_INSTANCE(CFontManager)->ReleaseAllTextures();
		GET_INSTANCE(CSoundManager)->Delete_AllSound();
		GET_INSTANCE(CLoadManager)->DeleteSceneTexture(CLoadManager::OnlyLobby);
		_bool IsSlasher = ((CUI_OverlayMenu*)m_pCurUI)->Get_Job();
		m_IsLoading = true;
		GET_INSTANCE_MANAGEMENTR(-1);
		pManagement->Clear_Layers(SCENE_LOBBY);
		//pManagement->Clear_Layers(SCENE_STAGE);
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Loading", SCENE_LOBBY, L"Layer_Lobby",&m_pCurUI);
		Safe_Release(pManagement);
		((CUI_Loading*)m_pCurUI)->Set_Job(IsSlasher);
	
		CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
		if (nullptr == pLight_Manager)
			return E_FAIL;

		pLight_Manager->AddRef();
		pLight_Manager->Delete_AllLight();
		Safe_Release(pLight_Manager);
		//GET_INSTANCE(CLoadManager)->S1_LightInfo();
		InitializeCriticalSection(&m_CritSec);
		m_hLoadingThread = (HANDLE)_beginthreadex(nullptr, 0, LoadingThreadFunc, this, 0, nullptr);
		return 0;
	}
	else if (m_IsLoading)
	{
		_int PlayerNum = 0;
		_int ReadyNum = 0;
		for (int i = 0; i < 5; i++)
		{
			if (lserver_data.playerdat[i].bConnect)
			{
				PlayerNum++;
				if (!lserver_data.playerdat[i].Ready)
					ReadyNum++;
			}
		}
		if (PlayerNum == ReadyNum)
		{
			exCountPlayerNum = PlayerNum;
			CloseHandle(m_hLoadingThread);
			Change_To_Stage();
			return 0;
		}
		return CScene::LastUpdate_One_Scene(fTimeDelta, SCENE_LOBBY);
	}
	else
		return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Lobby::Render_Scene()
{
	
}

size_t CScene_Lobby::Lobby_Thread(LPVOID lpParam)
{
	sendPacket = 0;
	QueryPerformanceFrequency(&tickPerSecond);
	QueryPerformanceCounter(&startTick);
	while (bLobbyServer) {
		QueryPerformanceCounter(&endTick);
		dTime = (double)(endTick.QuadPart - startTick.QuadPart) / tickPerSecond.QuadPart;

		while (dTime >= 0.030000) {
			sendPacket = 0;
			QueryPerformanceCounter(&startTick);
			dTime = 0;
			lobby_data.bConnect = true;
			
			CServerManager::GetInstance()->Data_ExchangeLobby(Lobbysock, lobby_data, client_data, lserver_data);
			sendPacket++;
		}
	}

	return 0;
}

size_t CScene_Lobby::LoadingThreadFunc(LPVOID lpParam)
{
	CScene_Lobby* pLobby = reinterpret_cast<CScene_Lobby*>(lpParam);

	EnterCriticalSection(&(pLobby->m_CritSec));

	GET_INSTANCE(CSoundManager)->Load_StageSound();
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_Component()))
		return E_FAIL;
	GET_INSTANCE(CLoadManager)->Set_Progress(0.1f);
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_DynamicMesh()))
		return E_FAIL;
	GET_INSTANCE(CLoadManager)->Set_Progress(0.4f);
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_StaticMesh()))
		return E_FAIL;
	GET_INSTANCE(CLoadManager)->Set_Progress(0.7f);
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_Texture()))
		return E_FAIL;
	GET_INSTANCE(CLoadManager)->Set_Progress(0.75f);
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_GameObject()))
		return E_FAIL;
	GET_INSTANCE(CLoadManager)->Set_Progress(0.8f);


	
	
	GET_INSTANCE(CLoadManager)->Set_Progress(0.97f);
	
	GET_INSTANCE(CLoadManager)->Set_Progress(1.01f);
	LeaveCriticalSection(&(pLobby->m_CritSec));
	return 0;

}

HRESULT CScene_Lobby::Ready_Prototype_GameObject()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Lobby", SCENE_LOBBY, L"Layer_UI", &m_pCurUI)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Lobby::Ready_Prototype_Component()
{
	return NOERROR;
}

HRESULT CScene_Lobby::Ready_Layer_Camera(const _tchar * pLayerTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	CCamera_Lobby*	pLobbyCamera = nullptr;

	pManagement->Add_GameObjectToLayer(L"GameObject_LobbyCamera", SCENE_LOBBY, pLayerTag, (CGameObject**)&m_pCamera);
	
	CAMERADESC		CameraDesc;
	ZeroMemory(&CameraDesc, sizeof(CAMERADESC));
	CameraDesc.vEye = _vec3(1378.4f, 346.29f, -346.64f);
	CameraDesc.vAt = _vec3(755.22f, 397.53f, 756.67f);
	CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);

	PROJDESC		ProjDesc;
	ZeroMemory(&ProjDesc, sizeof(PROJDESC));
	ProjDesc.fFovY = D3DXToRadian(60.0f);
	ProjDesc.fAspect = _float(g_iBackCX) / g_iBackCY;
	ProjDesc.fNear = 5.f;
	ProjDesc.fFar = 20000.f;

	if (FAILED(m_pCamera->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
		return E_FAIL;
	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Lobby::Ready_LightInfo()
{
	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return E_FAIL;

	pLight_Manager->AddRef();

	D3DLIGHT9			LightInfo;
	ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	LightInfo.Type = D3DLIGHT_DIRECTIONAL;
	LightInfo.Diffuse = D3DXCOLOR(0.15f, 0.15f, 0.15f, 1.f);
	LightInfo.Specular = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.f);
	LightInfo.Ambient = D3DXCOLOR(0.13f, 0.13f, 0.13f, 1.f);

	LightInfo.Direction = _vec3(1.f, -1.f, 1.f);

	if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
		return E_FAIL;

	Safe_Release(pLight_Manager);

	return NOERROR;

}

HRESULT CScene_Lobby::Ready_Layer_Camper(const _tchar * pLayerTag, _uint Index)
{
	if (lserver_data.Number[client_data.client_imei] <= 0)
		return NOERROR;

	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Player
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camper", SCENE_LOBBY, pLayerTag)))
		return E_FAIL;
	if (Index != 0)
	{
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"UI", (CGameObject**)&m_pCamperName[Index-1]);
		m_pCamperName[Index-1]->Set_Font(wstring(L"생존자") + to_wstring(Index), _vec2(20, 35.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		m_pCamperName[Index-1]->Set_IsBillboard(true);
	}
	GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/BlackSmoke_Appear_VFX_Bounce_01.ogg"), _vec3(), 0, 0.5f);
	((CCamper*)pManagement->Get_ObjectList(SCENE_LOBBY, pLayerTag).back())->Set_Index(Index);
	CTransform* pTransform = (CTransform*)pManagement->Get_ObjectList(SCENE_LOBBY, pLayerTag).back()->Get_ComponentPointer(L"Com_Transform");
	if (Index == lserver_data.Number[client_data.client_imei])
	{
		pTransform->Scaling(2.f, 2.f, 2.f);
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(1700.f, 0.f, 1020.f));
		EFFMGR->Make_Effect(CEffectManager::E_Change, *&_vec3(1700.f, 0.f, 1020.f));
		pTransform->SetUp_RotationY(D3DXToRadian(180.f));
		if (Index != 0)
			m_pCamperName[Index-1]->Set_Pos(_vec3(1700.f, 380.f, 1020.f));
	}
	else
	{
		_uint offset = 0;
		_uint iIndex = Index;
		if (lserver_data.Number[client_data.client_imei] < 5)
			offset = lserver_data.Number[client_data.client_imei];
		else
			offset = 100;

		if (iIndex > offset)
			iIndex -= 1;

		_vec3 vPos = { 1000.f, 0.f, 1120.f };
		vPos.x += iIndex * 150;

		pTransform->Scaling(1.8f, 1.8f, 1.8f);
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
		EFFMGR->Make_Effect(CEffectManager::E_Change, *&vPos);
		pTransform->SetUp_RotationY(D3DXToRadian(180.f));
		if (Index != 0)
			m_pCamperName[Index-1]->Set_Pos(vPos + _vec3(0.f, 380.f, 0.f));
	}

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Lobby::Ready_Layer_Camper(const _uint& PosIndex, const _uint& CamperNum)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	if (nullptr != pManagement->Get_GameObjectFromObjectID(SCENE_LOBBY, L"Layer_Camper", CAMPER + CamperNum))
	{
		if (m_pCamper[CamperNum] != nullptr)
		{
			m_pCamper[CamperNum]->SetDead();
			m_pCamper[CamperNum] = nullptr;
			m_pCamperName[CamperNum]->SetDead();
			m_pCamperName[CamperNum] = nullptr;
		}
		Safe_Release(pManagement);
		return NOERROR;
	}
	_int PlayerIndex = -1;
	for (_int i = 0; i < 4; i++)
	{
		if (lserver_data.Number[i] - 1 == CamperNum)
			PlayerIndex = i;
	}
	m_PlayerCharictor[CamperNum] = lserver_data.playerdat[PlayerIndex].character;
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"UI", (CGameObject**)&m_pCamperName[CamperNum]);
	m_pCamperName[CamperNum]->Set_Font(wstring(L"생존자") + to_wstring(CamperNum+1), _vec2(20, 35.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
	m_pCamperName[CamperNum]->Set_IsBillboard(true);


	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camper", SCENE_LOBBY, L"Layer_Camper", (CGameObject**)&m_pCamper[CamperNum])))
		return E_FAIL;

	_float fScale = 1.8f;
	_vec3 vPos = { 1000.f + PosIndex*150.f, 0.f, 1120.f };
	if (PosIndex == 0)
	{
		fScale = 2.f;
		vPos = { 1700.f, 0.f, 1020.f };
	}

	m_pCamper[CamperNum]->Set_Index(CamperNum+1);
	CTransform* pTransform = (CTransform*)pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Camper").back()->Get_ComponentPointer(L"Com_Transform");
	pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	pTransform->Scaling(fScale, fScale, fScale);
	pTransform->SetUp_RotationY(D3DXToRadian(180.f));
	EFFMGR->Make_Effect(CEffectManager::E_Change, vPos);

	m_pCamperName[CamperNum]->Set_Pos(vPos +_vec3(0.f, 380.f, 0.f));
	GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/BlackSmoke_Appear_VFX_Bounce_01.ogg"), _vec3(), 0, 0.5f);
	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CScene_Lobby::Ready_Layer_Slasher(const _tchar * pLayerTag, _uint Character)
{
	if (lserver_data.Number[client_data.client_imei] <= 0)
		return NOERROR;

	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	_uint iCharacter = Character;

	if (iCharacter > 1 || iCharacter < 0)
		iCharacter = 0;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Slasher", SCENE_LOBBY, pLayerTag,(CGameObject**)&m_pSlasher)))
		return E_FAIL;
	GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/BlackSmoke_Appear_VFX_Bounce_01.ogg"), _vec3(), 0, 0.5f);
	((CSlasher*)pManagement->Get_ObjectList(SCENE_LOBBY, pLayerTag).back())->Set_Index(iCharacter);
	CTransform* pTransform = (CTransform*)pManagement->Get_ObjectList(SCENE_LOBBY, pLayerTag).back()->Get_ComponentPointer(L"Com_Transform");

	pTransform->Scaling(2.2f, 2.2f, 2.2f);
	pTransform->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(1600.f, -50.f, -70.f));
	pTransform->SetUp_RotationY(D3DXToRadian(350.f));
	EFFMGR->Make_Effect(CEffectManager::E_Change, _vec3(1600.f, 0.f, -70.f));

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Lobby::Ready_Layer_Slasher(const _int & Character)
{
	if (Character != 0 && Character != 1)
		return NOERROR;
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	if (nullptr != pManagement->Get_GameObjectFromObjectID(SCENE_LOBBY, L"Layer_Slasher", SLASHER))
	{
		if (m_pSlasher != nullptr)
		{
			m_pSlasher->SetDead();
			m_pSlasher = nullptr;
		}
		Safe_Release(pManagement);
		return NOERROR;
	}

	m_PlayerCharictor[4] = Character;

	if (Character == -1)
	{
		Safe_Release(pManagement);
		return NOERROR;
	}
	
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Slasher", SCENE_LOBBY, L"Layer_Slasher", (CGameObject**)&m_pSlasher)))
		return E_FAIL;

	m_pSlasher->Set_Index(Character);
	CTransform* pTransform = (CTransform*)pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Slasher").back()->Get_ComponentPointer(L"Com_Transform");
	pTransform->Scaling(2.2f, 2.2f, 2.2f);
	pTransform->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(1600.f, 0.f, -70.f));
	pTransform->SetUp_RotationY(D3DXToRadian(350.f));
	EFFMGR->Make_Effect(CEffectManager::E_Change, _vec3(1600.f, 0.f, -70.f));
	GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/BlackSmoke_Appear_VFX_Bounce_01.ogg"), _vec3(), 0, 0.5f);
	Safe_Release(pManagement);
	return NOERROR;
}

void CScene_Lobby::SetUp_Sound()
{
	GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby_Sound1"), string("Lobby/mu_lobby_dlc_mali_01.ogg"),_vec3(),0,0.1f);
	GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby_Burning"),string("Lobby/Ambience_Menus_am_burning_02.ogg"),_vec3(), 0, 0.5f);
	GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby_Sound2"), string("Lobby/Ambience_Menus_am_lobby_01.ogg"), _vec3(), 0, 0.3f);
	GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby_Sound3"), string("Lobby/Ambience_Menus_am_ui_background_04.ogg"),_vec3(), 0, 0.3f);
}

void CScene_Lobby::Update_Server()
{
}

CScene_Lobby * CScene_Lobby::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Lobby*	pInstance = new CScene_Lobby(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		MessageBox(0, L"CScene_Lobby Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CScene_Lobby::LobbyState_Check()
{
	if (m_eCurLobbyState == RoleSelection)
	{
		_uint iClickState = ((CUI_RoleSelection*)m_pCurUI)->Get_ClickButton();
		if (iClickState == CUI_RoleSelection::NONE)
			return;
		m_pCurUI->SetDead();
		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_OverlayMenu", SCENE_LOBBY, L"Layer_UI_Lobby", &m_pCurUI);
		m_eCurLobbyState = OverlayMenu;
		if (iClickState == CUI_RoleSelection::CAMPER)
		{
			((CUI_OverlayMenu*)m_pCurUI)->Set_Job(false);

			m_pCamera->Set_State(CCamera_Lobby::L_Camper);
		}
		else if (iClickState == CUI_RoleSelection::SLASHER)
		{
			((CUI_OverlayMenu*)m_pCurUI)->Set_Job(true);
			m_pCamera->Set_State(CCamera_Lobby::L_Slasher);
		}
		Safe_Release(pManagement);
	}
	else if (m_eCurLobbyState == OverlayMenu)
	{
		_uint iClickState = ((CUI_OverlayMenu*)m_pCurUI)->Get_State();
		if (iClickState == CUI_OverlayMenu::NONE)
			return;

		if (iClickState == CUI_OverlayMenu::Go_Back)
		{
			m_pCamera->Set_State(CCamera_Lobby::L_RoleSelection);
			m_pCurUI->SetDead();
			GET_INSTANCE_MANAGEMENT;
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Lobby", SCENE_LOBBY, L"Layer_UI_Lobby", &m_pCurUI);
			m_eCurLobbyState = RoleSelection;
			Safe_Release(pManagement);
		}
		else if(iClickState == CUI_OverlayMenu::Change_To_Camper)
			m_pCamera->Set_State(CCamera_Lobby::L_Camper);
		else if (iClickState == CUI_OverlayMenu::Change_To_Slasher)
			m_pCamera->Set_State(CCamera_Lobby::L_Slasher);
			
	}
}

_int CScene_Lobby::Change_To_Stage()
{
	exPlayerNumber = lserver_data.Number[client_data.client_imei];
	bUseServer = true;
	bLobbyServer = false;
	if (5 > lserver_data.Number[client_data.client_imei])
	{
		camper_data.Character = lserver_data.playerdat[client_data.client_imei].character;
		//camper_data.Number = lserver_data.playerdat[client_data.client_imei].Number;
		//camper_data.iHP = 100;
		//camper_data.iState = 
	}
	else
	{
		slasher_data.iCharacter = lserver_data.playerdat[client_data.client_imei].character;
	}

	CScene_Stage*	pNewScene = CScene_Stage::Create(m_pGraphic_Device);
	if (nullptr == pNewScene)
		return -1;

	GET_INSTANCE_MANAGEMENTR(-1);
	if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
		return -1;

	Safe_Release(pManagement);

	Safe_Release(pNewScene);

	return 0;
}

void CScene_Lobby::Clear_LayerObjList()
{

	GET_INSTANCE_MANAGEMENT;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camper", SCENE_LOBBY, L"Layer_Camper")))
		return;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Slasher", SCENE_LOBBY, L"Layer_Slasher")))
		return;

	for (auto& pCamper : pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Camper"))
	{
		pCamper->SetDead();
	}
	for (auto& pSlasher : pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Slasher"))
	{
		pSlasher->SetDead();
	}

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camper", SCENE_STAGE, L"Layer_Camper")))
		return;
	((CCamper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").back())->Set_Index(1);

	for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
	{
		pCamper->SetDead();
	}

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Slasher", SCENE_STAGE, L"Layer_Slasher")))
		return;
	((CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").back())->Set_Index(1);

	for (auto& pSlasher : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher"))
	{
		pSlasher->SetDead();
	}

	Safe_Release(pManagement);
}

void CScene_Lobby::Check_Connedtion_Players()
{
	if (client_data.client_imei == -1 || lserver_data.AllReady)
		return;

	_int MyNum = lserver_data.Number[client_data.client_imei];

	if (MyNum == 0)
	{
		Delete_AllCharictor();
		for (_int i = 0; i < 5; i++)
			m_PlayerCharictor[i] = -1;
		return;
	}
	_int PlayerCharictor[5] = { -1, -1, -1, -1, -1 };
	for (_int i = 0; i < 5; i++) // 캠퍼 접속체크
	{
		_int PlayerNum = lserver_data.Number[i]-1;
		if (lserver_data.playerdat[i].bConnect)
			PlayerCharictor[PlayerNum] = lserver_data.playerdat[i].character;
	}

	_int PosNum = 1;
	for (_int i = 0; i < 4; i++)
	{
		_int PlayerNum = lserver_data.Number[i];

		if (PlayerCharictor[i] == m_PlayerCharictor[i])
			continue;

		if (MyNum-1 == i)
			Ready_Layer_Camper((_uint)0, (_uint)i);
		else
		{
			Ready_Layer_Camper(PosNum, i);
			PosNum++;
		}
		if (PlayerCharictor[i] == -1)
			m_PlayerCharictor[i] = -1;
	}
	if (PlayerCharictor[4] != m_PlayerCharictor[4])
		Ready_Layer_Slasher(PlayerCharictor[4]);
}

void CScene_Lobby::Check_Connection_OtherPlayers()
{
	if (lserver_data.AllReady)
		return;

	for (int i = 0; i < 5; ++i)
	{
		_uint pNumber = lserver_data.Number[i] - 1;
		if (!lserver_data.playerdat[i].bConnect)
		{
			if (pNumber < 4 && nullptr != m_pCamper[pNumber] && !m_pCamper[pNumber]->Get_IsDead())
			{
				m_pCamper[pNumber]->SetDead();
				m_pCamper[pNumber] = nullptr;
				m_pCamperName[pNumber]->SetDead();
				m_pCamperName[pNumber] = nullptr;
			}

			continue;
		}

		if (lserver_data.Number[i] <= 0)
			continue;

		if (lserver_data.Number[i] > 0)
		{
			if (lserver_data.Number[i] < 5)
			{
				if (nullptr != m_pSlasher && lserver_data.Number[client_data.client_imei] != 5)
				{
					m_pSlasher->SetDead();
					m_pSlasher = nullptr;
				}

				if (nullptr != m_pCamper[pNumber])
				{
					if (nullptr != m_pCamper[pNumber]->Get_Mesh() && m_pCamper[pNumber]->Get_Character() != lserver_data.playerdat[i].character)
					{
						m_pCamper[pNumber]->SetDead();
						m_pCamper[pNumber] = nullptr;
						if (nullptr != m_pCamperName[pNumber] && !m_pCamperName[pNumber]->Get_IsDead())
						{
							m_pCamperName[pNumber]->SetDead();
							m_pCamperName[pNumber] = nullptr;
						}
						Delete_Camper(pNumber);
					}
				}
				else
				{
					if (nullptr == Find_Camper(pNumber))
					{
						Ready_Layer_Camper(L"Layer_Camper", pNumber + 1);
						m_pCamper[pNumber] = (CCamper*)Find_Camper(pNumber);
					}
				}
			}
			else
			{
				if (lserver_data.Number[client_data.client_imei] == 5)
				{
					if (lserver_data.playerdat[i].character > 1)
						lobby_data.character = 1;
					else if (lserver_data.playerdat[i].character < 0)
						lobby_data.character = 0;

					if (nullptr == m_pSlasher)
					{
						Ready_Layer_Slasher(L"Layer_Slasher", lserver_data.playerdat[i].character);
						m_pSlasher = (CSlasher*)Find_Slasher();
					}
					else
					{
						if (m_pSlasher->Get_Character() != (CSlasher::CHARACTER)lserver_data.playerdat[i].character)
						{
							m_pSlasher->SetDead();
							m_pSlasher = nullptr;
						}
					}
				}
			}
		}
	}

	if(0 == lserver_data.Number[client_data.client_imei])
	{
		if (nullptr != m_pSlasher && lserver_data.Number[client_data.client_imei] != 5)
		{
			m_pSlasher->SetDead();
			m_pSlasher = nullptr;
		}

		for (int i = 0; i < 4; ++i)
		{
			CCamper* pCamper = (CCamper*)Find_Camper(i);
			if (nullptr != pCamper)
			{
				if (m_pCamperName[0] != nullptr)
					m_pCamperName[0]->SetDead();
				m_pCamperName[0] = nullptr;
				pCamper->SetDead();
			}
		}
	}

	//_bool bE = false;
	//for (int i = 0; i < 4; ++i)
	//{
	//	bE = false;
	//	if (nullptr != m_pCamper[i])
	//	{
	//		for (int j = 0; j < 5; ++j)
	//		{
	//			if (lserver_data.Number[j] == (i + 1))
	//			{
	//				bE = true;
	//				break;
	//			}
	//		}
	//		if (!bE)
	//		{
	//			m_pCamperName[i]->SetDead();
	//			m_pCamperName[i] = nullptr;
	//			m_pCamper[i]->SetDead();
	//			m_pCamper[i] = nullptr;
	//		}
	//	}
	//}
}

CGameObject* CScene_Lobby::Find_Camper(_uint idx)
{
	GET_INSTANCE_MANAGEMENTR(nullptr);
	
	for (auto& pCamper : pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Camper"))
	{
		if (idx == (pCamper->GetID() - CAMPER))
		{
			Safe_Release(pManagement);
			return pCamper;
		}
	}
	Safe_Release(pManagement);
	return nullptr;
}

void CScene_Lobby::Delete_Camper(_uint idx)
{
	GET_INSTANCE_MANAGEMENT;

	for (auto& pCamper : pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Camper"))
	{
		if (idx == (pCamper->GetID() - CAMPER))
		{
			pCamper->SetDead();
		}
	}
	Safe_Release(pManagement);
}

CGameObject* CScene_Lobby::Find_Slasher()
{
	GET_INSTANCE_MANAGEMENTR(nullptr);

	if (pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Slasher").size() == 0)
	{
		Safe_Release(pManagement);
		return nullptr;
	}
	else
	{
		Safe_Release(pManagement);
		return pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Slasher").front();
	}
}

void CScene_Lobby::Delete_AllCharictor()
{
	for (_int i = 0; i < 4; i++)
	{
		if (m_pCamper[i] != nullptr)
		{
			m_pCamper[i]->SetDead();
			m_pCamper[i] = nullptr;
		}
		if (m_pCamperName[i] != nullptr)
		{
			m_pCamperName[i]->SetDead();
			m_pCamperName[i] = nullptr;
		}
	}
	if (m_pSlasher != nullptr)
	{
		m_pSlasher->SetDead();
		m_pSlasher = nullptr;
	}
}

void CScene_Lobby::Free()
{
	m_pGraphic_Device->EvictManagedResources();

	ZeroMemory(&lobby_data, sizeof(LobbyData));

	CServerManager::GetInstance()->Close_Lobby();

	CloseHandle(hThread);

	for (int i = 0; i < 4; ++i)
	{
		if (nullptr != m_pCamper[i])
		{
			m_pCamper[i]->SetDead();
			m_pCamper[i] = nullptr;
		}
			
	}
	if (nullptr != m_pSlasher)
	{
		m_pSlasher->SetDead();
		m_pSlasher = nullptr;
	}
	//GET_INSTANCE_MANAGEMENT;

	//for (auto& pCamper : pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Camper"))
	//{
	//	pCamper->SetDead();
	//}
	//for (auto& pSlasher : pManagement->Get_ObjectList(SCENE_LOBBY, L"Layer_Slasher"))
	//{
	//	pSlasher->SetDead();
	//}

	//Safe_Release(pManagement);

	CScene::Free();
}